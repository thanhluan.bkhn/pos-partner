const path = require('path');
const fs = require('fs');


function pacalCaseToSnakeCase (str) {
    return str.replace(/(?:^|\.?)([A-Z])/g, function (x,y){return "_" + y.toLowerCase()}).replace(/^_/, "")
}

function defineIndex() {

    

    const srcDir = path.join(__dirname, "../output");
    const entityPrimaryPath = path.join(srcDir, 'entities');
    const indexPath = path.join(entityPrimaryPath, "index.ts");
    if (!fs.existsSync(indexPath)) {
        fs.createWriteStream(indexPath)
    }
    fs.truncateSync(indexPath);
    const arr = fs.readdirSync(entityPrimaryPath)

    const list = arr.map(nameFile => {
        // const newFileName = pacalCaseToSnakeCase(nameFile).replace(/_/g,"-");
        // fs.renameSync(path.join(entityPrimaryPath,nameFile),path.join(entityPrimaryPath,newFileName));
        // return `export * from "./${newFileName.replace(".ts","")}"; \n`
        return `export * from "./${nameFile.replace(".ts","")}"; \n`
    })
    
    list.forEach(content => {
        fs.appendFileSync(indexPath, content, {
            encoding: 'utf-8'
        })
    })

}


defineIndex();
