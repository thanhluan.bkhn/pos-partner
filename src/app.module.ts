import { DynamicModule, MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { dbModules } from '~/@config';
import { MerchantCentricMiddleware } from './@systems/middlewares/merchant-centric.middleware';
import bootServer from './boot.server';
import * as allModules from './x-modules';

const envConfig = bootServer.envConfig
const multipleDatabaseModule: DynamicModule[] = dbModules(envConfig.DBS);
const modules = Object.values(allModules);
@Module({
  imports: [
    ...multipleDatabaseModule,
    ...modules
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .forRoutes({ path: "*", method: RequestMethod.ALL })
      .apply(MerchantCentricMiddleware).forRoutes({path: "merchant-centric", method: RequestMethod.ALL});
  }
}
