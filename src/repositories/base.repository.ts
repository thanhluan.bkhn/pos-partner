import { AbstractRepository, FindConditions, FindManyOptions, JoinOptions, ObjectLiteral, Repository, Entity } from 'typeorm';
import { Utils } from '~/@utils';
import { IPageRequest, IPageResponse } from '~/common/interfaces/paginations';

export  class BaseRepository<Entity> extends Repository<Entity> {
    constructor() {
        super();
    }
    static getConnectionName() {
        return "default";
    }
    async findByPage(pageRequest?: IPageRequest<Entity>) {
        const {pageIndex, pageSize, ...whereReq} = pageRequest
        const skip = pageSize && pageIndex && pageIndex > 0 ? (pageIndex - 1) * pageSize : undefined

        const formatWhere =  Utils.mapWhereAndRelations(whereReq);
        const {where, relations} = formatWhere;
        const alias = 'table_root';

        const join: JoinOptions = {
            alias: alias,
            leftJoin: {},
        }

        relations.forEach( keyName => {
            Object.assign(join.leftJoin, {
                [keyName]: `${alias}.${keyName}`.trim()
            })
        })

        const findOptions: FindManyOptions<Entity> = {
            join: {
                ...join
            },

            where: {
                ...where
            },
            take: skip !== undefined && skip >= 0 ? pageSize : undefined,
            skip: skip,
            // loadRelationIds: true
        }
        const data =  this.find(findOptions);
        const total =  this.count({
            where: {
                ...where
            }
        });
        const res = await Promise.all([
            data,
            total
        ])
        return {
            data: res[0],
            total: res[1],
            pageIndex: pageIndex,
            pageSize: pageSize
        } as IPageResponse<Entity>
    }
}



export class BaseFixRepository<Entity extends ObjectLiteral> extends AbstractRepository<Entity> {
    constructor(){
        super();
    }
    find(options?: FindManyOptions<Entity>): Promise<Entity[]>;
    find(conditions?: FindConditions<Entity>): Promise<Entity[]>;
    find(options?: FindManyOptions<Entity> | FindConditions<Entity>) {
        return new Promise((res,rej) => {
            res([]);
        })
    }
}