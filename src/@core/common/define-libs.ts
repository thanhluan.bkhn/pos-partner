import { INestApplication, RequestMethod } from "@nestjs/common";
import { NestContainer, MetadataScanner } from "@nestjs/core";
import { MODULE_PATH, PATH_METADATA, METHOD_METADATA } from "@nestjs/common/constants";

export function defineLib (app: INestApplication) {
    const container: NestContainer = (app as any).container;
    const modules = container.getModules();
    const scanner = new MetadataScanner();
    modules.forEach(item => {
            const { routes , metatype} = item

            // Reflect.defineMetadata(MODULE_PATH, "/luan", metatype);
            let modulePath = metatype ? Reflect.getMetadata(MODULE_PATH, metatype) : undefined;

            routes.forEach(rt => {
                const { metatype, instance } = rt;
                const controllerPath = Reflect.getMetadata(PATH_METADATA, metatype);

                // Reflect.defineMetadata(PATH_METADATA, "abc/" + controllerPath , metatype);
                // Reflect.defineMetadata(MODULE_PATH, GeoCountryController, metatype);
                // Reflect.defineMetadata(MODULE_PATH, parentController, target);
                const instancePrototype = Object.getPrototypeOf(instance);
                const controllerPath1 = Reflect.getMetadata(PATH_METADATA, metatype);
                console.log('-------------------');
                console.log({
                    controllerPath1
                });
                console.log('-------------------');
                scanner.scanFromPrototype(instance, instancePrototype, method => {
                    const targetCallback = instancePrototype[method];
                    const isExcludeMethod = Reflect.getMetadata('exclude', targetCallback) === 'true';
                    const requestMethod: RequestMethod = Reflect.getMetadata(METHOD_METADATA, targetCallback);
                    const routePath = Reflect.getMetadata(PATH_METADATA, targetCallback);
                    console.log('-------------------');
                    console.log(routePath, requestMethod);
                    console.log('-------------------');
                })

            })

        })
}