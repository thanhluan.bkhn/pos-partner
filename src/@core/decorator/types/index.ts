export type TRepoClass = { getConnectionName: any, new(...args: any[]): any; };
export type TClass = { new(...args: any[]): any; };