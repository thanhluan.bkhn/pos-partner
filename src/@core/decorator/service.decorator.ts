import { TClass } from "./types";
import * as services from '~/services'
import { Injectable, InjectableOptions, Inject } from "@nestjs/common";
let serviceDecoratorList = {

}
type KeyBindService = keyof typeof services;
export function InjectService(className: KeyBindService): PropertyDecorator {
    if (!className) {
        throw "EMPTY CLASS NAME"
    }
    return function (target: Object, propertyKey: string) {
        let val = target[propertyKey];
        Object.defineProperty(target, propertyKey, {
            get: () => {

                if (!val) {
                    const newService = new serviceDecoratorList[className as string]();

                    return newService;
                }
                return val;
            }
        })
    }
}
export function Service(options?: InjectableOptions): ClassDecorator {

    return function (target: Function) {
        Injectable(options)
        const name = target.name
        if (serviceDecoratorList[name]) {
            // throw new Error(`Bean ${name} is existed`);
            console.log(`Bean ${name} is existed`);
        }
        serviceDecoratorList[name] = target;

    }
}