import 'reflect-metadata';
import { Expose } from 'class-transformer';
export const DTO_PROP = "dto:prop"
export function DtoProp(options?: any) {
   return function  (target: Object, propertyKey: string ) {
       console.log('-------------------');
       console.log("DtoProp");
       console.log(propertyKey);
       console.log('-------------------');
       Reflect.defineMetadata(DTO_PROP, target[propertyKey], target);
   }
}
export function DefDto(): ClassDecorator {
    return function (target: Object) {
        Reflect.defineMetadata(DTO_PROP, "TTT" , target);
    }
}
