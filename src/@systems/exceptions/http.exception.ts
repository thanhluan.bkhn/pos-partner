import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { ValidateError } from '~/common/utilities/validate/global.validate';
import { HttpPartnerException } from '~/x-modules/partner/partner.exceptions';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    let status = exception.getStatus();

    if(exception instanceof ValidateError) {
        response.status(status)
        .send({
          code: status,
          error: true,
          messages: exception.messages
        })
        return;
    }

    // Custome cho từng nghiệp vụ
    if (exception["customeType"]) {
      return customeEceptionHandle(exception, response);
    }

    console.log("Errorhandle", request.path, "status", status, "message:", exception.message);
    let res = exception.getResponse();
    try {
      if (response instanceof Object) {
        if (res['statusCode']) {
          res = Object.assign({code: res['statusCode']}, res);
          if (res['statusCode'] > 300) { res["error"] = true; }
          delete res['statusCode'];
        }
      }
    } catch (ex) {
      console.log(ex);
    }
    response
      .status(status)
      .send( res );
  }
}

function customeEceptionHandle(exception: HttpException, response: Response) {
  let ex ;
  switch (exception["customeType"]) {
    case "HttpPartnerException":
      ex = HttpPartnerException.getInstance(exception);
      break;
  }
  let status = exception.getStatus() < 200 ? 200 : exception.getStatus();
  response.status(status).json(ex);
}