import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { Request, Response } from 'express';
import { KeyHeader } from '~/common/constants';
import bootServer from '~/boot.server';

@Injectable()
export class MerchantCentricMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    try {
      const { headers = {} } = req;
      if (!headers || !headers[KeyHeader.APIKEY]) {
        throw new UnauthorizedException("EMPTY API KEY");
      }
      if (headers[KeyHeader.APIKEY] !== bootServer.envConfig.APIKEY.MERCHANT_CENTRIC) {
        throw new UnauthorizedException("API KEY NOT FOUND");
      }
      next();
    } catch (error) {
      next(error);
    }
  }
}
