import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Component } from "./component";
import { Language } from "./language";
import { User } from "./user";

@Index("IX_ConstraintGroup", ["languageId", "componentId", "clientId", "key"], {
  unique: true
})
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Index("FK_LanguageResource_Client_clientId", ["clientId"], {})
@Index("FK_LanguageResource_Component_componentId", ["componentId"], {})
@Entity("languageresource", { schema: "swm" })
export class Languageresource {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "languageId", length: 36 })
  languageId: string;

  @DefColumn("char", { name: "componentId", length: 36 })
  componentId: string;

  @DefColumn("char", { name: "clientId", nullable: true, length: 36 })
  clientId: string | null;

  @DefColumn("varchar", { name: "key", length: 100 })
  key: string;

  @DefColumn("longtext", { name: "value", nullable: true })
  value: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @ManyToOne(
    () => Component,
    component => component.languageresources,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "componentId", referencedColumnName: "id" }])
  component: Component;

  @ManyToOne(
    () => Language,
    language => language.languageresources,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "languageId", referencedColumnName: "id" }])
  language: Language;

  @ManyToOne(
    () => User,
    user => user.languageresources,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.languageresources2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
