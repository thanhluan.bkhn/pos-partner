import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TRA_274_UNIQUE", ["id"], { unique: true })
@Entity("charge", { schema: "swm" })
export class Charge {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "CHARGECODE", length: 50 })
  chargecode: string;

  @DefColumn("varchar", { name: "CHARGENAME", nullable: true, length: 255 })
  chargename: string | null;

  @DefColumn("varchar", { name: "CHARGENAMEERP", nullable: true, length: 255 })
  chargenameerp: string | null;

  @DefColumn("varchar", { name: "CHARGETYPECODE", nullable: true, length: 65 })
  chargetypecode: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 65 })
  notes: string | null;

  @DefColumn("tinyint", { name: "HIDE", nullable: true, default: () => "'0'" })
  hide: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
