import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("dock", { schema: "swm" })
export class Dock {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "id" })
  id: string;

  @DefColumn("varchar", { name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "dockcode", length: 30 })
  dockcode: string;

  @DefColumn("varchar", { name: "description", nullable: true, length: 65 })
  description: string | null;

  @DefColumn("varchar", { name: "dockstatus", nullable: true, length: 20 })
  dockstatus: string | null;

  @DefColumn("int", { name: "docklimit", nullable: true })
  docklimit: number | null;

  @DefColumn("varchar", { name: "docktype", nullable: true, length: 10 })
  docktype: string | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 30 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 30 })
  editwho: string | null;

  @DefColumn("tinyint", { name: "deleted", nullable: true })
  deleted: number | null;
}
