import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("crystalreportindex", { schema: "swm" })
export class Crystalreportindex {
  @DefColumn("int", { primary: true, name: "id" })
  id: number;

  @DefColumn("int", { name: "idx", nullable: true })
  idx: number | null;
}
