import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_SKU", ["whseid", "storerkey", "sku"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("replenishmentsku", { schema: "swm" })
export class Replenishmentsku {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { primary: true, name: "UOM", length: 10 })
  uom: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE01", length: 100 })
  lottable01: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE02", length: 100 })
  lottable02: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE03", length: 100 })
  lottable03: string;

  @DefColumn("datetime", { primary: true, name: "LOTTABLE04" })
  lottable04: Date;

  @DefColumn("datetime", { primary: true, name: "LOTTABLE05" })
  lottable05: Date;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE06", length: 100 })
  lottable06: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE07", length: 100 })
  lottable07: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE08", length: 100 })
  lottable08: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE09", length: 100 })
  lottable09: string;

  @DefColumn("varchar", { primary: true, name: "LOTTABLE10", length: 100 })
  lottable10: string;

  @DefColumn("datetime", { primary: true, name: "LOTTABLE11" })
  lottable11: Date;

  @DefColumn("datetime", { primary: true, name: "LOTTABLE12" })
  lottable12: Date;

  @DefColumn("decimal", {
    name: "MIN",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  min: string | null;

  @DefColumn("decimal", {
    name: "MAX",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  max: string | null;
}
