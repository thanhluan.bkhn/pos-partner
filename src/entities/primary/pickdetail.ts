import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_ID", ["id"], { unique: true })
@Index("IX_SKU_LOC", ["whseid", "sku", "loc"], {})
@Index(
  "IX_ORDERKEY_ORDERLINENUMBER",
  ["whseid", "orderkey", "orderlinenumber"],
  {}
)
@Index("IX_ORDERKEY", ["whseid", "orderkey"], {})
@Index("IX_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_LPNID", ["whseid", "lpnid"], {})
@Index("IX_WHSEID", ["whseid"], {})
@Index(
  "IX_ORDERKEY_ORDERLINE_LPNID_LOC_FROMUNIT_FROMCARTON_FROMPALLET",
  [
    "whseid",
    "orderkey",
    "orderlinenumber",
    "lpnid",
    "loc",
    "fromunitid",
    "fromcartonid",
    "frompalletid"
  ],
  {}
)
@Index("IX_ORDERKEY_STATUS", ["whseid", "orderkey", "status"], {})
@Index("IX_SKU", ["whseid", "storerkey", "sku"], {})
@Index("IX_SKU_EDITDATE", ["whseid", "storerkey", "sku", "editdate"], {})
@Index("IX_SKU_LOT", ["whseid", "storerkey", "sku", "lot"], {})
@Index("IX_EDITDATE", ["whseid", "editdate"], {})
@Index("IX_EDITDATE_STATUS", ["whseid", "editdate", "status"], {})
@Index("IX_SKU_ORDERKEY", ["whseid", "orderkey", "sku", "orderlinenumber"], {})
@Index(
  "IX_PICKDETAIL_INV",
  [
    "whseid",
    "lot",
    "storerkey",
    "sku",
    "loc",
    "lpnid",
    "fromunitid",
    "fromcartonid",
    "frompalletid"
  ],
  {}
)
@Index("IX_PICKDETAIL_LOTATTRIBUTE", ["whseid", "storerkey", "sku", "lot"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("pickdetail", { schema: "swm" })
export class Pickdetail {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "PICKDETAILKEY", length: 18 })
  pickdetailkey: string;

  @DefColumn("varchar", { name: "CASEID", nullable: true, length: 20 })
  caseid: string | null;

  @DefColumn("varchar", { name: "PICKHEADERKEY", nullable: true, length: 18 })
  pickheaderkey: string | null;

  @DefColumn("varchar", { name: "ORDERKEY", length: 10 })
  orderkey: string;

  @DefColumn("varchar", { name: "ORDERLINENUMBER", length: 5 })
  orderlinenumber: string;

  @DefColumn("varchar", { name: "LOT", length: 10 })
  lot: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "ALTSKU", nullable: true, length: 50 })
  altsku: string | null;

  @DefColumn("varchar", { name: "UOM", length: 10 })
  uom: string;

  @DefColumn("decimal", { name: "UOMQTY", precision: 22, scale: 5 })
  uomqty: string;

  @DefColumn("decimal", { name: "QTY", precision: 22, scale: 5 })
  qty: string;

  @DefColumn("decimal", {
    name: "QTYMOVED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtymoved: string | null;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("varchar", { name: "DROPID", nullable: true, length: 18 })
  dropid: string | null;

  @DefColumn("varchar", { name: "LOC", length: 30 })
  loc: string;

  @DefColumn("varchar", { name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { name: "PACKKEY", length: 50 })
  packkey: string;

  @DefColumn("varchar", { name: "UPDATESOURCE", nullable: true, length: 10 })
  updatesource: string | null;

  @DefColumn("varchar", { name: "CARTONGROUP", nullable: true, length: 10 })
  cartongroup: string | null;

  @DefColumn("varchar", { name: "CARTONTYPE", nullable: true, length: 10 })
  cartontype: string | null;

  @DefColumn("varchar", { name: "TOLOC", nullable: true, length: 30 })
  toloc: string | null;

  @DefColumn("varchar", { name: "DOREPLENISH", nullable: true, length: 1 })
  doreplenish: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE", nullable: true, length: 10 })
  replenishzone: string | null;

  @DefColumn("varchar", { name: "DOCARTONIZE", nullable: true, length: 1 })
  docartonize: string | null;

  @DefColumn("varchar", { name: "PICKMETHOD", nullable: true, length: 1 })
  pickmethod: string | null;

  @DefColumn("varchar", { name: "WAVEKEY", nullable: true, length: 10 })
  wavekey: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE" })
  effectivedate: Date;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "FROMLOC", nullable: true, length: 30 })
  fromloc: string | null;

  @DefColumn("varchar", { name: "TRACKINGID", nullable: true, length: 45 })
  trackingid: string | null;

  @DefColumn("double", {
    name: "FREIGHTCHARGES",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  freightcharges: number | null;

  @DefColumn("varchar", {
    name: "INTERMODALVEHICLE",
    nullable: true,
    length: 30
  })
  intermodalvehicle: string | null;

  @DefColumn("varchar", { name: "LOADID", nullable: true, length: 20 })
  loadid: string | null;

  @DefColumn("int", { name: "STOP", nullable: true, default: () => "'0'" })
  stop: number | null;

  @DefColumn("varchar", { name: "DOOR", nullable: true, length: 18 })
  door: string | null;

  @DefColumn("varchar", { name: "ROUTE", nullable: true, length: 18 })
  route: string | null;

  @DefColumn("varchar", {
    name: "SORTATIONLOCATION",
    nullable: true,
    length: 18
  })
  sortationlocation: string | null;

  @DefColumn("varchar", {
    name: "SORTATIONSTATION",
    nullable: true,
    length: 18
  })
  sortationstation: string | null;

  @DefColumn("varchar", { name: "BATCHCARTONID", nullable: true, length: 18 })
  batchcartonid: string | null;

  @DefColumn("varchar", { name: "ISCLOSED", nullable: true, length: 1 })
  isclosed: string | null;

  @DefColumn("varchar", { name: "QCSTATUS", nullable: true, length: 10 })
  qcstatus: string | null;

  @DefColumn("varchar", { name: "PDUDF1", nullable: true, length: 10 })
  pdudf1: string | null;

  @DefColumn("varchar", { name: "PDUDF2", nullable: true, length: 10 })
  pdudf2: string | null;

  @DefColumn("varchar", { name: "PDUDF3", nullable: true, length: 10 })
  pdudf3: string | null;

  @DefColumn("varchar", { name: "PICKNOTES", nullable: true, length: 255 })
  picknotes: string | null;

  @DefColumn("varchar", { name: "RECEIPTKEY", nullable: true, length: 10 })
  receiptkey: string | null;

  @DefColumn("varchar", { name: "CROSSDOCKED", nullable: true, length: 1 })
  crossdocked: string | null;

  @DefColumn("int", { name: "SEQNO", nullable: true, default: () => "'0'" })
  seqno: number | null;

  @DefColumn("varchar", { name: "LABELTYPE", nullable: true, length: 10 })
  labeltype: string | null;

  @DefColumn("varchar", { name: "COMPANYPREFIX", nullable: true, length: 10 })
  companyprefix: string | null;

  @DefColumn("varchar", { name: "SERIALREFERENCE", nullable: true, length: 15 })
  serialreference: string | null;

  @DefColumn("varchar", { name: "ITRNKEY", length: 15 })
  itrnkey: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 100 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 100 })
  editwho: string;

  @DefColumn("varchar", { name: "OPTIMIZECOP", nullable: true, length: 1 })
  optimizecop: string | null;

  @DefColumn("decimal", {
    name: "QTYREJECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyrejected: string | null;

  @DefColumn("varchar", { name: "REJECTEDREASON", nullable: true, length: 10 })
  rejectedreason: string | null;

  @DefColumn("varchar", { name: "STATUSREQUIRED", nullable: true, length: 10 })
  statusrequired: string | null;

  @DefColumn("varchar", {
    name: "SELECTEDCARTONTYPE",
    nullable: true,
    length: 10
  })
  selectedcartontype: string | null;

  @DefColumn("varchar", {
    name: "SELECTEDCARTONID",
    nullable: true,
    length: 20
  })
  selectedcartonid: string | null;

  @DefColumn("varchar", { name: "SHIPID", nullable: true, length: 40 })
  shipid: string | null;

  @DefColumn("varchar", { name: "TOID", nullable: true, length: 18 })
  toid: string | null;

  @DefColumn("varchar", { name: "TOCARTONTYPE", nullable: true, length: 10 })
  tocartontype: string | null;

  @DefColumn("varchar", { name: "MOVETOLOCATION", nullable: true, length: 50 })
  movetolocation: string | null;

  @DefColumn("varchar", { name: "MOVETOLPN", nullable: true, length: 50 })
  movetolpn: string | null;

  @DefColumn("decimal", {
    name: "MOVETOQTY",
    nullable: true,
    precision: 18,
    scale: 0,
    default: () => "'0'"
  })
  movetoqty: string | null;

  @DefColumn("int", {
    name: "MOVECONFIRM",
    nullable: true,
    default: () => "'0'"
  })
  moveconfirm: number | null;

  @DefColumn("decimal", {
    name: "QTYCASE",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtycase: string | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 30 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 30 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 32 })
  palletid: string | null;

  @DefColumn("varchar", { name: "FROMUNITID", nullable: true, length: 30 })
  fromunitid: string | null;

  @DefColumn("varchar", { name: "FROMCARTONID", nullable: true, length: 30 })
  fromcartonid: string | null;

  @DefColumn("varchar", { name: "FROMPALLETID", nullable: true, length: 32 })
  frompalletid: string | null;

  @DefColumn("varchar", { name: "CONDITIONCODE", nullable: true, length: 50 })
  conditioncode: string | null;

  @DefColumn("tinyint", {
    name: "ISPACKED",
    nullable: true,
    width: 1,
    default: () => "'0'"
  })
  ispacked: boolean | null;

  @DefColumn("varchar", { name: "TRIPCODE", nullable: true, length: 45 })
  tripcode: string | null;

  @DefColumn("varchar", { name: "DRIVERNAME", nullable: true, length: 100 })
  drivername: string | null;

  @DefColumn("varchar", { name: "TRAILERNUMBER", nullable: true, length: 45 })
  trailernumber: string | null;

  @DefColumn("datetime", { name: "TRIPDATE", nullable: true })
  tripdate: Date | null;
}
