import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { User } from "./user";

@Index("IX_Type", ["type"], { unique: true })
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("reportrequest", { schema: "swm" })
export class Reportrequest {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("longtext", { name: "language", nullable: true })
  language: string | null;

  @DefColumn("varchar", { name: "type", unique: true, length: 50 })
  type: string;

  @DefColumn("longtext", { name: "param", nullable: true })
  param: string | null;

  @DefColumn("longtext", { name: "client", nullable: true })
  client: string | null;

  @DefColumn("longtext", { name: "warehouse", nullable: true })
  warehouse: string | null;

  @DefColumn("longtext", { name: "storer", nullable: true })
  storer: string | null;

  @DefColumn("longtext", { name: "supplier", nullable: true })
  supplier: string | null;

  @DefColumn("longtext", { name: "customer", nullable: true })
  customer: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @ManyToOne(
    () => User,
    user => user.reportrequests,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.reportrequests2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
