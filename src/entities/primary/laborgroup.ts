import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("id_UNIQUE", ["id"], { unique: true })
@Entity("laborgroup", { schema: "swm" })
export class Laborgroup {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "id", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 45 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "employeegroupcode",
    length: 45
  })
  employeegroupcode: string;

  @DefColumn("varchar", {
    name: "employeegroupname",
    nullable: true,
    length: 65
  })
  employeegroupname: string | null;

  @DefColumn("tinyint", { name: "hide", nullable: true, default: () => "'0'" })
  hide: number | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 45 })
  editwho: string | null;
}
