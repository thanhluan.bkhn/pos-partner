import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Report } from "./report";
import { User } from "./user";

@Index("IX_reportId", ["reportId"], {})
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("reportparam", { schema: "swm" })
export class Reportparam {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "reportId", length: 36 })
  reportId: string;

  @DefColumn("varchar", { name: "paramName", length: 200 })
  paramName: string;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("varchar", { name: "paramCode", length: 50 })
  paramCode: string;

  @ManyToOne(
    () => Report,
    report => report.reportparams,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "reportId", referencedColumnName: "id" }])
  report: Report;

  @ManyToOne(
    () => User,
    user => user.reportparams,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.reportparams2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
