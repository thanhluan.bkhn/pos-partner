import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WHSEID_UNIQUE", ["whseid", "mastercode"], { unique: true })
@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_MASTERCODE", ["mastercode"], {})
@Entity("masterpick", { schema: "swm" })
export class Masterpick {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "MASTERCODE", length: 30 })
  mastercode: string;

  @DefColumn("varchar", { name: "MASTERDESC", length: 500 })
  masterdesc: string;

  @DefColumn("varchar", {
    name: "MASTERSTATUS",
    nullable: true,
    length: 15,
    default: () => "'NEW'"
  })
  masterstatus: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("int", { name: "LOCK", nullable: true, default: () => "'0'" })
  lock: number | null;
}
