import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ADJ_6_UNIQUE", ["id"], { unique: true })
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_STOREKEY_SKU", ["whseid", "storerkey", "sku"], {})
@Index(
  "IX_STORERKEY_SKU_EDITDATE",
  ["whseid", "storerkey", "sku", "editdate"],
  {}
)
@Index("IX_STORERKEY_SKU_LOT", ["whseid", "storerkey", "sku", "lot"], {})
@Index(
  "IX_STORERKEY_SKU_LOT_EDITDATE",
  ["whseid", "storerkey", "sku", "lot", "editdate"],
  {}
)
@Index("IX_EDITDATE", ["whseid", "editdate"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("adjustmentdetail", { schema: "swm" })
export class Adjustmentdetail {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "ADJUSTMENTKEY", length: 10 })
  adjustmentkey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "ADJUSTMENTLINENUMBER",
    length: 5
  })
  adjustmentlinenumber: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "LOC", length: 30 })
  loc: string;

  @DefColumn("varchar", { name: "LOT", length: 10 })
  lot: string;

  @DefColumn("varchar", { name: "REASONCODE", nullable: true, length: 10 })
  reasoncode: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qty: string | null;

  @DefColumn("decimal", {
    name: "CASECNT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  casecnt: string | null;

  @DefColumn("decimal", {
    name: "INNERPACK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  innerpack: string | null;

  @DefColumn("decimal", {
    name: "PALLET",
    nullable: true,
    precision: 22,
    scale: 5
  })
  pallet: string | null;

  @DefColumn("double", { name: "CUBE", nullable: true, precision: 22 })
  cube: number | null;

  @DefColumn("double", { name: "GROSSWGT", nullable: true, precision: 22 })
  grosswgt: number | null;

  @DefColumn("double", { name: "NETWGT", nullable: true, precision: 22 })
  netwgt: number | null;

  @DefColumn("double", { name: "OTHERUNIT1", nullable: true, precision: 22 })
  otherunit1: number | null;

  @DefColumn("double", { name: "OTHERUNIT2", nullable: true, precision: 22 })
  otherunit2: number | null;

  @DefColumn("varchar", { name: "ITRNKEY", nullable: true, length: 10 })
  itrnkey: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE" })
  effectivedate: Date;

  @DefColumn("varchar", { name: "RECEIPTKEY", nullable: true, length: 10 })
  receiptkey: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTLINENUMBER",
    nullable: true,
    length: 10
  })
  receiptlinenumber: string | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "UPDATEFLAG", nullable: true, length: 1 })
  updateflag: string | null;

  @DefColumn("varchar", { name: "REFERENCEKEY", nullable: true, length: 10 })
  referencekey: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 2000 })
  notes: string | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 30 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 18 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 32 })
  palletid: string | null;

  @DefColumn("tinyint", { name: "CONFIRM", default: () => "'0'" })
  confirm: number;

  @DefColumn("varchar", { name: "LPNID", nullable: true, length: 18 })
  lpnid: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;
}
