import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_ShortText1", ["shortText1"], {})
@Index("IX_ShortText2", ["shortText2"], {})
@Index("IX_ShortText3", ["shortText3"], {})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("infomation", { schema: "swm" })
export class Infomation {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "referenceId", nullable: true, length: 36 })
  referenceId: string | null;

  @DefColumn("varchar", { name: "referenceCode", nullable: true, length: 50 })
  referenceCode: string | null;

  @DefColumn("longtext", { name: "referenceInfomation", nullable: true })
  referenceInfomation: string | null;

  @DefColumn("varchar", { name: "shortText1", nullable: true, length: 50 })
  shortText1: string | null;

  @DefColumn("varchar", { name: "shortText2", nullable: true, length: 50 })
  shortText2: string | null;

  @DefColumn("varchar", { name: "shortText3", nullable: true, length: 50 })
  shortText3: string | null;

  @DefColumn("varchar", { name: "longText1", nullable: true, length: 200 })
  longText1: string | null;

  @DefColumn("varchar", { name: "longText2", nullable: true, length: 200 })
  longText2: string | null;

  @DefColumn("varchar", { name: "longText3", nullable: true, length: 200 })
  longText3: string | null;

  @DefColumn("longtext", { name: "extraText1", nullable: true })
  extraText1: string | null;

  @DefColumn("longtext", { name: "extraText2", nullable: true })
  extraText2: string | null;

  @DefColumn("longtext", { name: "extraText3", nullable: true })
  extraText3: string | null;

  @DefColumn("double", { name: "number1", nullable: true, precision: 22 })
  number1: number | null;

  @DefColumn("double", { name: "number2", nullable: true, precision: 22 })
  number2: number | null;

  @DefColumn("double", { name: "number3", nullable: true, precision: 22 })
  number3: number | null;

  @DefColumn("double", { name: "number4", nullable: true, precision: 22 })
  number4: number | null;

  @DefColumn("datetime", { name: "date1", nullable: true })
  date1: Date | null;

  @DefColumn("datetime", { name: "date2", nullable: true })
  date2: Date | null;

  @DefColumn("datetime", { name: "date3", nullable: true })
  date3: Date | null;

  @DefColumn("datetime", { name: "date4", nullable: true })
  date4: Date | null;

  @DefColumn("tinyint", { name: "flag1", nullable: true, width: 1 })
  flag1: boolean | null;

  @DefColumn("tinyint", { name: "flag2", nullable: true, width: 1 })
  flag2: boolean | null;

  @DefColumn("tinyint", { name: "flag3", nullable: true, width: 1 })
  flag3: boolean | null;

  @DefColumn("tinyint", { name: "flag4", nullable: true, width: 1 })
  flag4: boolean | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;
}
