import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Tripdetail } from "./tripdetail";

@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("IX_Trip", ["code"], {})
@Entity("trip", { schema: "swm" })
export class Trip {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "code", length: 50 })
  code: string;

  @DefColumn("varchar", { name: "carrierCode", length: 65 })
  carrierCode: string;

  @DefColumn("varchar", { name: "driverCode", length: 65 })
  driverCode: string;

  @DefColumn("varchar", { name: "driverName", length: 200 })
  driverName: string;

  @DefColumn("varchar", { name: "driverPhone", length: 20 })
  driverPhone: string;

  @DefColumn("datetime", { name: "ETD" })
  etd: Date;

  @DefColumn("datetime", { name: "ETA" })
  eta: Date;

  @DefColumn("datetime", { name: "ATD" })
  atd: Date;

  @DefColumn("datetime", { name: "ATA" })
  ata: Date;

  @DefColumn("varchar", { name: "plateNumber", nullable: true, length: 50 })
  plateNumber: string | null;

  @DefColumn("varchar", { name: "status", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("varchar", { name: "note1", nullable: true, length: 200 })
  note1: string | null;

  @DefColumn("varchar", { name: "note2", nullable: true, length: 200 })
  note2: string | null;

  @DefColumn("varchar", { name: "note3", nullable: true, length: 200 })
  note3: string | null;

  @DefColumn("varchar", { name: "note4", nullable: true, length: 200 })
  note4: string | null;

  @DefColumn("varchar", { name: "note5", nullable: true, length: 200 })
  note5: string | null;

  @DefColumn("varchar", { name: "clientid", length: 50 })
  clientid: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @OneToMany(
    () => Tripdetail,
    tripdetail => tripdetail.trip
  )
  tripdetails: Tripdetail[];
}
