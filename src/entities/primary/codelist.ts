import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("codelist", { schema: "swm" })
export class Codelist {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "LISTNAME", length: 25 })
  listname: string;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 60 })
  description: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
