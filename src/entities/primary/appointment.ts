import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("appointment", { schema: "swm" })
export class Appointment {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "id" })
  id: string;

  @DefColumn("varchar", { name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "storerkey", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "appointmentkey", length: 15 })
  appointmentkey: string;

  @DefColumn("varchar", { name: "appointmenttype", nullable: true, length: 10 })
  appointmenttype: string | null;

  @DefColumn("varchar", { name: "pokey", nullable: true, length: 100 })
  pokey: string | null;

  @DefColumn("varchar", { name: "sokey", nullable: true, length: 100 })
  sokey: string | null;

  @DefColumn("varchar", { name: "notes", nullable: true, length: 200 })
  notes: string | null;

  @DefColumn("datetime", { name: "appointmentdate", nullable: true })
  appointmentdate: Date | null;

  @DefColumn("varchar", {
    name: "suggestedduration",
    nullable: true,
    length: 10
  })
  suggestedduration: string | null;

  @DefColumn("datetime", { name: "starttime", nullable: true })
  starttime: Date | null;

  @DefColumn("datetime", { name: "endtime", nullable: true })
  endtime: Date | null;

  @DefColumn("varchar", { name: "carriercode", nullable: true, length: 40 })
  carriercode: string | null;

  @DefColumn("varchar", { name: "billofloading", nullable: true, length: 100 })
  billofloading: string | null;

  @DefColumn("varchar", { name: "truckno", nullable: true, length: 100 })
  truckno: string | null;

  @DefColumn("varchar", { name: "driverno", nullable: true, length: 30 })
  driverno: string | null;

  @DefColumn("varchar", { name: "drivername", nullable: true, length: 200 })
  drivername: string | null;

  @DefColumn("varchar", { name: "filename", nullable: true, length: 500 })
  filename: string | null;

  @DefColumn("varchar", { name: "notes2", nullable: true, length: 500 })
  notes2: string | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", {
    name: "appointmentstatus",
    nullable: true,
    length: 2
  })
  appointmentstatus: string | null;

  @DefColumn("varchar", { name: "customercode", nullable: true, length: 30 })
  customercode: string | null;

  @DefColumn("varchar", { name: "customername", nullable: true, length: 300 })
  customername: string | null;

  @DefColumn("datetime", { name: "timein", nullable: true })
  timein: Date | null;

  @DefColumn("datetime", { name: "timeindock", nullable: true })
  timeindock: Date | null;

  @DefColumn("datetime", { name: "timeout", nullable: true })
  timeout: Date | null;

  @DefColumn("tinyint", { name: "deleted", nullable: true })
  deleted: number | null;

  @DefColumn("varchar", { name: "containerno", nullable: true, length: 50 })
  containerno: string | null;
}
