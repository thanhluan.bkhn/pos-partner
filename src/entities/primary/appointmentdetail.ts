import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("appointmentdetail", { schema: "swm" })
export class Appointmentdetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @DefColumn("varchar", { name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "storerkey", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "appointmentkey", length: 30 })
  appointmentkey: string;

  @DefColumn("varchar", { name: "sku", nullable: true, length: 50 })
  sku: string | null;

  @DefColumn("decimal", {
    name: "qty",
    nullable: true,
    precision: 18,
    scale: 4
  })
  qty: string | null;

  @DefColumn("decimal", {
    name: "netweight",
    nullable: true,
    precision: 18,
    scale: 4
  })
  netweight: string | null;

  @DefColumn("decimal", {
    name: "grossweight",
    nullable: true,
    precision: 18,
    scale: 4
  })
  grossweight: string | null;

  @DefColumn("decimal", {
    name: "cube",
    nullable: true,
    precision: 18,
    scale: 4
  })
  cube: string | null;

  @DefColumn("varchar", { name: "description", nullable: true, length: 200 })
  description: string | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", { name: "deleted", nullable: true })
  deleted: number | null;
}
