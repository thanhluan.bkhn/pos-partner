import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("premoveheader", { schema: "swm" })
export class Premoveheader {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 65 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "MOVECODE", length: 50 })
  movecode: string;

  @DefColumn("varchar", { name: "SOURCETYPE", nullable: true, length: 45 })
  sourcetype: string | null;

  @DefColumn("int", { name: "STATUS" })
  status: number;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", { name: "DELETED", nullable: true })
  deleted: number | null;
}
