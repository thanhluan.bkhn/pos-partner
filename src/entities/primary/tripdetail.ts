import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Order } from "./order";
import { Trip } from "./trip";

@Index("IX_TripDetail", ["tripId", "orderId", "orderLineNumber"], {
  unique: true
})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("FK_TripDetail_Order_orderId", ["orderId"], {})
@Entity("tripdetail", { schema: "swm" })
export class Tripdetail {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "tripId", length: 36 })
  tripId: string;

  @DefColumn("char", { name: "orderId", length: 36 })
  orderId: string;

  @DefColumn("varchar", { name: "orderLineNumber", length: 5 })
  orderLineNumber: string;

  @DefColumn("decimal", { name: "originalQty", precision: 18, scale: 2 })
  originalQty: string;

  @DefColumn("decimal", { name: "netWeight", precision: 18, scale: 2 })
  netWeight: string;

  @DefColumn("decimal", { name: "grossWeight", precision: 18, scale: 2 })
  grossWeight: string;

  @DefColumn("decimal", { name: "cube", precision: 18, scale: 2 })
  cube: string;

  @DefColumn("varchar", { name: "status", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("datetime", { name: "ATD" })
  atd: Date;

  @DefColumn("datetime", { name: "ATA" })
  ata: Date;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @ManyToOne(
    () => Order,
    order => order.tripdetails,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "orderId", referencedColumnName: "id" }])
  order: Order;

  @ManyToOne(
    () => Trip,
    trip => trip.tripdetails,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "tripId", referencedColumnName: "id" }])
  trip: Trip;
}
