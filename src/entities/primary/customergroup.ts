import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("customergroup", { schema: "swm" })
export class Customergroup {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 20 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "GROUPCODE", length: 40 })
  groupcode: string;

  @DefColumn("varchar", { name: "GROUPNAME", nullable: true, length: 200 })
  groupname: string | null;

  @DefColumn("int", { name: "SHELFLIFE", nullable: true, default: () => "'0'" })
  shelflife: number | null;

  @DefColumn("varchar", { name: "ADDRESS", nullable: true, length: 200 })
  address: string | null;

  @DefColumn("varchar", { name: "PHONENUMBER", nullable: true, length: 45 })
  phonenumber: string | null;

  @DefColumn("varchar", { name: "CONTACT", nullable: true, length: 45 })
  contact: string | null;

  @DefColumn("varchar", { name: "VALUE1", nullable: true, length: 10 })
  value1: string | null;

  @DefColumn("varchar", { name: "VALUE2", nullable: true, length: 20 })
  value2: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
