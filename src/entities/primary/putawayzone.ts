import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("PUT_225_UNIQUE", ["id"], { unique: true })
@Entity("putawayzone", { schema: "swm" })
export class Putawayzone {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "PUTAWAYZONE", length: 10 })
  putawayzone: string;

  @DefColumn("varchar", { name: "DESCR", nullable: true, length: 60 })
  descr: string | null;

  @DefColumn("varchar", { name: "INLOC", nullable: true, length: 10 })
  inloc: string | null;

  @DefColumn("varchar", { name: "OUTLOC", nullable: true, length: 10 })
  outloc: string | null;

  @DefColumn("varchar", { name: "UOM1PICKMETHOD", nullable: true, length: 1 })
  uom1Pickmethod: string | null;

  @DefColumn("varchar", { name: "UOM2PICKMETHOD", nullable: true, length: 1 })
  uom2Pickmethod: string | null;

  @DefColumn("varchar", { name: "UOM3PICKMETHOD", nullable: true, length: 1 })
  uom3Pickmethod: string | null;

  @DefColumn("varchar", { name: "UOM4PICKMETHOD", nullable: true, length: 1 })
  uom4Pickmethod: string | null;

  @DefColumn("varchar", { name: "UOM5PICKMETHOD", nullable: true, length: 1 })
  uom5Pickmethod: string | null;

  @DefColumn("varchar", { name: "UOM6PICKMETHOD", nullable: true, length: 1 })
  uom6Pickmethod: string | null;

  @DefColumn("varchar", { name: "CLEAN_LOCATION", nullable: true, length: 1 })
  cleanLocation: string | null;

  @DefColumn("varchar", { name: "TOP_OFF", nullable: true, length: 1 })
  topOff: string | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENT_FLAG",
    nullable: true,
    length: 1
  })
  replenishmentFlag: string | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENT_HOTLEVEL",
    nullable: true,
    length: 1
  })
  replenishmentHotlevel: string | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENTMETHOD",
    nullable: true,
    length: 10
  })
  replenishmentmethod: string | null;

  @DefColumn("double", {
    name: "MAXCUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  maxcube: number | null;

  @DefColumn("double", {
    name: "MAXWEIGHT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  maxweight: number | null;

  @DefColumn("decimal", {
    name: "MAXCASECNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  maxcasecnt: string | null;

  @DefColumn("varchar", { name: "PICKTOLOC", nullable: true, length: 10 })
  picktoloc: string | null;

  @DefColumn("int", {
    name: "MaxPalletsPerSku",
    nullable: true,
    default: () => "'0'"
  })
  maxPalletsPerSku: number | null;

  @DefColumn("varchar", { name: "POSVERMETHOD", nullable: true, length: 10 })
  posvermethod: string | null;

  @DefColumn("varchar", { name: "PICKCC", nullable: true, length: 1 })
  pickcc: string | null;

  @DefColumn("decimal", {
    name: "QTYCC",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtycc: string | null;

  @DefColumn("varchar", {
    name: "CREATEASSIGNMENTS",
    nullable: true,
    length: 1
  })
  createassignments: string | null;

  @DefColumn("varchar", { name: "ZONEBREAK", nullable: true, length: 1 })
  zonebreak: string | null;

  @DefColumn("int", {
    name: "MAXPICKLINES",
    nullable: true,
    default: () => "'0'"
  })
  maxpicklines: number | null;

  @DefColumn("int", {
    name: "MAXPICKINGCONTAINERS",
    nullable: true,
    default: () => "'0'"
  })
  maxpickingcontainers: number | null;

  @DefColumn("int", {
    name: "LABORMAXCASECNT",
    nullable: true,
    default: () => "'0'"
  })
  labormaxcasecnt: number | null;

  @DefColumn("int", {
    name: "LABORMAXCUBE",
    nullable: true,
    default: () => "'0'"
  })
  labormaxcube: number | null;

  @DefColumn("int", {
    name: "LABORMAXWEIGHT",
    nullable: true,
    default: () => "'0'"
  })
  labormaxweight: number | null;

  @DefColumn("int", {
    name: "AISLESTART",
    nullable: true,
    default: () => "'0'"
  })
  aislestart: number | null;

  @DefColumn("int", { name: "AISLEEND", nullable: true, default: () => "'0'" })
  aisleend: number | null;

  @DefColumn("int", { name: "SLOTSTART", nullable: true, default: () => "'0'" })
  slotstart: number | null;

  @DefColumn("int", { name: "SLOTEND", nullable: true, default: () => "'0'" })
  slotend: number | null;

  @DefColumn("varchar", {
    name: "AUTOPRINTADDRLABEL",
    nullable: true,
    length: 1
  })
  autoprintaddrlabel: string | null;

  @DefColumn("varchar", {
    name: "AUTOPRINTCARTONCONTENT",
    nullable: true,
    length: 1
  })
  autoprintcartoncontent: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTLABELPRINTER",
    nullable: true,
    length: 45
  })
  defaultlabelprinter: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTREPORTPRINTER",
    nullable: true,
    length: 45
  })
  defaultreportprinter: string | null;

  @DefColumn("varchar", { name: "ABANDONLOC", nullable: true, length: 10 })
  abandonloc: string | null;

  @DefColumn("tinyint", {
    name: "SETDEFAULT",
    nullable: true,
    default: () => "'0'"
  })
  setdefault: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", { name: "PICKLEVELMETHOD", nullable: true, length: 45 })
  picklevelmethod: string | null;
}
