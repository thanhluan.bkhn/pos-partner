import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("replenishmentstrategydetail", { schema: "swm" })
export class Replenishmentstrategydetail {
  @DefColumn("varchar", { name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "replenishmentstrategykey",
    length: 30
  })
  replenishmentstrategykey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "replenishmentstrategylinenumber",
    length: 10
  })
  replenishmentstrategylinenumber: string;

  @DefColumn("varchar", { name: "descr", length: 200 })
  descr: string;

  @DefColumn("varchar", { name: "uom", length: 10 })
  uom: string;

  @DefColumn("varchar", { name: "fromloc", nullable: true, length: 100 })
  fromloc: string | null;

  @DefColumn("varchar", { name: "toloc", nullable: true, length: 100 })
  toloc: string | null;

  @DefColumn("varchar", { name: "fromzone", nullable: true, length: 100 })
  fromzone: string | null;

  @DefColumn("varchar", { name: "tozone", nullable: true, length: 100 })
  tozone: string | null;

  @DefColumn("double", { name: "stepnumber", precision: 22 })
  stepnumber: number;

  @DefColumn("datetime", { name: "adddate" })
  adddate: Date;

  @DefColumn("varchar", { name: "addwho", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 50 })
  editwho: string | null;
}
