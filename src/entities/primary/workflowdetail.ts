import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WORKFLOWID_UNIQUE", ["workflowid", "actionid"], { unique: true })
@Entity("workflowdetail", { schema: "swm" })
export class Workflowdetail {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("char", { name: "WORKFLOWID", length: 36 })
  workflowid: string;

  @DefColumn("char", { name: "ACTIONID", length: 36 })
  actionid: string;

  @DefColumn("int", { name: "STEP" })
  step: number;

  @DefColumn("tinyint", { name: "STATUS" })
  status: number;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;
}
