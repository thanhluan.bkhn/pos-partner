import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WHSEID_UNIQUE", ["whseid", "stockcountcode", "originalrowno"], {
  unique: true
})
@Entity("stockcountdetail", { schema: "swm" })
export class Stockcountdetail {
  @DefColumn("varchar", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 50 })
  whseid: string;

  @DefColumn("varchar", { name: "STOCKCOUNTCODE", length: 11 })
  stockcountcode: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("int", { name: "ORIGINALROWNO" })
  originalrowno: number;

  @DefColumn("int", { name: "ROWNO" })
  rowno: number;

  @DefColumn("varchar", { name: "LOCATION", nullable: true, length: 50 })
  location: string | null;

  @DefColumn("varchar", { name: "SKU", length: 65 })
  sku: string;

  @DefColumn("decimal", {
    name: "QTYCASE",
    nullable: true,
    precision: 18,
    scale: 0,
    default: () => "'0'"
  })
  qtycase: string | null;

  @DefColumn("decimal", {
    name: "QTYPIECE",
    nullable: true,
    precision: 18,
    scale: 0,
    default: () => "'0'"
  })
  qtypiece: string | null;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 100 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 100 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 100 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 100 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 100 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 100 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 100 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 100 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("varchar", { name: "UNITID", length: 30 })
  unitid: string;

  @DefColumn("varchar", { name: "CARTONID", length: 30 })
  cartonid: string;

  @DefColumn("varchar", { name: "PALLETID", length: 30 })
  palletid: string;

  @DefColumn("varchar", { name: "SKU_1", nullable: true, length: 50 })
  sku_1: string | null;

  @DefColumn("decimal", {
    name: "QTYCASE_1",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtycase_1: string | null;

  @DefColumn("varchar", { name: "STATUS_1", nullable: true, length: 10 })
  status_1: string | null;

  @DefColumn("decimal", {
    name: "QTYPIECE_1",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtypiece_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01_1", nullable: true, length: 100 })
  lottable01_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02_1", nullable: true, length: 100 })
  lottable02_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03_1", nullable: true, length: 100 })
  lottable03_1: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04_1", nullable: true })
  lottable04_1: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05_1", nullable: true })
  lottable05_1: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06_1", nullable: true, length: 100 })
  lottable06_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07_1", nullable: true, length: 100 })
  lottable07_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08_1", nullable: true, length: 100 })
  lottable08_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09_1", nullable: true, length: 100 })
  lottable09_1: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10_1", nullable: true, length: 100 })
  lottable10_1: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11_1", nullable: true })
  lottable11_1: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12_1", nullable: true })
  lottable12_1: Date | null;

  @DefColumn("varchar", { name: "UNITID_1", nullable: true, length: 30 })
  unitid_1: string | null;

  @DefColumn("varchar", { name: "CARTONID_1", nullable: true, length: 30 })
  cartonid_1: string | null;

  @DefColumn("varchar", { name: "PALLETID_1", nullable: true, length: 30 })
  palletid_1: string | null;

  @DefColumn("datetime", { name: "KEYDATE1", nullable: true })
  keydate1: Date | null;

  @DefColumn("varchar", { name: "KEYWHO1", nullable: true, length: 50 })
  keywho1: string | null;

  @DefColumn("varchar", { name: "SKU_2", nullable: true, length: 50 })
  sku_2: string | null;

  @DefColumn("decimal", {
    name: "QTYCASE_2",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtycase_2: string | null;

  @DefColumn("varchar", { name: "STATUS_2", nullable: true, length: 10 })
  status_2: string | null;

  @DefColumn("decimal", {
    name: "QTYPIECE_2",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtypiece_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01_2", nullable: true, length: 100 })
  lottable01_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02_2", nullable: true, length: 100 })
  lottable02_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03_2", nullable: true, length: 100 })
  lottable03_2: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04_2", nullable: true })
  lottable04_2: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05_2", nullable: true })
  lottable05_2: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06_2", nullable: true, length: 100 })
  lottable06_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07_2", nullable: true, length: 100 })
  lottable07_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08_2", nullable: true, length: 100 })
  lottable08_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09_2", nullable: true, length: 100 })
  lottable09_2: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10_2", nullable: true, length: 100 })
  lottable10_2: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11_2", nullable: true })
  lottable11_2: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12_2", nullable: true })
  lottable12_2: Date | null;

  @DefColumn("varchar", { name: "UNITID_2", nullable: true, length: 30 })
  unitid_2: string | null;

  @DefColumn("varchar", { name: "CARTONID_2", nullable: true, length: 30 })
  cartonid_2: string | null;

  @DefColumn("varchar", { name: "PALLETID_2", nullable: true, length: 30 })
  palletid_2: string | null;

  @DefColumn("datetime", { name: "KEYDATE2", nullable: true })
  keydate2: Date | null;

  @DefColumn("varchar", { name: "KEYWHO2", nullable: true, length: 50 })
  keywho2: string | null;

  @DefColumn("varchar", { name: "SKU_3", nullable: true, length: 50 })
  sku_3: string | null;

  @DefColumn("decimal", {
    name: "QTYCASE_3",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtycase_3: string | null;

  @DefColumn("varchar", { name: "STATUS_3", nullable: true, length: 10 })
  status_3: string | null;

  @DefColumn("decimal", {
    name: "QTYPIECE_3",
    nullable: true,
    precision: 18,
    scale: 0
  })
  qtypiece_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01_3", nullable: true, length: 100 })
  lottable01_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02_3", nullable: true, length: 100 })
  lottable02_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03_3", nullable: true, length: 100 })
  lottable03_3: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04_3", nullable: true })
  lottable04_3: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05_3", nullable: true })
  lottable05_3: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06_3", nullable: true, length: 100 })
  lottable06_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07_3", nullable: true, length: 100 })
  lottable07_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08_3", nullable: true, length: 100 })
  lottable08_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09_3", nullable: true, length: 100 })
  lottable09_3: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10_3", nullable: true, length: 100 })
  lottable10_3: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11_3", nullable: true })
  lottable11_3: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12_3", nullable: true })
  lottable12_3: Date | null;

  @DefColumn("varchar", { name: "UNITID_3", nullable: true, length: 30 })
  unitid_3: string | null;

  @DefColumn("varchar", { name: "CARTONID_3", nullable: true, length: 30 })
  cartonid_3: string | null;

  @DefColumn("varchar", { name: "PALLETID_3", nullable: true, length: 30 })
  palletid_3: string | null;

  @DefColumn("datetime", { name: "KEYDATE3", nullable: true })
  keydate3: Date | null;

  @DefColumn("varchar", { name: "KEYWHO3", nullable: true, length: 50 })
  keywho3: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;
}
