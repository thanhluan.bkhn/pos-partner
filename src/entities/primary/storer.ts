import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("STO_252_UNIQUE", ["id"], { unique: true })
@Index("IX_WHSEID_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_STORERKEY_TYPE", ["whseid", "storerkey", "type"], {})
@Entity("storer", { schema: "swm" })
export class Storer {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "TYPE", length: 30 })
  type: string;

  @DefColumn("int", {
    name: "SOURCEVERSION",
    nullable: true,
    default: () => "'0'"
  })
  sourceversion: number | null;

  @DefColumn("varchar", { name: "COMPANY", nullable: true, length: 200 })
  company: string | null;

  @DefColumn("varchar", { name: "VAT", nullable: true, length: 18 })
  vat: string | null;

  @DefColumn("varchar", { name: "ADDRESS1", nullable: true, length: 200 })
  address1: string | null;

  @DefColumn("varchar", { name: "ADDRESS2", nullable: true, length: 200 })
  address2: string | null;

  @DefColumn("varchar", { name: "ADDRESS3", nullable: true, length: 45 })
  address3: string | null;

  @DefColumn("varchar", { name: "ADDRESS4", nullable: true, length: 45 })
  address4: string | null;

  @DefColumn("varchar", { name: "CITY", nullable: true, length: 45 })
  city: string | null;

  @DefColumn("varchar", { name: "STATE", nullable: true, length: 25 })
  state: string | null;

  @DefColumn("varchar", { name: "ZIP", nullable: true, length: 18 })
  zip: string | null;

  @DefColumn("varchar", { name: "COUNTRY", nullable: true, length: 30 })
  country: string | null;

  @DefColumn("varchar", { name: "ISOCNTRYCODE", nullable: true, length: 10 })
  isocntrycode: string | null;

  @DefColumn("varchar", { name: "CONTACT1", nullable: true, length: 200 })
  contact1: string | null;

  @DefColumn("varchar", { name: "CONTACT2", nullable: true, length: 30 })
  contact2: string | null;

  @DefColumn("varchar", { name: "PHONE1", nullable: true, length: 50 })
  phone1: string | null;

  @DefColumn("varchar", { name: "PHONE2", nullable: true, length: 50 })
  phone2: string | null;

  @DefColumn("varchar", { name: "FAX1", nullable: true, length: 18 })
  fax1: string | null;

  @DefColumn("varchar", { name: "FAX2", nullable: true, length: 18 })
  fax2: string | null;

  @DefColumn("varchar", { name: "EMAIL1", nullable: true, length: 60 })
  email1: string | null;

  @DefColumn("varchar", { name: "EMAIL2", nullable: true, length: 60 })
  email2: string | null;

  @DefColumn("varchar", { name: "B_CONTACT1", nullable: true, length: 30 })
  bContact1: string | null;

  @DefColumn("varchar", { name: "B_CONTACT2", nullable: true, length: 30 })
  bContact2: string | null;

  @DefColumn("varchar", { name: "B_COMPANY", nullable: true, length: 45 })
  bCompany: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS1", nullable: true, length: 45 })
  bAddress1: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS2", nullable: true, length: 45 })
  bAddress2: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS3", nullable: true, length: 45 })
  bAddress3: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS4", nullable: true, length: 45 })
  bAddress4: string | null;

  @DefColumn("varchar", { name: "B_CITY", nullable: true, length: 45 })
  bCity: string | null;

  @DefColumn("varchar", { name: "B_STATE", nullable: true, length: 25 })
  bState: string | null;

  @DefColumn("varchar", { name: "B_ZIP", nullable: true, length: 18 })
  bZip: string | null;

  @DefColumn("varchar", { name: "B_COUNTRY", nullable: true, length: 30 })
  bCountry: string | null;

  @DefColumn("varchar", { name: "B_ISOCNTRYCODE", nullable: true, length: 10 })
  bIsocntrycode: string | null;

  @DefColumn("varchar", { name: "B_PHONE1", nullable: true, length: 50 })
  bPhone1: string | null;

  @DefColumn("varchar", { name: "B_PHONE2", nullable: true, length: 50 })
  bPhone2: string | null;

  @DefColumn("varchar", { name: "B_FAX1", nullable: true, length: 18 })
  bFax1: string | null;

  @DefColumn("varchar", { name: "B_FAX2", nullable: true, length: 18 })
  bFax2: string | null;

  @DefColumn("varchar", { name: "B_EMAIL1", nullable: true, length: 60 })
  bEmail1: string | null;

  @DefColumn("varchar", { name: "B_EMAIL2", nullable: true, length: 60 })
  bEmail2: string | null;

  @DefColumn("varchar", { name: "CREDITLIMIT", nullable: true, length: 18 })
  creditlimit: string | null;

  @DefColumn("varchar", { name: "CARTONGROUP", nullable: true, length: 10 })
  cartongroup: string | null;

  @DefColumn("varchar", { name: "PICKCODE", nullable: true, length: 10 })
  pickcode: string | null;

  @DefColumn("varchar", {
    name: "CREATEPATASKONRFRECEIPT",
    nullable: true,
    length: 10
  })
  createpataskonrfreceipt: string | null;

  @DefColumn("varchar", {
    name: "CALCULATEPUTAWAYLOCATION",
    nullable: true,
    length: 10
  })
  calculateputawaylocation: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 18 })
  status: string | null;

  @DefColumn("varchar", { name: "DEFAULTSTRATEGY", nullable: true, length: 10 })
  defaultstrategy: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTSKUROTATION",
    nullable: true,
    length: 10
  })
  defaultskurotation: string | null;

  @DefColumn("varchar", { name: "DEFAULTROTATION", nullable: true, length: 1 })
  defaultrotation: string | null;

  @DefColumn("varchar", { name: "SCAC_CODE", nullable: true, length: 4 })
  scacCode: string | null;

  @DefColumn("varchar", { name: "TITLE1", nullable: true, length: 50 })
  title1: string | null;

  @DefColumn("varchar", { name: "TITLE2", nullable: true, length: 50 })
  title2: string | null;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 50 })
  description: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "SUSR6", nullable: true, length: 30 })
  susr6: string | null;

  @DefColumn("varchar", { name: "MULTIZONEPLPA", nullable: true, length: 1 })
  multizoneplpa: string | null;

  @DefColumn("varchar", { name: "CWOFLAG", nullable: true, length: 1 })
  cwoflag: string | null;

  @DefColumn("varchar", { name: "ROLLRECEIPT", nullable: true, length: 1 })
  rollreceipt: string | null;

  @DefColumn("varchar", { name: "NOTES1", nullable: true, length: 3072 })
  notes1: string | null;

  @DefColumn("varchar", { name: "NOTES2", nullable: true, length: 3072 })
  notes2: string | null;

  @DefColumn("varchar", { name: "APPORTIONRULE", nullable: true, length: 10 })
  apportionrule: string | null;

  @DefColumn("varchar", { name: "ENABLEOPPXDOCK", nullable: true, length: 1 })
  enableoppxdock: string | null;

  @DefColumn("varchar", {
    name: "ALLOWOVERSHIPMENT",
    nullable: true,
    length: 1
  })
  allowovershipment: string | null;

  @DefColumn("int", {
    name: "MAXIMUMORDERS",
    nullable: true,
    default: () => "'0'"
  })
  maximumorders: number | null;

  @DefColumn("decimal", {
    name: "MINIMUMPERCENT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  minimumpercent: string | null;

  @DefColumn("int", {
    name: "ORDERDATESTARTDAYS",
    nullable: true,
    default: () => "'0'"
  })
  orderdatestartdays: number | null;

  @DefColumn("int", {
    name: "ORDERDATEENDDAYS",
    nullable: true,
    default: () => "'0'"
  })
  orderdateenddays: number | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT01",
    nullable: true,
    length: 10
  })
  ordertyperestrict01: string | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT02",
    nullable: true,
    length: 10
  })
  ordertyperestrict02: string | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT03",
    nullable: true,
    length: 10
  })
  ordertyperestrict03: string | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT04",
    nullable: true,
    length: 10
  })
  ordertyperestrict04: string | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT05",
    nullable: true,
    length: 10
  })
  ordertyperestrict05: string | null;

  @DefColumn("varchar", {
    name: "ORDERTYPERESTRICT06",
    nullable: true,
    length: 10
  })
  ordertyperestrict06: string | null;

  @DefColumn("varchar", {
    name: "OPPORDERSTRATEGYKEY",
    nullable: true,
    length: 10
  })
  opporderstrategykey: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTVALIDATIONTEMPLATE",
    nullable: true,
    length: 18
  })
  receiptvalidationtemplate: string | null;

  @DefColumn("varchar", {
    name: "ALLOWAUTOCLOSEFORPO",
    nullable: true,
    length: 1
  })
  allowautocloseforpo: string | null;

  @DefColumn("varchar", {
    name: "ALLOWAUTOCLOSEFORASN",
    nullable: true,
    length: 1
  })
  allowautocloseforasn: string | null;

  @DefColumn("varchar", {
    name: "ALLOWAUTOCLOSEFORPS",
    nullable: true,
    length: 1
  })
  allowautocloseforps: string | null;

  @DefColumn("varchar", {
    name: "ALLOWSYSTEMGENERATEDLPN",
    nullable: true,
    length: 1
  })
  allowsystemgeneratedlpn: string | null;

  @DefColumn("varchar", {
    name: "ALLOWDUPLICATELICENSEPLATES",
    nullable: true,
    length: 1
  })
  allowduplicatelicenseplates: string | null;

  @DefColumn("varchar", {
    name: "ALLOWCOMMINGLEDLPN",
    nullable: true,
    length: 1
  })
  allowcommingledlpn: string | null;

  @DefColumn("varchar", {
    name: "LPNBARCODESYMBOLOGY",
    nullable: true,
    length: 60
  })
  lpnbarcodesymbology: string | null;

  @DefColumn("varchar", {
    name: "LPNBARCODEFORMAT",
    nullable: true,
    length: 60
  })
  lpnbarcodeformat: string | null;

  @DefColumn("varchar", {
    name: "ALLOWSINGLESCANRECEIVING",
    nullable: true,
    length: 1
  })
  allowsinglescanreceiving: string | null;

  @DefColumn("int", { name: "LPNLENGTH", nullable: true, default: () => "'0'" })
  lpnlength: number | null;

  @DefColumn("varchar", { name: "APPLICATIONID", nullable: true, length: 2 })
  applicationid: string | null;

  @DefColumn("int", {
    name: "SSCC1STDIGIT",
    nullable: true,
    default: () => "'0'"
  })
  sscc1Stdigit: number | null;

  @DefColumn("varchar", { name: "UCCVENDORNUMBER", nullable: true, length: 9 })
  uccvendornumber: string | null;

  @DefColumn("varchar", { name: "CaseLabelType", nullable: true, length: 10 })
  caseLabelType: string | null;

  @DefColumn("varchar", {
    name: "AUTOPRINTLABELLPN",
    nullable: true,
    length: 1
  })
  autoprintlabellpn: string | null;

  @DefColumn("varchar", {
    name: "AUTOPRINTLABELPUTAWAY",
    nullable: true,
    length: 1
  })
  autoprintlabelputaway: string | null;

  @DefColumn("varchar", { name: "LPNSTARTNUMBER", nullable: true, length: 18 })
  lpnstartnumber: string | null;

  @DefColumn("varchar", { name: "NEXTLPNNUMBER", nullable: true, length: 18 })
  nextlpnnumber: string | null;

  @DefColumn("varchar", {
    name: "LPNROLLBACKNUMBER",
    nullable: true,
    length: 18
  })
  lpnrollbacknumber: string | null;

  @DefColumn("varchar", {
    name: "BARCODECONFIGKEY",
    nullable: true,
    length: 18
  })
  barcodeconfigkey: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTPUTAWAYSTRATEGY",
    nullable: true,
    length: 10
  })
  defaultputawaystrategy: string | null;

  @DefColumn("varchar", { name: "AUTOCLOSEASN", nullable: true, length: 1 })
  autocloseasn: string | null;

  @DefColumn("varchar", { name: "AUTOCLOSEPO", nullable: true, length: 1 })
  autoclosepo: string | null;

  @DefColumn("varchar", { name: "TRACKINVENTORYBY", nullable: true, length: 1 })
  trackinventoryby: string | null;

  @DefColumn("varchar", { name: "DUPCASEID", nullable: true, length: 1 })
  dupcaseid: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTRETURNSLOC",
    nullable: true,
    length: 10
  })
  defaultreturnsloc: string | null;

  @DefColumn("varchar", { name: "DEFAULTQCLOC", nullable: true, length: 10 })
  defaultqcloc: string | null;

  @DefColumn("varchar", { name: "PISKUXLOC", nullable: true, length: 1 })
  piskuxloc: string | null;

  @DefColumn("varchar", { name: "CCSKUXLOC", nullable: true, length: 1 })
  ccskuxloc: string | null;

  @DefColumn("varchar", {
    name: "CCDISCREPANCYRULE",
    nullable: true,
    length: 10
  })
  ccdiscrepancyrule: string | null;

  @DefColumn("varchar", { name: "CCADJBYRF", nullable: true, length: 10 })
  ccadjbyrf: string | null;

  @DefColumn("varchar", {
    name: "ORDERBREAKDEFAULT",
    nullable: true,
    length: 1
  })
  orderbreakdefault: string | null;

  @DefColumn("varchar", { name: "SKUSETUPREQUIRED", nullable: true, length: 1 })
  skusetuprequired: string | null;

  @DefColumn("varchar", { name: "DEFAULTQCLOCOUT", nullable: true, length: 10 })
  defaultqclocout: string | null;

  @DefColumn("varchar", {
    name: "ENABLEPACKINGDEFAULT",
    nullable: true,
    length: 1
  })
  enablepackingdefault: string | null;

  @DefColumn("varchar", {
    name: "DEFAULTPACKINGLOCATION",
    nullable: true,
    length: 10
  })
  defaultpackinglocation: string | null;

  @DefColumn("varchar", { name: "GENERATEPACKLIST", nullable: true, length: 1 })
  generatepacklist: string | null;

  @DefColumn("varchar", {
    name: "PACKINGVALIDATIONTEMPLATE",
    nullable: true,
    length: 10
  })
  packingvalidationtemplate: string | null;

  @DefColumn("varchar", { name: "INSPECTATPACK", nullable: true, length: 1 })
  inspectatpack: string | null;

  @DefColumn("varchar", {
    name: "ADDRESSOVERWRITEINDICATOR",
    nullable: true,
    length: 1
  })
  addressoverwriteindicator: string | null;

  @DefColumn("varchar", {
    name: "ACCOUNTINGENTITY",
    nullable: true,
    length: 64
  })
  accountingentity: string | null;

  @DefColumn("int", {
    name: "CREATEOPPXDTASKS",
    nullable: true,
    default: () => "'0'"
  })
  createoppxdtasks: number | null;

  @DefColumn("int", {
    name: "ISSUEOPPXDTASKS",
    nullable: true,
    default: () => "'0'"
  })
  issueoppxdtasks: number | null;

  @DefColumn("varchar", { name: "OPPXDPICKFROM", nullable: true, length: 20 })
  oppxdpickfrom: string | null;

  @DefColumn("varchar", { name: "OBXDSTAGE", nullable: true, length: 20 })
  obxdstage: string | null;

  @DefColumn("int", {
    name: "KSHIP_CARRIER",
    nullable: true,
    default: () => "'0'"
  })
  kshipCarrier: number | null;

  @DefColumn("varchar", { name: "SPSUOMWEIGHT", nullable: true, length: 10 })
  spsuomweight: string | null;

  @DefColumn("varchar", { name: "SPSUOMDIMENSION", nullable: true, length: 10 })
  spsuomdimension: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("varchar", { name: "GROUPCODE", nullable: true, length: 100 })
  groupcode: string | null;

  @DefColumn("tinyint", {
    name: "LOTEXPORT",
    nullable: true,
    default: () => "'0'"
  })
  lotexport: number | null;

  @DefColumn("tinyint", {
    name: "UPCCODE",
    nullable: true,
    default: () => "'0'"
  })
  upccode: number | null;

  @DefColumn("tinyint", {
    name: "GROUPOFCARGO",
    nullable: true,
    default: () => "'0'"
  })
  groupofcargo: number | null;

  @DefColumn("tinyint", {
    name: "PRIORITYUOM",
    nullable: true,
    default: () => "'0'"
  })
  priorityuom: number | null;

  @DefColumn("tinyint", {
    name: "STOCKCOUNT_NSX",
    nullable: true,
    default: () => "'0'"
  })
  stockcountNsx: number | null;

  @DefColumn("tinyint", {
    name: "STOCKCOUNT_HSD",
    nullable: true,
    default: () => "'0'"
  })
  stockcountHsd: number | null;

  @DefColumn("varchar", { name: "STOCKINFO", nullable: true, length: 20 })
  stockinfo: string | null;

  @DefColumn("varchar", { name: "FREEDAY", nullable: true, length: 20 })
  freeday: string | null;

  @DefColumn("varchar", {
    name: "ALLOCATESTRATEGYKEY",
    nullable: true,
    length: 10
  })
  allocatestrategykey: string | null;

  @DefColumn("int", { name: "AGEINDAYS", nullable: true, default: () => "'0'" })
  ageindays: number | null;

  @DefColumn("varchar", { name: "LOTTABLE", nullable: true, length: 3072 })
  lottable: string | null;

  @DefColumn("varchar", { name: "COUNTLOTTABLE", nullable: true, length: 3072 })
  countlottable: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", {
    name: "DECLARATIONPLACE",
    nullable: true,
    length: 45
  })
  declarationplace: string | null;

  @DefColumn("varchar", { name: "DECLARATIONTYPE", nullable: true, length: 45 })
  declarationtype: string | null;

  @DefColumn("varchar", { name: "TAXCODE", nullable: true, length: 45 })
  taxcode: string | null;

  @DefColumn("varchar", { name: "DIVISION", nullable: true, length: 45 })
  division: string | null;
}
