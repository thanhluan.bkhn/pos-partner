import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("id_UNIQUE", ["id"], { unique: true })
@Entity("altsku", { schema: "swm" })
export class Altsku {
  @DefColumn("varchar", { name: "id", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "storerkey", length: 45 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "sku", length: 45 })
  sku: string;

  @DefColumn("varchar", { primary: true, name: "upc", length: 65 })
  upc: string;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;
}
