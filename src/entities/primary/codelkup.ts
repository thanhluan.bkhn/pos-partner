import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_LISTNAME", ["whseid", "listname"], {})
@Entity("codelkup", { schema: "swm" })
export class Codelkup {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "LISTNAME", length: 50 })
  listname: string;

  @DefColumn("varchar", { primary: true, name: "CODE", length: 50 })
  code: string;

  @DefColumn("varchar", { name: "CODEFORGENERATOR", nullable: true, length: 5 })
  codeforgenerator: string | null;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 250 })
  description: string | null;

  @DefColumn("varchar", { name: "SHORT", nullable: true, length: 30 })
  short: string | null;

  @DefColumn("varchar", { name: "LONG_VALUE", nullable: true, length: 250 })
  longValue: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 3072 })
  notes: string | null;

  @DefColumn("int", { name: "WOFLAG", nullable: true, default: () => "'0'" })
  woflag: number | null;

  @DefColumn("int", { name: "LOT1", nullable: true, default: () => "'0'" })
  lot1: number | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
