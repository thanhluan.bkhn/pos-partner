import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index(
  "IX_WHSEID_STORERKEY_ORDERKEY_SUSR1_OUTVERIFY",
  ["whseid", "storerkey", "orderkey", "susr1", "outverify"],
  {}
)
@Index(
  "IX_WHSEID_STORERKEY_UNITID_SUSR1",
  ["whseid", "storerkey", "unitid", "susr1"],
  {}
)
@Entity("qcverify", { schema: "swm" })
export class Qcverify {
  @DefColumn("varchar", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "whseid", length: 45 })
  whseid: string;

  @DefColumn("varchar", { name: "storerkey", length: 45 })
  storerkey: string;

  @DefColumn("varchar", { name: "receiptkey", nullable: true, length: 45 })
  receiptkey: string | null;

  @DefColumn("varchar", {
    name: "externreceiptkey",
    nullable: true,
    length: 45
  })
  externreceiptkey: string | null;

  @DefColumn("varchar", { name: "orderkey", nullable: true, length: 45 })
  orderkey: string | null;

  @DefColumn("varchar", { name: "externorderkey", nullable: true, length: 45 })
  externorderkey: string | null;

  @DefColumn("varchar", { name: "palletid", nullable: true, length: 45 })
  palletid: string | null;

  @DefColumn("varchar", { name: "cartonid", nullable: true, length: 45 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "unitid", nullable: true, length: 45 })
  unitid: string | null;

  @DefColumn("int", { name: "inverify", nullable: true, default: () => "'0'" })
  inverify: number | null;

  @DefColumn("datetime", { name: "indate", nullable: true })
  indate: Date | null;

  @DefColumn("int", { name: "outverify", nullable: true, default: () => "'0'" })
  outverify: number | null;

  @DefColumn("datetime", { name: "outdate", nullable: true })
  outdate: Date | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", length: 45 })
  addwho: string;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("varchar", { name: "createfrom", nullable: true, length: 45 })
  createfrom: string | null;

  @DefColumn("varchar", { name: "inverifyfrom", nullable: true, length: 45 })
  inverifyfrom: string | null;

  @DefColumn("varchar", { name: "outverifyfrom", nullable: true, length: 45 })
  outverifyfrom: string | null;

  @DefColumn("varchar", { name: "susr1", nullable: true, length: 100 })
  susr1: string | null;

  @DefColumn("varchar", { name: "consigneekey", nullable: true, length: 100 })
  consigneekey: string | null;

  @DefColumn("varchar", { name: "orderlinenumber", nullable: true, length: 5 })
  orderlinenumber: string | null;

  @DefColumn("varchar", {
    name: "receiptlinenumber",
    nullable: true,
    length: 5
  })
  receiptlinenumber: string | null;
}
