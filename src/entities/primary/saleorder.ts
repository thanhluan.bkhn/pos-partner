import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("saleorder", { schema: "swm" })
export class Saleorder {
  @DefColumn("varchar", { name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "SALEORDERKEY", length: 10 })
  saleorderkey: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("tinyint", {
    name: "ALLOCATEONRECEIVED",
    nullable: true,
    width: 1,
    default: () => "'0'"
  })
  allocateonreceived: boolean | null;

  @DefColumn("varchar", {
    name: "EXTERNSALEORDERKEY",
    nullable: true,
    length: 32
  })
  externsaleorderkey: string | null;

  @DefColumn("datetime", { name: "SALEORDERDATE", nullable: true })
  saleorderdate: Date | null;

  @DefColumn("datetime", { name: "DELIVERYDATE", nullable: true })
  deliverydate: Date | null;

  @DefColumn("tinyint", {
    name: "PRIORITY",
    nullable: true,
    width: 1,
    default: () => "'0'"
  })
  priority: boolean | null;

  @DefColumn("varchar", { name: "CONSIGNEEKEY", nullable: true, length: 100 })
  consigneekey: string | null;

  @DefColumn("varchar", { name: "C_CONTACT1", nullable: true, length: 200 })
  cContact1: string | null;

  @DefColumn("varchar", { name: "C_CONTACT2", nullable: true, length: 30 })
  cContact2: string | null;

  @DefColumn("varchar", { name: "C_COMPANY", nullable: true, length: 200 })
  cCompany: string | null;

  @DefColumn("varchar", { name: "C_ADDRESS1", nullable: true, length: 200 })
  cAddress1: string | null;

  @DefColumn("varchar", { name: "C_ADDRESS2", nullable: true, length: 200 })
  cAddress2: string | null;

  @DefColumn("varchar", { name: "C_ADDRESS3", nullable: true, length: 45 })
  cAddress3: string | null;

  @DefColumn("varchar", { name: "C_ADDRESS4", nullable: true, length: 45 })
  cAddress4: string | null;

  @DefColumn("varchar", { name: "C_CITY", nullable: true, length: 200 })
  cCity: string | null;

  @DefColumn("varchar", { name: "C_STATE", nullable: true, length: 25 })
  cState: string | null;

  @DefColumn("varchar", { name: "C_ZIP", nullable: true, length: 18 })
  cZip: string | null;

  @DefColumn("varchar", { name: "C_COUNTRY", nullable: true, length: 30 })
  cCountry: string | null;

  @DefColumn("varchar", { name: "C_ISOCNTRYCODE", nullable: true, length: 10 })
  cIsocntrycode: string | null;

  @DefColumn("varchar", { name: "C_PHONE1", nullable: true, length: 50 })
  cPhone1: string | null;

  @DefColumn("varchar", { name: "C_PHONE2", nullable: true, length: 50 })
  cPhone2: string | null;

  @DefColumn("varchar", { name: "C_FAX1", nullable: true, length: 18 })
  cFax1: string | null;

  @DefColumn("varchar", { name: "C_FAX2", nullable: true, length: 18 })
  cFax2: string | null;

  @DefColumn("varchar", { name: "C_VAT", nullable: true, length: 18 })
  cVat: string | null;

  @DefColumn("varchar", { name: "BUYERPO", nullable: true, length: 20 })
  buyerpo: string | null;

  @DefColumn("varchar", { name: "BILLTOKEY", nullable: true, length: 15 })
  billtokey: string | null;

  @DefColumn("varchar", { name: "B_CONTACT1", nullable: true, length: 30 })
  bContact1: string | null;

  @DefColumn("varchar", { name: "B_CONTACT2", nullable: true, length: 30 })
  bContact2: string | null;

  @DefColumn("varchar", { name: "B_COMPANY", nullable: true, length: 200 })
  bCompany: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS1", nullable: true, length: 200 })
  bAddress1: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS2", nullable: true, length: 45 })
  bAddress2: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS3", nullable: true, length: 45 })
  bAddress3: string | null;

  @DefColumn("varchar", { name: "B_ADDRESS4", nullable: true, length: 45 })
  bAddress4: string | null;

  @DefColumn("varchar", { name: "B_CITY", nullable: true, length: 45 })
  bCity: string | null;

  @DefColumn("varchar", { name: "B_STATE", nullable: true, length: 25 })
  bState: string | null;

  @DefColumn("varchar", { name: "B_ZIP", nullable: true, length: 18 })
  bZip: string | null;

  @DefColumn("varchar", { name: "B_COUNTRY", nullable: true, length: 30 })
  bCountry: string | null;

  @DefColumn("varchar", { name: "B_ISOCNTRYCODE", nullable: true, length: 10 })
  bIsocntrycode: string | null;

  @DefColumn("varchar", { name: "B_PHONE1", nullable: true, length: 50 })
  bPhone1: string | null;

  @DefColumn("varchar", { name: "B_PHONE2", nullable: true, length: 50 })
  bPhone2: string | null;

  @DefColumn("varchar", { name: "B_FAX1", nullable: true, length: 18 })
  bFax1: string | null;

  @DefColumn("varchar", { name: "B_FAX2", nullable: true, length: 18 })
  bFax2: string | null;

  @DefColumn("varchar", { name: "B_VAT", nullable: true, length: 18 })
  bVat: string | null;

  @DefColumn("varchar", { name: "INCOTERM", nullable: true, length: 10 })
  incoterm: string | null;

  @DefColumn("varchar", { name: "PMTTERM", nullable: true, length: 10 })
  pmtterm: string | null;

  @DefColumn("varchar", { name: "DOOR", nullable: true, length: 10 })
  door: string | null;

  @DefColumn("varchar", {
    name: "SORTATIONLOCATION",
    nullable: true,
    length: 18
  })
  sortationlocation: string | null;

  @DefColumn("varchar", { name: "BATCHFLAG", nullable: true, length: 1 })
  batchflag: string | null;

  @DefColumn("varchar", { name: "BULKCARTONGROUP", nullable: true, length: 10 })
  bulkcartongroup: string | null;

  @DefColumn("varchar", { name: "ROUTE", nullable: true, length: 10 })
  route: string | null;

  @DefColumn("int", { name: "STOP", nullable: true, default: () => "'0'" })
  stop: number | null;

  @DefColumn("decimal", {
    name: "OPENQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  openqty: string | null;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("varchar", { name: "DISCHARGEPLACE", nullable: true, length: 50 })
  dischargeplace: string | null;

  @DefColumn("varchar", { name: "DELIVERYPLACE", nullable: true, length: 50 })
  deliveryplace: string | null;

  @DefColumn("varchar", {
    name: "INTERMODALVEHICLE",
    nullable: true,
    length: 30
  })
  intermodalvehicle: string | null;

  @DefColumn("varchar", { name: "COUNTRYOFORIGIN", nullable: true, length: 30 })
  countryoforigin: string | null;

  @DefColumn("varchar", {
    name: "COUNTRYDESTINATION",
    nullable: true,
    length: 30
  })
  countrydestination: string | null;

  @DefColumn("varchar", { name: "UPDATESOURCE", nullable: true, length: 10 })
  updatesource: string | null;

  @DefColumn("varchar", { name: "TYPE", length: 15 })
  type: string;

  @DefColumn("varchar", { name: "SALEORDERGROUP", nullable: true, length: 20 })
  saleordergroup: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "STAGE", nullable: true, length: 10 })
  stage: string | null;

  @DefColumn("varchar", { name: "DC_ID", nullable: true, length: 10 })
  dcId: string | null;

  @DefColumn("varchar", { name: "WHSE_ID", nullable: true, length: 10 })
  whseId: string | null;

  @DefColumn("varchar", { name: "SPLIT_SALEORDERS", nullable: true, length: 1 })
  splitSaleorders: string | null;

  @DefColumn("varchar", { name: "APPT_STATUS", nullable: true, length: 10 })
  apptStatus: string | null;

  @DefColumn("varchar", {
    name: "CHEPPALLETINDICATOR",
    nullable: true,
    length: 10
  })
  cheppalletindicator: string | null;

  @DefColumn("varchar", { name: "CONTAINERTYPE", nullable: true, length: 20 })
  containertype: string | null;

  @DefColumn("decimal", {
    name: "CONTAINERQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  containerqty: string | null;

  @DefColumn("decimal", {
    name: "BILLEDCONTAINERQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  billedcontainerqty: string | null;

  @DefColumn("varchar", {
    name: "TRANSPORTATIONMODE",
    nullable: true,
    length: 30
  })
  transportationmode: string | null;

  @DefColumn("varchar", {
    name: "TRANSPORTATIONSERVICE",
    nullable: true,
    length: 30
  })
  transportationservice: string | null;

  @DefColumn("varchar", {
    name: "EXTERNALSALEORDERKEY2",
    nullable: true,
    length: 32
  })
  externalsaleorderkey2: string | null;

  @DefColumn("varchar", { name: "C_EMAIL1", nullable: true, length: 55 })
  cEmail1: string | null;

  @DefColumn("varchar", { name: "C_EMAIL2", nullable: true, length: 55 })
  cEmail2: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "SUSR6", nullable: true, length: 30 })
  susr6: string | null;

  @DefColumn("varchar", { name: "NOTES2", nullable: true, length: 3072 })
  notes2: string | null;

  @DefColumn("decimal", {
    name: "ITEM_NUMBER",
    nullable: true,
    precision: 28,
    scale: 0,
    default: () => "'0'"
  })
  itemNumber: string | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "LOADID", nullable: true, length: 20 })
  loadid: string | null;

  @DefColumn("varchar", { name: "SHIPTOGETHER", nullable: true, length: 1 })
  shiptogether: string | null;

  @DefColumn("datetime", { name: "DELIVERYDATE2", nullable: true })
  deliverydate2: Date | null;

  @DefColumn("datetime", { name: "REQUESTEDSHIPDATE", nullable: true })
  requestedshipdate: Date | null;

  @DefColumn("datetime", { name: "ACTUALSHIPDATE", nullable: true })
  actualshipdate: Date | null;

  @DefColumn("datetime", { name: "DELIVER_DATE", nullable: true })
  deliverDate: Date | null;

  @DefColumn("decimal", {
    name: "SALEORDERVALUE",
    nullable: true,
    precision: 10,
    scale: 2,
    default: () => "'0.00'"
  })
  saleordervalue: string | null;

  @DefColumn("varchar", { name: "OHTYPE", nullable: true, length: 10 })
  ohtype: string | null;

  @DefColumn("varchar", { name: "EXTERNALLOADID", nullable: true, length: 20 })
  externalloadid: string | null;

  @DefColumn("int", {
    name: "DESTINATIONNESTID",
    nullable: true,
    default: () => "'0'"
  })
  destinationnestid: number | null;

  @DefColumn("varchar", { name: "REFERENCENUM", nullable: true, length: 30 })
  referencenum: string | null;

  @DefColumn("varchar", { name: "INTRANSITKEY", nullable: true, length: 10 })
  intransitkey: string | null;

  @DefColumn("varchar", { name: "RECEIPTKEY", nullable: true, length: 10 })
  receiptkey: string | null;

  @DefColumn("varchar", { name: "CaseLabelType", nullable: true, length: 10 })
  caseLabelType: string | null;

  @DefColumn("varchar", { name: "LABELNAME", nullable: true, length: 20 })
  labelname: string | null;

  @DefColumn("varchar", {
    name: "STDSSCCLABELNAME",
    nullable: true,
    length: 20
  })
  stdsscclabelname: string | null;

  @DefColumn("varchar", {
    name: "STDGTINLABELNAME",
    nullable: true,
    length: 20
  })
  stdgtinlabelname: string | null;

  @DefColumn("varchar", {
    name: "RFIDSSCCLABELNAME",
    nullable: true,
    length: 20
  })
  rfidsscclabelname: string | null;

  @DefColumn("varchar", {
    name: "RFIDGTINLABELNAME",
    nullable: true,
    length: 20
  })
  rfidgtinlabelname: string | null;

  @DefColumn("varchar", { name: "RFIDFLAG", nullable: true, length: 1 })
  rfidflag: string | null;

  @DefColumn("varchar", { name: "CarrierCode", nullable: true, length: 40 })
  carrierCode: string | null;

  @DefColumn("varchar", { name: "CarrierName", nullable: true, length: 200 })
  carrierName: string | null;

  @DefColumn("varchar", {
    name: "CarrierAddress1",
    nullable: true,
    length: 200
  })
  carrierAddress1: string | null;

  @DefColumn("varchar", { name: "CarrierAddress2", nullable: true, length: 45 })
  carrierAddress2: string | null;

  @DefColumn("varchar", { name: "CarrierCity", nullable: true, length: 45 })
  carrierCity: string | null;

  @DefColumn("varchar", { name: "CarrierState", nullable: true, length: 25 })
  carrierState: string | null;

  @DefColumn("varchar", { name: "CarrierZip", nullable: true, length: 18 })
  carrierZip: string | null;

  @DefColumn("varchar", { name: "CarrierCountry", nullable: true, length: 30 })
  carrierCountry: string | null;

  @DefColumn("varchar", { name: "CarrierPhone", nullable: true, length: 18 })
  carrierPhone: string | null;

  @DefColumn("varchar", { name: "DriverName", nullable: true, length: 25 })
  driverName: string | null;

  @DefColumn("varchar", { name: "TrailerNumber", nullable: true, length: 40 })
  trailerNumber: string | null;

  @DefColumn("varchar", { name: "TrailerOwner", nullable: true, length: 25 })
  trailerOwner: string | null;

  @DefColumn("varchar", { name: "TrailerType", nullable: true, length: 10 })
  trailerType: string | null;

  @DefColumn("datetime", { name: "DepDateTime", nullable: true })
  depDateTime: Date | null;

  @DefColumn("varchar", { name: "SALEORDERBREAK", nullable: true, length: 1 })
  saleorderbreak: string | null;

  @DefColumn("varchar", { name: "ALLOCATEDONERP", nullable: true, length: 1 })
  allocatedonerp: string | null;

  @DefColumn("varchar", { name: "TRADINGPARTNER", nullable: true, length: 30 })
  tradingpartner: string | null;

  @DefColumn("varchar", { name: "PRONUMBER", nullable: true, length: 32 })
  pronumber: string | null;

  @DefColumn("varchar", { name: "ENABLEPACKING", nullable: true, length: 1 })
  enablepacking: string | null;

  @DefColumn("varchar", { name: "PACKINGLOCATION", nullable: true, length: 10 })
  packinglocation: string | null;

  @DefColumn("varchar", { name: "SALEORDERSID", nullable: true, length: 32 })
  saleordersid: string | null;

  @DefColumn("varchar", {
    name: "SUSPENDEDINDICATOR",
    nullable: true,
    length: 1
  })
  suspendedindicator: string | null;

  @DefColumn("varchar", { name: "PICKLISTREPORTID", nullable: true, length: 8 })
  picklistreportid: string | null;

  @DefColumn("varchar", {
    name: "PACKINGLISTREPORTID",
    nullable: true,
    length: 8
  })
  packinglistreportid: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCETYPE", nullable: true, length: 64 })
  referencetype: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEDOCUMENT",
    nullable: true,
    length: 64
  })
  referencedocument: string | null;

  @DefColumn("varchar", {
    name: "REFERENCELOCATION",
    nullable: true,
    length: 64
  })
  referencelocation: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEVERSION",
    nullable: true,
    length: 20
  })
  referenceversion: string | null;

  @DefColumn("decimal", {
    name: "FREIGHTCHARGEAMOUNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  freightchargeamount: string | null;

  @DefColumn("decimal", {
    name: "FREIGHTCOSTAMOUNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  freightcostamount: string | null;

  @DefColumn("varchar", { name: "APPOINTMENTKEY", nullable: true, length: 10 })
  appointmentkey: string | null;

  @DefColumn("int", {
    name: "AllowOverPick",
    nullable: true,
    default: () => "'0'"
  })
  allowOverPick: number | null;

  @DefColumn("varchar", {
    name: "ReferenceAccountingEntity",
    nullable: true,
    length: 64
  })
  referenceAccountingEntity: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 100 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 100 })
  editwho: string;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 3072 })
  notes: string | null;

  @DefColumn("varchar", { name: "DRIVER", nullable: true, length: 150 })
  driver: string | null;

  @DefColumn("varchar", { name: "KEEPER", nullable: true, length: 150 })
  keeper: string | null;

  @DefColumn("varchar", { name: "LIFTER", nullable: true, length: 100 })
  lifter: string | null;

  @DefColumn("varchar", { name: "GATE", nullable: true, length: 50 })
  gate: string | null;

  @DefColumn("datetime", { name: "BEGINTIME", nullable: true })
  begintime: Date | null;

  @DefColumn("datetime", { name: "ENDTIME", nullable: true })
  endtime: Date | null;

  @DefColumn("varchar", { name: "ID_CUR", nullable: true, length: 3 })
  idCur: string | null;

  @DefColumn("double", {
    name: "RATE_CUR",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  rateCur: number | null;

  @DefColumn("datetime", { name: "DATE_CREATE_SAI", nullable: true })
  dateCreateSai: Date | null;

  @DefColumn("decimal", {
    name: "AMOUNT_GOOD",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountGood: string | null;

  @DefColumn("decimal", {
    name: "TOTAL_AMOUNT_REDUCTION",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  totalAmountReduction: string | null;

  @DefColumn("double", {
    name: "AMOUNT_VAT_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountVatTax: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_REDUCTION_PAYMENT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfReductionPayment: number | null;

  @DefColumn("double", {
    name: "AMOUNT_REDUCTION_PAYMENT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountReductionPayment: number | null;

  @DefColumn("varchar", { name: "ID_ENT_CUS", nullable: true, length: 31 })
  idEntCus: string | null;

  @DefColumn("varchar", { name: "ID_ENT_VOU", nullable: true, length: 31 })
  idEntVou: string | null;

  @DefColumn("varchar", { name: "TAXCODE_VOU", nullable: true, length: 31 })
  taxcodeVou: string | null;

  @DefColumn("varchar", { name: "INVOICE_NO_SAI", nullable: true, length: 31 })
  invoiceNoSai: string | null;

  @DefColumn("varchar", {
    name: "INVOICE_SERIE_SAI",
    nullable: true,
    length: 31
  })
  invoiceSerieSai: string | null;

  @DefColumn("varchar", { name: "INVOICE_TYPE", nullable: true, length: 127 })
  invoiceType: string | null;

  @DefColumn("datetime", { name: "INVOICE_DATE_SAI", nullable: true })
  invoiceDateSai: Date | null;

  @DefColumn("varchar", { name: "ADDRESS_VOU", nullable: true, length: 127 })
  addressVou: string | null;

  @DefColumn("varchar", { name: "RECEIVER_SAI", nullable: true, length: 127 })
  receiverSai: string | null;

  @DefColumn("varchar", {
    name: "INFO_PLACE_OF_DELIVERY",
    nullable: true,
    length: 127
  })
  infoPlaceOfDelivery: string | null;

  @DefColumn("varchar", {
    name: "INFO_PLACE_OF_DESTINATION",
    nullable: true,
    length: 127
  })
  infoPlaceOfDestination: string | null;

  @DefColumn("varchar", { name: "CUS_PO_NO", nullable: true, length: 255 })
  cusPoNo: string | null;

  @DefColumn("varchar", { name: "DESC_SAI", nullable: true, length: 255 })
  descSai: string | null;

  @DefColumn("varchar", { name: "NAME_CUS_VOU", nullable: true, length: 127 })
  nameCusVou: string | null;

  @DefColumn("varchar", { name: "TRANSFERSTATUS", nullable: true, length: 10 })
  transferstatus: string | null;

  @DefColumn("varchar", { name: "SHIPCODE", nullable: true, length: 40 })
  shipcode: string | null;

  @DefColumn("varchar", { name: "SHIPTIME", nullable: true, length: 30 })
  shiptime: string | null;

  @DefColumn("varchar", {
    name: "DELIVERYSALEORDERS",
    nullable: true,
    length: 50
  })
  deliverysaleorders: string | null;

  @DefColumn("varchar", { name: "FEEDER", nullable: true, length: 40 })
  feeder: string | null;

  @DefColumn("varchar", { name: "PORT", nullable: true, length: 20 })
  port: string | null;

  @DefColumn("varchar", { name: "SEAL", nullable: true, length: 20 })
  seal: string | null;

  @DefColumn("varchar", { name: "SEALCUSTOM", nullable: true, length: 20 })
  sealcustom: string | null;

  @DefColumn("datetime", { name: "CYCUTOFFTIME", nullable: true })
  cycutofftime: Date | null;

  @DefColumn("varchar", { name: "CSO", nullable: true, length: 20 })
  cso: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("datetime", { name: "PROVISIONDATE", nullable: true })
  provisiondate: Date | null;

  @DefColumn("varchar", { name: "VOYAGECODE", nullable: true, length: 30 })
  voyagecode: string | null;

  @DefColumn("varchar", { name: "VEHICLENUMBER", nullable: true, length: 18 })
  vehiclenumber: string | null;

  @DefColumn("datetime", { name: "VEHICLEDATE", nullable: true })
  vehicledate: Date | null;

  @DefColumn("varchar", { name: "CONTAINERKEY", nullable: true, length: 40 })
  containerkey: string | null;

  @DefColumn("varchar", { name: "SEALCAB", nullable: true, length: 20 })
  sealcab: string | null;

  @DefColumn("varchar", { name: "VESSELCODE", nullable: true, length: 100 })
  vesselcode: string | null;

  @DefColumn("decimal", {
    name: "MAXWEIGHT",
    nullable: true,
    precision: 18,
    scale: 5,
    default: () => "'0.00000'"
  })
  maxweight: string | null;

  @DefColumn("decimal", {
    name: "TAREWEIGHT",
    nullable: true,
    precision: 18,
    scale: 5,
    default: () => "'0.00000'"
  })
  tareweight: string | null;

  @DefColumn("datetime", { name: "DOCUMENTTIME", nullable: true })
  documenttime: Date | null;

  @DefColumn("varchar", { name: "CHECKCONTAINER", nullable: true, length: 128 })
  checkcontainer: string | null;

  @DefColumn("datetime", { name: "CYCOT", nullable: true })
  cycot: Date | null;

  @DefColumn("datetime", { name: "SICOST", nullable: true })
  sicost: Date | null;

  @DefColumn("datetime", { name: "CHECKCONTAINERTIME", nullable: true })
  checkcontainertime: Date | null;

  @DefColumn("varchar", { name: "SHIPPINGLINES", nullable: true, length: 100 })
  shippinglines: string | null;

  @DefColumn("varchar", {
    name: "ID_DECLARATION",
    nullable: true,
    length: 1024
  })
  idDeclaration: string | null;

  @DefColumn("datetime", { name: "DATE_DECLARATION", nullable: true })
  dateDeclaration: Date | null;

  @DefColumn("varchar", { name: "PODCODE", nullable: true, length: 20 })
  podcode: string | null;

  @DefColumn("varchar", { name: "XDOCKKEY", nullable: true, length: 30 })
  xdockkey: string | null;

  @DefColumn("varchar", { name: "SALEORDERTYPE2", nullable: true, length: 10 })
  saleordertype2: string | null;

  @DefColumn("varchar", { name: "GENERATEDCODE", nullable: true, length: 50 })
  generatedcode: string | null;

  @DefColumn("tinyint", {
    name: "APPROVE",
    nullable: true,
    default: () => "'0'"
  })
  approve: number | null;

  @DefColumn("datetime", { name: "FROMDATE", nullable: true })
  fromdate: Date | null;

  @DefColumn("datetime", { name: "TODATE", nullable: true })
  todate: Date | null;

  @DefColumn("varchar", { name: "TOWHSEID", nullable: true, length: 45 })
  towhseid: string | null;
}
