import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WHSEID_UNIQUE", ["whseid", "name"], { unique: true })
@Entity("rack", { schema: "swm" })
export class Rack {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 45 })
  whseid: string;

  @DefColumn("varchar", { name: "NAME", length: 45 })
  name: string;

  @DefColumn("int", { name: "QTYROW" })
  qtyrow: number;

  @DefColumn("int", { name: "QTYHEIGHT" })
  qtyheight: number;

  @DefColumn("int", { name: "QTYCOLUMN" })
  qtycolumn: number;

  @DefColumn("varchar", { name: "SIZELOCATION", length: 255 })
  sizelocation: string;

  @DefColumn("varchar", { name: "POINT", length: 255 })
  point: string;

  @DefColumn("varchar", { name: "COLOR", nullable: true, length: 45 })
  color: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;
}
