import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("LOT_138_UNIQUE", ["id"], { unique: true })
@Index(
  "IX_LOTATTRIBUTE",
  [
    "whseid",
    "storerkey",
    "sku",
    "lottable01",
    "lottable02",
    "lottable03",
    "lottable04",
    "lottable05",
    "lottable06",
    "lottable07",
    "lottable08",
    "lottable09",
    "lottable10",
    "lottable11",
    "lottable12"
  ],
  {}
)
@Index("IX_LOTKEY", ["whseid", "lot"], {})
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_ADDDATE", ["whseid", "adddate"], {})
@Index("IX_LOTTABLE11", ["whseid", "lottable11"], {})
@Index("IX_LOTTABLE05", ["whseid", "lottable05"], {})
@Entity("lotattribute", { schema: "swm" })
export class Lotattribute {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { primary: true, name: "LOT", length: 10 })
  lot: string;

  @DefColumn("varchar", { name: "EXTERNALLOT", nullable: true, length: 100 })
  externallot: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 100 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 100 })
  editwho: string;

  @DefColumn("varchar", {
    name: "EXTERNRECEIPTKEY_LOT",
    nullable: true,
    length: 32
  })
  externreceiptkeyLot: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 100 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 100 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 100 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 100 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 100 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 100 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 100 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 100 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;
}
