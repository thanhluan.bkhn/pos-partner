import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("uq_id", ["id"], { unique: true })
@Index("ix_whseid", ["whseid"], {})
@Index("ix_code", ["whseid", "code"], {})
@Index("ix_type", ["whseid", "type"], {})
@Index("ix_adddate", ["adddate"], {})
@Entity("validationdetail", { schema: "swm" })
export class Validationdetail {
  @DefColumn("varchar", { name: "id", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "code", length: 100 })
  code: string;

  @DefColumn("varchar", { name: "name", nullable: true, length: 255 })
  name: string | null;

  @DefColumn("varchar", { primary: true, name: "type", length: 100 })
  type: string;

  @DefColumn("tinyint", {
    name: "lottable01",
    nullable: true,
    default: () => "'0'"
  })
  lottable01: number | null;

  @DefColumn("tinyint", {
    name: "lottable02",
    nullable: true,
    default: () => "'0'"
  })
  lottable02: number | null;

  @DefColumn("tinyint", {
    name: "lottable03",
    nullable: true,
    default: () => "'0'"
  })
  lottable03: number | null;

  @DefColumn("tinyint", {
    name: "lottable04",
    nullable: true,
    default: () => "'0'"
  })
  lottable04: number | null;

  @DefColumn("tinyint", {
    name: "lottable05",
    nullable: true,
    default: () => "'0'"
  })
  lottable05: number | null;

  @DefColumn("tinyint", {
    name: "lottable06",
    nullable: true,
    default: () => "'0'"
  })
  lottable06: number | null;

  @DefColumn("tinyint", {
    name: "lottable07",
    nullable: true,
    default: () => "'0'"
  })
  lottable07: number | null;

  @DefColumn("tinyint", {
    name: "lottable08",
    nullable: true,
    default: () => "'0'"
  })
  lottable08: number | null;

  @DefColumn("tinyint", {
    name: "lottable09",
    nullable: true,
    default: () => "'0'"
  })
  lottable09: number | null;

  @DefColumn("tinyint", {
    name: "lottable10",
    nullable: true,
    default: () => "'0'"
  })
  lottable10: number | null;

  @DefColumn("tinyint", {
    name: "lottable11",
    nullable: true,
    default: () => "'0'"
  })
  lottable11: number | null;

  @DefColumn("tinyint", {
    name: "lottable12",
    nullable: true,
    default: () => "'0'"
  })
  lottable12: number | null;

  @DefColumn("tinyint", {
    name: "unitid",
    nullable: true,
    default: () => "'0'"
  })
  unitid: number | null;

  @DefColumn("tinyint", {
    name: "cartonid",
    nullable: true,
    default: () => "'0'"
  })
  cartonid: number | null;

  @DefColumn("tinyint", {
    name: "palletid",
    nullable: true,
    default: () => "'0'"
  })
  palletid: number | null;

  @DefColumn("tinyint", {
    name: "conditioncode",
    nullable: true,
    default: () => "'0'"
  })
  conditioncode: number | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 18 })
  editwho: string | null;
}
