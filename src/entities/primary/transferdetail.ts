import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TRA_275_UNIQUE", ["id"], { unique: true })
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("transferdetail", { schema: "swm" })
export class Transferdetail {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TRANSFERKEY", length: 10 })
  transferkey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "TRANSFERLINENUMBER",
    length: 5
  })
  transferlinenumber: string;

  @DefColumn("varchar", { name: "FROMWHSEID", length: 30 })
  fromwhseid: string;

  @DefColumn("varchar", { name: "FROMSTORERKEY", length: 30 })
  fromstorerkey: string;

  @DefColumn("varchar", { name: "FROMSKU", length: 50 })
  fromsku: string;

  @DefColumn("varchar", { name: "FROMLOC", length: 30 })
  fromloc: string;

  @DefColumn("varchar", { name: "FROMLOT", length: 10 })
  fromlot: string;

  @DefColumn("varchar", { name: "FROMID", length: 18 })
  fromid: string;

  @DefColumn("decimal", { name: "FROMQTY", precision: 22, scale: 5 })
  fromqty: string;

  @DefColumn("varchar", { name: "FROMPACKKEY", length: 50 })
  frompackkey: string;

  @DefColumn("varchar", { name: "FROMUOM", length: 10 })
  fromuom: string;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 50 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 50 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 50 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 50 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 50 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 50 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 50 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 50 })
  lottable10: string | null;

  @DefColumn("varchar", { name: "TOWHSEID", length: 30 })
  towhseid: string;

  @DefColumn("varchar", { name: "TOSTORERKEY", length: 30 })
  tostorerkey: string;

  @DefColumn("varchar", { name: "TOSKU", length: 50 })
  tosku: string;

  @DefColumn("varchar", { name: "TOLOC", length: 30 })
  toloc: string;

  @DefColumn("varchar", { name: "TOLOT", nullable: true, length: 10 })
  tolot: string | null;

  @DefColumn("varchar", { name: "TOID", nullable: true, length: 18 })
  toid: string | null;

  @DefColumn("decimal", { name: "TOQTY", precision: 22, scale: 5 })
  toqty: string;

  @DefColumn("varchar", { name: "TOPACKKEY", length: 50 })
  topackkey: string;

  @DefColumn("varchar", { name: "TOUOM", length: 10 })
  touom: string;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("datetime", { name: "EFFECTIVEDATE" })
  effectivedate: Date;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", { name: "FROMUNITID", nullable: true, length: 30 })
  fromunitid: string | null;

  @DefColumn("varchar", { name: "FROMCARTONID", nullable: true, length: 30 })
  fromcartonid: string | null;

  @DefColumn("varchar", { name: "FROMPALLETID", nullable: true, length: 32 })
  frompalletid: string | null;

  @DefColumn("varchar", { name: "TOUNITID", nullable: true, length: 30 })
  tounitid: string | null;

  @DefColumn("varchar", { name: "TOCARTONID", nullable: true, length: 30 })
  tocartonid: string | null;

  @DefColumn("varchar", { name: "TOPALLETID", nullable: true, length: 32 })
  topalletid: string | null;

  @DefColumn("varchar", {
    name: "FROMCONDITIONCODE",
    nullable: true,
    length: 50
  })
  fromconditioncode: string | null;

  @DefColumn("varchar", { name: "TOCONDITIONCODE", nullable: true, length: 50 })
  toconditioncode: string | null;
}
