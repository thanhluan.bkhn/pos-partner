import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("billing", { schema: "swm" })
export class Billing {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 50 })
  type: string | null;

  @DefColumn("varchar", { name: "WHSEID", nullable: true, length: 20 })
  whseid: string | null;

  @DefColumn("varchar", { name: "STORERKEY", nullable: true, length: 40 })
  storerkey: string | null;

  @DefColumn("datetime", { name: "BILLDATE", nullable: true })
  billdate: Date | null;

  @DefColumn("decimal", {
    name: "QTY_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyBegin: string | null;

  @DefColumn("decimal", {
    name: "CBM_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmBegin: string | null;

  @DefColumn("decimal", {
    name: "PALLET_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletBegin: string | null;

  @DefColumn("decimal", {
    name: "QTY_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyIn: string | null;

  @DefColumn("decimal", {
    name: "CBM_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmIn: string | null;

  @DefColumn("decimal", {
    name: "PALLET_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletIn: string | null;

  @DefColumn("decimal", {
    name: "QTY_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyOut: string | null;

  @DefColumn("decimal", {
    name: "CBM_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmOut: string | null;

  @DefColumn("decimal", {
    name: "PALLET_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletOut: string | null;

  @DefColumn("decimal", {
    name: "QTY_STOCK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyStock: string | null;

  @DefColumn("decimal", {
    name: "CBM_STOCK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmStock: string | null;

  @DefColumn("decimal", {
    name: "PALLET_STOCK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletStock: string | null;

  @DefColumn("decimal", {
    name: "QTY_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyStorage: string | null;

  @DefColumn("decimal", {
    name: "CBM_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmStorage: string | null;

  @DefColumn("decimal", {
    name: "PALLET_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletStorage: string | null;

  @DefColumn("decimal", {
    name: "QTY_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyInv: string | null;

  @DefColumn("decimal", {
    name: "CBM_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmInv: string | null;

  @DefColumn("decimal", {
    name: "PALLET_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletInv: string | null;

  @DefColumn("decimal", {
    name: "KG_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  kgInv: string | null;

  @DefColumn("decimal", {
    name: "QTY_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyAdjust: string | null;

  @DefColumn("decimal", {
    name: "CBM_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmAdjust: string | null;

  @DefColumn("decimal", {
    name: "PALLET_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletAdjust: string | null;

  @DefColumn("decimal", {
    name: "KG_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  kgAdjust: string | null;

  @DefColumn("decimal", {
    name: "QTY_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyTf: string | null;

  @DefColumn("decimal", {
    name: "CBM_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmTf: string | null;

  @DefColumn("decimal", {
    name: "PALLET_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletTf: string | null;

  @DefColumn("decimal", {
    name: "KG_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  kgTf: string | null;

  @DefColumn("decimal", {
    name: "QTY_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyInOt: string | null;

  @DefColumn("decimal", {
    name: "CBM_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmInOt: string | null;

  @DefColumn("decimal", {
    name: "PALLET_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletInOt: string | null;

  @DefColumn("decimal", {
    name: "KG_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  kgInOt: string | null;

  @DefColumn("decimal", {
    name: "CBM_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cbmOutOt: string | null;

  @DefColumn("decimal", {
    name: "QTY_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyOutOt: string | null;

  @DefColumn("decimal", {
    name: "PALLET_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  palletOutOt: string | null;

  @DefColumn("decimal", {
    name: "KG_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  kgOutOt: string | null;

  @DefColumn("datetime", { name: "CDATETIME", nullable: true })
  cdatetime: Date | null;

  @DefColumn("datetime", { name: "UDATETIME", nullable: true })
  udatetime: Date | null;

  @DefColumn("varchar", { name: "CUSERCODE", nullable: true, length: 20 })
  cusercode: string | null;

  @DefColumn("varchar", { name: "UUSERCODE", nullable: true, length: 20 })
  uusercode: string | null;

  @DefColumn("varchar", { name: "BILL_TYPE", nullable: true, length: 15 })
  billType: string | null;

  @DefColumn("int", { name: "CONFIRMED", nullable: true })
  confirmed: number | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightBegin: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightBegin: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightIn: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightIn: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightOut: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightOut: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_STOCK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightStock: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_STOCK",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightStock: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightStorage: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightStorage: string | null;

  @DefColumn("decimal", {
    name: "CARTON_STORAGE",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonStorage: string | null;

  @DefColumn("decimal", {
    name: "CARTON_BEGIN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonBegin: string | null;

  @DefColumn("decimal", {
    name: "CARTON_IN",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonIn: string | null;

  @DefColumn("decimal", {
    name: "CARTON_OUT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonOut: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightInv: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightInv: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightAdjust: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightAdjust: string | null;

  @DefColumn("decimal", {
    name: "CARTON_INV",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonInv: string | null;

  @DefColumn("decimal", {
    name: "CARTON_ADJUST",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonAdjust: string | null;

  @DefColumn("decimal", {
    name: "CARTON_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonTf: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightTf: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_TF",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightTf: string | null;

  @DefColumn("decimal", {
    name: "CARTON_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonInOt: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightInOt: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_IN_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightInOt: string | null;

  @DefColumn("decimal", {
    name: "CARTON_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  cartonOutOt: string | null;

  @DefColumn("decimal", {
    name: "NETWEIGHT_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  netweightOutOt: string | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT_OUT_OT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  grossweightOutOt: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "CATEGORY", nullable: true, length: 40 })
  category: string | null;
}
