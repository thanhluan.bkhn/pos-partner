import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WHSEID_UNIQUE", ["whseid", "shipcode", "storerkey"], { unique: true })
@Index(
  "SHIPCODE_WHSEID_STORERKEY_DESTCODE_SHIPCDE",
  ["whseid", "storerkey", "shipcode"],
  { unique: true }
)
@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_DESTCODE", ["whseid", "destcode", "storerkey"], {})
@Entity("shipcode", { schema: "swm" })
export class Shipcode {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "SHIPCODE", length: 40 })
  shipcode: string;

  @DefColumn("varchar", { primary: true, name: "DESTCODE", length: 50 })
  destcode: string;

  @DefColumn("varchar", { name: "DESTNAME", nullable: true, length: 200 })
  destname: string | null;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "ADDRESS", nullable: true, length: 200 })
  address: string | null;

  @DefColumn("varchar", { name: "CONTACT", nullable: true, length: 200 })
  contact: string | null;

  @DefColumn("varchar", { name: "PHONE", nullable: true, length: 50 })
  phone: string | null;

  @DefColumn("varchar", { name: "WARD", nullable: true, length: 50 })
  ward: string | null;

  @DefColumn("varchar", { name: "DISTRICT", nullable: true, length: 50 })
  district: string | null;

  @DefColumn("varchar", { name: "CITY", nullable: true, length: 50 })
  city: string | null;

  @DefColumn("varchar", { name: "COUNTRY", nullable: true, length: 50 })
  country: string | null;

  @DefColumn("varchar", { name: "PLANTCODE", nullable: true, length: 50 })
  plantcode: string | null;

  @DefColumn("varchar", { name: "CABCODE", nullable: true, length: 50 })
  cabcode: string | null;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 200 })
  remark: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("date", { name: "ADDDATE", nullable: true })
  adddate: string | null;
}
