import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Claim } from "./claim";
import { Role } from "./role";

@Index("IX_RoleClaim", ["clientId", "roleId", "claimId"], { unique: true })
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("FK_RoleClaim_Claim_claimId", ["claimId"], {})
@Index("FK_RoleClaim_Role_roleId", ["roleId"], {})
@Entity("roleclaim", { schema: "swm" })
export class Roleclaim {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("char", { name: "roleId", length: 36 })
  roleId: string;

  @DefColumn("char", { name: "claimId", length: 36 })
  claimId: string;

  @DefColumn("varchar", { name: "permission", nullable: true, length: 10 })
  permission: string | null;

  @DefColumn("varchar", { name: "roleCode", length: 20 })
  roleCode: string;

  @DefColumn("varchar", { name: "claimCode", length: 20 })
  claimCode: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @ManyToOne(
    () => Claim,
    claim => claim.roleclaims,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "claimId", referencedColumnName: "id" }])
  claim: Claim;

  @ManyToOne(
    () => Role,
    role => role.roleclaims,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "roleId", referencedColumnName: "id" }])
  role: Role;
}
