import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Order } from "./order";
import { Customer } from "./customer";

@Index("IX_customerId", ["customerId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("IX_SoldTo", ["whseid", "soldCode"], {})
@Entity("soldto", { schema: "swm" })
export class Soldto {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "customerId", length: 36 })
  customerId: string;

  @DefColumn("varchar", { name: "whseid", length: 36 })
  whseid: string;

  @DefColumn("varchar", { name: "soldCode", length: 36 })
  soldCode: string;

  @DefColumn("varchar", { name: "countryCode", nullable: true, length: 10 })
  countryCode: string | null;

  @DefColumn("varchar", { name: "provinceCode", nullable: true, length: 10 })
  provinceCode: string | null;

  @DefColumn("varchar", { name: "districtCode", nullable: true, length: 10 })
  districtCode: string | null;

  @DefColumn("varchar", { name: "wardcode", nullable: true, length: 10 })
  wardcode: string | null;

  @DefColumn("float", { name: "lat", precision: 12 })
  lat: number;

  @DefColumn("float", { name: "lng", precision: 12 })
  lng: number;

  @DefColumn("varchar", { name: "clientid", length: 50 })
  clientid: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @DefColumn("datetime", { name: "wmsSyncDate", nullable: true })
  wmsSyncDate: Date | null;

  @DefColumn("varchar", { name: "wmsSyncStatus", nullable: true, length: 50 })
  wmsSyncStatus: string | null;

  @DefColumn("varchar", { name: "wmsSyncError", nullable: true, length: 5000 })
  wmsSyncError: string | null;

  @DefColumn("datetime", { name: "tmsSyncDate", nullable: true })
  tmsSyncDate: Date | null;

  @DefColumn("longtext", { name: "tmsSyncStatus", nullable: true })
  tmsSyncStatus: string | null;

  @DefColumn("longtext", { name: "tmsSyncError", nullable: true })
  tmsSyncError: string | null;

  @OneToMany(
    () => Order,
    order => order.soldTo
  )
  orders: Order[];

  @ManyToOne(
    () => Customer,
    customer => customer.soldtos,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "customerId", referencedColumnName: "id" }])
  customer: Customer;
}
