import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_116_UNIQUE", ["id"], { unique: true })
@Entity("lpnid", { schema: "swm" })
export class Lpnid {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qty: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("decimal", {
    name: "PUTAWAYTI",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  putawayti: string | null;

  @DefColumn("decimal", {
    name: "PUTAWAYHI",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  putawayhi: string | null;

  @DefColumn("varchar", { name: "IPSKey", nullable: true, length: 10 })
  ipsKey: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;
}
