import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ALL_11_UNIQUE", ["id"], { unique: true })
@Entity("allocatestrategydetail", { schema: "swm" })
export class Allocatestrategydetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "ALLOCATESTRATEGYKEY",
    length: 10
  })
  allocatestrategykey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "ALLOCATESTRATEGYLINENUMBER",
    length: 10
  })
  allocatestrategylinenumber: string;

  @DefColumn("varchar", { name: "DESCR", nullable: true, length: 70 })
  descr: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "PICKCODE", nullable: true, length: 20 })
  pickcode: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEOVERRIDE",
    nullable: true,
    length: 10
  })
  locationtypeoverride: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEOVERRIDESTRIPE",
    nullable: true,
    length: 10
  })
  locationtypeoverridestripe: string | null;

  @DefColumn("tinyint", {
    name: "ALLOWOVERALLOCATIONS",
    nullable: true,
    default: () => "'0'"
  })
  allowoverallocations: number | null;

  @DefColumn("varchar", { name: "PICKCODESQL", nullable: true, length: 2000 })
  pickcodesql: string | null;

  @DefColumn("double", {
    name: "STEPNUMBER",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stepnumber: number | null;

  @DefColumn("tinyint", {
    name: "GROUPUOMPICKS",
    nullable: true,
    default: () => "'0'"
  })
  groupuompicks: number | null;

  @DefColumn("varchar", { name: "ZONEPCS", nullable: true, length: 100 })
  zonepcs: string | null;

  @DefColumn("varchar", { name: "SORTBY", nullable: true, length: 200 })
  sortby: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
