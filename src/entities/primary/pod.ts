import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("pod", { schema: "swm" })
export class Pod {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "CODE", length: 20 })
  code: string;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 45 })
  status: string | null;

  @DefColumn("varchar", { name: "ISRECEIVED", nullable: true, length: 6 })
  isreceived: string | null;

  @DefColumn("varchar", { name: "FILENAME", nullable: true, length: 45 })
  filename: string | null;

  @DefColumn("varchar", { name: "FILEPATH", nullable: true, length: 255 })
  filepath: string | null;

  @DefColumn("varchar", { name: "ORIGINNAME", nullable: true, length: 50 })
  originname: string | null;

  @DefColumn("varchar", { name: "ORIGINPATH", nullable: true, length: 100 })
  originpath: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;
}
