import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Client } from "./client";
import { User } from "./user";

@Index("IX_clientId", ["clientId"], {})
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("file", { schema: "swm" })
export class File {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "clientId", nullable: true, length: 36 })
  clientId: string | null;

  @DefColumn("longtext", { name: "path", nullable: true })
  path: string | null;

  @DefColumn("longtext", { name: "name", nullable: true })
  name: string | null;

  @DefColumn("longtext", { name: "originName", nullable: true })
  originName: string | null;

  @DefColumn("longtext", { name: "extension", nullable: true })
  extension: string | null;

  @DefColumn("double", { name: "size", precision: 22 })
  size: number;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @ManyToOne(
    () => Client,
    client => client.files,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "clientId", referencedColumnName: "id" }])
  client: Client;

  @ManyToOne(
    () => User,
    user => user.files,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.files2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
