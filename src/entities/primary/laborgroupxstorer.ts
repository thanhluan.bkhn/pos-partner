import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("id_UNIQUE", ["id"], { unique: true })
@Entity("laborgroupxstorer", { schema: "swm" })
export class Laborgroupxstorer {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "id", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 45 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "storerkey", length: 40 })
  storerkey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "employeegroupcode",
    length: 45
  })
  employeegroupcode: string;

  @DefColumn("decimal", { name: "rate", precision: 18, scale: 4 })
  rate: string;

  @DefColumn("varchar", { name: "base", nullable: true, length: 30 })
  base: string | null;

  @DefColumn("varchar", { name: "addwho", length: 45 })
  addwho: string;

  @DefColumn("datetime", { name: "editdate" })
  editdate: Date;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("varchar", { name: "adddate", nullable: true, length: 45 })
  adddate: string | null;

  @DefColumn("decimal", {
    name: "min",
    nullable: true,
    precision: 18,
    scale: 4
  })
  min: string | null;

  @DefColumn("decimal", {
    name: "max",
    nullable: true,
    precision: 18,
    scale: 4
  })
  max: string | null;

  @DefColumn("varchar", { name: "type", nullable: true, length: 45 })
  type: string | null;
}
