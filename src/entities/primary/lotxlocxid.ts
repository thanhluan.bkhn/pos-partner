import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_ID", ["id"], { unique: true })
@Index("IX_QTY", ["qty"], {})
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_LOC_PALLETID", ["whseid", "loc", "palletid", "qty"], {})
@Index(
  "IX_LOC_PALLETID_CARTONID",
  ["whseid", "loc", "palletid", "cartonid", "qty"],
  {}
)
@Index("IX_UNITID", ["whseid", "unitid"], {})
@Index("IX_CARTONID", ["whseid", "cartonid"], {})
@Index("IX_PALLETID", ["whseid", "palletid"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Index("IX_STORERKEY_SKU_QTY", ["whseid", "storerkey", "sku", "qty"], {})
@Index("IX_LPNID", ["whseid", "lpnid"], {})
@Index(
  "IX_STORERKEY_SKU_QTY_CARTONID",
  ["whseid", "storerkey", "sku", "qty", "cartonid"],
  {}
)
@Index(
  "IX_STORERKEY_SKU_QTY_PALLETID",
  ["whseid", "storerkey", "sku", "qty", "palletid"],
  {}
)
@Index(
  "IX_STORERKEY_SKU_QTY_UNITID",
  ["whseid", "storerkey", "sku", "qty", "unitid"],
  {}
)
@Entity("lotxlocxid", { schema: "swm" })
export class Lotxlocxid {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { primary: true, name: "LOT", length: 10 })
  lot: string;

  @DefColumn("varchar", { primary: true, name: "LOC", length: 30 })
  loc: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qty: string | null;

  @DefColumn("decimal", {
    name: "QTYALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypicked: string | null;

  @DefColumn("decimal", {
    name: "QTYEXPECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyexpected: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKINPROCESS",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypickinprocess: string | null;

  @DefColumn("decimal", {
    name: "PENDINGMOVEIN",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  pendingmovein: string | null;

  @DefColumn("decimal", {
    name: "ARCHIVEQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  archiveqty: string | null;

  @DefColumn("datetime", { name: "ARCHIVEDATE", nullable: true })
  archivedate: Date | null;

  @DefColumn("varchar", { primary: true, name: "STATUS", length: 50 })
  status: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("timestamp", {
    name: "EDITDATE",
    default: () => "'CURRENT_TIMESTAMP(6)'"
  })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;

  @DefColumn("varchar", { primary: true, name: "UNITID", length: 30 })
  unitid: string;

  @DefColumn("varchar", { primary: true, name: "CARTONID", length: 30 })
  cartonid: string;

  @DefColumn("varchar", { primary: true, name: "PALLETID", length: 32 })
  palletid: string;
}
