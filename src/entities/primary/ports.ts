import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("ports", { schema: "swm" })
export class Ports {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "PORTCODE", length: 65 })
  portcode: string;

  @DefColumn("varchar", { name: "PORTNAME", nullable: true, length: 128 })
  portname: string | null;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 65 })
  type: string | null;

  @DefColumn("varchar", { name: "COUNTRYCODE", nullable: true, length: 65 })
  countrycode: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;
}
