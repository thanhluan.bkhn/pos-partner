import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_APPOINTMENTDATE", ["appointmentdate"], {})
@Index("IX_WHSEID_STORERKEY", ["whseid", "storerkey"], {})
@Entity("labordetail", { schema: "swm" })
export class Labordetail {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 45 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTKEY", length: 15 })
  receiptkey: string;

  @DefColumn("varchar", { primary: true, name: "ORDERKEY", length: 15 })
  orderkey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "EMPLOYEEGROUPCODE",
    length: 45
  })
  employeegroupcode: string;

  @DefColumn("varchar", { primary: true, name: "EMPLOYEECODE", length: 45 })
  employeecode: string;

  @DefColumn("datetime", { name: "APPOINTMENTDATE", nullable: true })
  appointmentdate: Date | null;

  @DefColumn("int", { name: "QTY", nullable: true })
  qty: number | null;

  @DefColumn("datetime", { name: "BEGINTIME", nullable: true })
  begintime: Date | null;

  @DefColumn("datetime", { name: "ENDTIME", nullable: true })
  endtime: Date | null;

  @DefColumn("int", { name: "STATUS", nullable: true })
  status: number | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;
}
