import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Client } from "./client";
import { User } from "./user";

@Index("IX_Code", ["code"], { unique: true })
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("connection", { schema: "swm" })
export class Connection {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "code", unique: true, length: 50 })
  code: string;

  @DefColumn("varchar", { name: "name", nullable: true, length: 50 })
  name: string | null;

  @DefColumn("varchar", { name: "databaseType", length: 10 })
  databaseType: string;

  @DefColumn("varchar", { name: "serverName", length: 50 })
  serverName: string;

  @DefColumn("varchar", { name: "databaseName", length: 50 })
  databaseName: string;

  @DefColumn("varchar", { name: "username", length: 50 })
  username: string;

  @DefColumn("varchar", { name: "password", length: 50 })
  password: string;

  @DefColumn("varchar", { name: "connectionString", length: 200 })
  connectionString: string;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @OneToMany(
    () => Client,
    client => client.connection
  )
  clients: Client[];

  @ManyToOne(
    () => User,
    user => user.connections,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.connections2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
