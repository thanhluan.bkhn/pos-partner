import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WMSWAREHOUSE_1_UNIQUE", ["id"], { unique: true })
@Entity("wmswarehouse", { schema: "swm" })
export class Wmswarehouse {
  @DefColumn("int", { name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WAREHOUSEID", length: 30 })
  warehouseid: string;

  @DefColumn("varchar", { primary: true, name: "SUBWAREHOUSEID", length: 30 })
  subwarehouseid: string;

  @DefColumn("varchar", { name: "WAREHOUSENAME", nullable: true, length: 50 })
  warehousename: string | null;

  @DefColumn("varchar", {
    name: "SUBWAREHOUSENAME",
    nullable: true,
    length: 50
  })
  subwarehousename: string | null;

  @DefColumn("varchar", { name: "SUBWAREHOUSEPREFIX", length: 5 })
  subwarehouseprefix: string;

  @DefColumn("varchar", { name: "ASNRUNNINGNO", length: 10 })
  asnrunningno: string;

  @DefColumn("varchar", { name: "SORUNNINGNO", length: 10 })
  sorunningno: string;

  @DefColumn("varchar", { name: "INVOICERUNNINGNO", length: 10 })
  invoicerunningno: string;

  @DefColumn("varchar", { name: "LOTRUNNINGNO", length: 10 })
  lotrunningno: string;

  @DefColumn("varchar", { name: "SCSRUNNINGNO", length: 10 })
  scsrunningno: string;

  @DefColumn("varchar", { name: "STORERPREFIX", nullable: true, length: 5 })
  storerprefix: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;

  @DefColumn("varchar", { name: "PICKDETAILKEY", nullable: true, length: 10 })
  pickdetailkey: string | null;
}
