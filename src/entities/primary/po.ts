import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("PO_206_UNIQUE", ["id"], { unique: true })
@Entity("po", { schema: "swm" })
export class Po {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "POKEY", length: 18 })
  pokey: string;

  @DefColumn("varchar", { name: "EXTERNPOKEY", nullable: true, length: 20 })
  externpokey: string | null;

  @DefColumn("varchar", { name: "POGROUP", nullable: true, length: 20 })
  pogroup: string | null;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("datetime", { name: "PODATE", nullable: true })
  podate: Date | null;

  @DefColumn("varchar", {
    name: "SELLERSREFERENCE",
    nullable: true,
    length: 50
  })
  sellersreference: string | null;

  @DefColumn("varchar", { name: "BUYERSREFERENCE", nullable: true, length: 50 })
  buyersreference: string | null;

  @DefColumn("varchar", { name: "OTHERREFERENCE", nullable: true, length: 18 })
  otherreference: string | null;

  @DefColumn("varchar", { name: "POTYPE", nullable: true, length: 10 })
  potype: string | null;

  @DefColumn("varchar", { name: "SELLERNAME", nullable: true, length: 200 })
  sellername: string | null;

  @DefColumn("varchar", { name: "SELLERADDRESS1", nullable: true, length: 200 })
  selleraddress1: string | null;

  @DefColumn("varchar", { name: "SELLERADDRESS2", nullable: true, length: 45 })
  selleraddress2: string | null;

  @DefColumn("varchar", { name: "SELLERADDRESS3", nullable: true, length: 45 })
  selleraddress3: string | null;

  @DefColumn("varchar", { name: "SELLERADDRESS4", nullable: true, length: 45 })
  selleraddress4: string | null;

  @DefColumn("varchar", { name: "SELLERCITY", nullable: true, length: 100 })
  sellercity: string | null;

  @DefColumn("varchar", { name: "SELLERSTATE", nullable: true, length: 25 })
  sellerstate: string | null;

  @DefColumn("varchar", { name: "SELLERZIP", nullable: true, length: 18 })
  sellerzip: string | null;

  @DefColumn("varchar", { name: "SELLERPHONE", nullable: true, length: 18 })
  sellerphone: string | null;

  @DefColumn("varchar", { name: "SELLERVAT", nullable: true, length: 18 })
  sellervat: string | null;

  @DefColumn("varchar", { name: "BUYERNAME", nullable: true, length: 200 })
  buyername: string | null;

  @DefColumn("varchar", { name: "BUYERADDRESS1", nullable: true, length: 200 })
  buyeraddress1: string | null;

  @DefColumn("varchar", { name: "BUYERADDRESS2", nullable: true, length: 45 })
  buyeraddress2: string | null;

  @DefColumn("varchar", { name: "BUYERADDRESS3", nullable: true, length: 45 })
  buyeraddress3: string | null;

  @DefColumn("varchar", { name: "BUYERADDRESS4", nullable: true, length: 45 })
  buyeraddress4: string | null;

  @DefColumn("varchar", { name: "BUYERCITY", nullable: true, length: 100 })
  buyercity: string | null;

  @DefColumn("varchar", { name: "BUYERSTATE", nullable: true, length: 25 })
  buyerstate: string | null;

  @DefColumn("varchar", { name: "BUYERZIP", nullable: true, length: 18 })
  buyerzip: string | null;

  @DefColumn("varchar", { name: "BUYERPHONE", nullable: true, length: 18 })
  buyerphone: string | null;

  @DefColumn("varchar", { name: "BUYERVAT", nullable: true, length: 18 })
  buyervat: string | null;

  @DefColumn("varchar", { name: "ORIGINCOUNTRY", nullable: true, length: 30 })
  origincountry: string | null;

  @DefColumn("varchar", {
    name: "DESTINATIONCOUNTRY",
    nullable: true,
    length: 30
  })
  destinationcountry: string | null;

  @DefColumn("varchar", { name: "VESSEL", nullable: true, length: 30 })
  vessel: string | null;

  @DefColumn("datetime", { name: "VESSELDATE", nullable: true })
  vesseldate: Date | null;

  @DefColumn("varchar", { name: "PLACEOFLOADING", nullable: true, length: 18 })
  placeofloading: string | null;

  @DefColumn("varchar", {
    name: "PLACEOFDISCHARGE",
    nullable: true,
    length: 18
  })
  placeofdischarge: string | null;

  @DefColumn("varchar", { name: "PLACEOFDELIVERY", nullable: true, length: 18 })
  placeofdelivery: string | null;

  @DefColumn("varchar", { name: "INCOTERMS", nullable: true, length: 10 })
  incoterms: string | null;

  @DefColumn("varchar", { name: "PMTTERM", nullable: true, length: 10 })
  pmtterm: string | null;

  @DefColumn("varchar", { name: "TRANSMETHOD", nullable: true, length: 10 })
  transmethod: string | null;

  @DefColumn("varchar", { name: "TERMSNOTE", nullable: true, length: 18 })
  termsnote: string | null;

  @DefColumn("varchar", { name: "SIGNATORY", nullable: true, length: 18 })
  signatory: string | null;

  @DefColumn("varchar", { name: "PLACEOFISSUE", nullable: true, length: 18 })
  placeofissue: string | null;

  @DefColumn("decimal", {
    name: "OPENQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  openqty: string | null;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "EXTERNALPOKEY2", nullable: true, length: 20 })
  externalpokey2: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "APPORTIONRULE", nullable: true, length: 10 })
  apportionrule: string | null;

  @DefColumn("datetime", { name: "CLOSEDDATE", nullable: true })
  closeddate: Date | null;

  @DefColumn("varchar", { name: "POID", nullable: true, length: 32 })
  poid: string | null;

  @DefColumn("datetime", { name: "EXPECTEDRECEIPTDATE", nullable: true })
  expectedreceiptdate: Date | null;

  @DefColumn("varchar", { name: "SOURCELOCATION", nullable: true, length: 64 })
  sourcelocation: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("longtext", { name: "NOTES", nullable: true })
  notes: string | null;

  @DefColumn("varchar", { name: "SUGGESTWHO", nullable: true, length: 45 })
  suggestwho: string | null;

  @DefColumn("varchar", { name: "PARTSRECOMMEND", nullable: true, length: 45 })
  partsrecommend: string | null;

  @DefColumn("varchar", { name: "TRANSACTIONTYPE", nullable: true, length: 45 })
  transactiontype: string | null;

  @DefColumn("datetime", { name: "SUGGESTDATE", nullable: true })
  suggestdate: Date | null;
}
