import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["whseid", "line"], { unique: true })
@Entity("line", { schema: "swm" })
export class Line {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "WHSEID", length: 50 })
  whseid: string;

  @DefColumn("varchar", { name: "LINE", length: 50 })
  line: string;

  @DefColumn("double", {
    name: "MAXPALLET",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  maxpallet: number | null;

  @DefColumn("double", {
    name: "MAXCARTON",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  maxcarton: number | null;

  @DefColumn("datetime", { name: "CDATETIME", nullable: true })
  cdatetime: Date | null;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 200 })
  remark: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;
}
