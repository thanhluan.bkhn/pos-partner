import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("replenishmentdetail", { schema: "swm" })
export class Replenishmentdetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "IMPORTCODE", length: 45 })
  importcode: string;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 3072 })
  remark: string | null;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "SO", length: 32 })
  so: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qty: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 50 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 50 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 50 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 50 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 50 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 50 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 50 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 50 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;
}
