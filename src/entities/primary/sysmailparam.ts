import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("sysmailparam", { schema: "swm" })
export class Sysmailparam {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "sysMailId", length: 36 })
  sysMailId: string;

  @DefColumn("text", { name: "command" })
  command: string;

  @DefColumn("varchar", { name: "variable", length: 255 })
  variable: string;

  @DefColumn("varchar", { name: "description", nullable: true, length: 255 })
  description: string | null;

  @DefColumn("tinyint", {
    name: "isQuery",
    nullable: true,
    default: () => "'0'"
  })
  isQuery: number | null;

  @DefColumn("datetime", { name: "createdDate", nullable: true })
  createdDate: Date | null;

  @DefColumn("varchar", { name: "createdBy", nullable: true, length: 65 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate", nullable: true })
  modifiedDate: Date | null;

  @DefColumn("varchar", { name: "modifiedBy", nullable: true, length: 65 })
  modifiedBy: string | null;
}
