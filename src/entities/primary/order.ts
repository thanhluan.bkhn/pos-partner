import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Shipto } from "./shipto";
import { Soldto } from "./soldto";
import { Tripdetail } from "./tripdetail";

@Index("IX_Order", ["whseid", "storerKey", "orderKey", "omsType"], {
  unique: true
})
@Index("IX_soldToId", ["soldToId"], {})
@Index("IX_shipToId", ["shipToId"], {})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("order", { schema: "swm" })
export class Order {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "soldToId", length: 36 })
  soldToId: string;

  @DefColumn("char", { name: "shipToId", length: 36 })
  shipToId: string;

  @DefColumn("char", { name: "consigneeId", nullable: true, length: 36 })
  consigneeId: string | null;

  @DefColumn("varchar", { name: "whseid", length: 36 })
  whseid: string;

  @DefColumn("varchar", { name: "storerKey", length: 65 })
  storerKey: string;

  @DefColumn("varchar", { name: "orderKey", length: 36 })
  orderKey: string;

  @DefColumn("varchar", { name: "externalKey1", nullable: true, length: 100 })
  externalKey1: string | null;

  @DefColumn("varchar", { name: "externalKey2", nullable: true, length: 100 })
  externalKey2: string | null;

  @DefColumn("int", { name: "priority" })
  priority: number;

  @DefColumn("varchar", { name: "gate", nullable: true, length: 36 })
  gate: string | null;

  @DefColumn("varchar", { name: "door", nullable: true, length: 36 })
  door: string | null;

  @DefColumn("varchar", { name: "carrierCode", nullable: true, length: 36 })
  carrierCode: string | null;

  @DefColumn("varchar", { name: "trailerNumber", nullable: true, length: 36 })
  trailerNumber: string | null;

  @DefColumn("varchar", { name: "driverName", nullable: true, length: 65 })
  driverName: string | null;

  @DefColumn("varchar", { name: "phoneNumber", nullable: true, length: 20 })
  phoneNumber: string | null;

  @DefColumn("datetime", { name: "orderDate", nullable: true })
  orderDate: Date | null;

  @DefColumn("datetime", { name: "beginTime", nullable: true })
  beginTime: Date | null;

  @DefColumn("datetime", { name: "endTime", nullable: true })
  endTime: Date | null;

  @DefColumn("datetime", { name: "requestShipDate", nullable: true })
  requestShipDate: Date | null;

  @DefColumn("datetime", { name: "actualShipDate", nullable: true })
  actualShipDate: Date | null;

  @DefColumn("datetime", { name: "deliveryDate", nullable: true })
  deliveryDate: Date | null;

  @DefColumn("varchar", { name: "status", nullable: true, length: 36 })
  status: string | null;

  @DefColumn("varchar", { name: "type", nullable: true, length: 15 })
  type: string | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @DefColumn("varchar", { name: "omsType", length: 10 })
  omsType: string;

  @DefColumn("datetime", { name: "expectedReceiptDate", nullable: true })
  expectedReceiptDate: Date | null;

  @DefColumn("datetime", { name: "receiptDate", nullable: true })
  receiptDate: Date | null;

  @DefColumn("datetime", { name: "wmsSyncDate", nullable: true })
  wmsSyncDate: Date | null;

  @DefColumn("varchar", { name: "wmsSyncStatus", nullable: true, length: 50 })
  wmsSyncStatus: string | null;

  @DefColumn("varchar", { name: "wmsSyncError", nullable: true, length: 5000 })
  wmsSyncError: string | null;

  @DefColumn("datetime", { name: "tmsSyncDate", nullable: true })
  tmsSyncDate: Date | null;

  @DefColumn("longtext", { name: "tmsSyncStatus", nullable: true })
  tmsSyncStatus: string | null;

  @DefColumn("longtext", { name: "tmsSyncError", nullable: true })
  tmsSyncError: string | null;

  @DefColumn("varchar", { name: "userdefine1", nullable: true, length: 65 })
  userdefine1: string | null;

  @DefColumn("varchar", { name: "userdefine2", nullable: true, length: 65 })
  userdefine2: string | null;

  @DefColumn("varchar", { name: "userdefine3", nullable: true, length: 65 })
  userdefine3: string | null;

  @DefColumn("varchar", { name: "userdefine4", nullable: true, length: 65 })
  userdefine4: string | null;

  @DefColumn("varchar", { name: "userdefine5", nullable: true, length: 65 })
  userdefine5: string | null;

  @DefColumn("longtext", { name: "userdefine6", nullable: true })
  userdefine6: string | null;

  @DefColumn("varchar", { name: "userdefine7", nullable: true, length: 65 })
  userdefine7: string | null;

  @DefColumn("varchar", { name: "userdefine8", nullable: true, length: 65 })
  userdefine8: string | null;

  @DefColumn("varchar", { name: "userdefine9", nullable: true, length: 65 })
  userdefine9: string | null;

  @DefColumn("varchar", { name: "userdefine10", nullable: true, length: 65 })
  userdefine10: string | null;

  @DefColumn("varchar", { name: "originplace", nullable: true, length: 100 })
  originplace: string | null;

  @DefColumn("varchar", { name: "transitplace", nullable: true, length: 100 })
  transitplace: string | null;

  @ManyToOne(
    () => Shipto,
    shipto => shipto.orders,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "shipToId", referencedColumnName: "id" }])
  shipTo: Shipto;

  @ManyToOne(
    () => Soldto,
    soldto => soldto.orders,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "soldToId", referencedColumnName: "id" }])
  soldTo: Soldto;

  @OneToMany(
    () => Tripdetail,
    tripdetail => tripdetail.order
  )
  tripdetails: Tripdetail[];
}
