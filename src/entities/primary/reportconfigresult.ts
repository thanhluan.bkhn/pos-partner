import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Reportresult } from "./reportresult";
import { User } from "./user";

@Index("IX_ReportConfigResult", ["reportConfigId", "reportResultId"], {
  unique: true
})
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Index(
  "FK_ReportConfigResult_ReportResult_reportResultId",
  ["reportResultId"],
  {}
)
@Entity("reportconfigresult", { schema: "swm" })
export class Reportconfigresult {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "reportConfigId", length: 36 })
  reportConfigId: string;

  @DefColumn("char", { name: "reportResultId", length: 36 })
  reportResultId: string;

  @DefColumn("varchar", { name: "operation", nullable: true, length: 10 })
  operation: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @ManyToOne(
    () => Reportresult,
    reportresult => reportresult.reportconfigresults,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "reportResultId", referencedColumnName: "id" }])
  reportResult: Reportresult;

  @ManyToOne(
    () => User,
    user => user.reportconfigresults,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.reportconfigresults2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;
}
