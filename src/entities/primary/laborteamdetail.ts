import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("id_UNIQUE", ["id"], { unique: true })
@Entity("laborteamdetail", { schema: "swm" })
export class Laborteamdetail {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 45 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TEAMCODE", length: 45 })
  teamcode: string;

  @DefColumn("varchar", {
    primary: true,
    name: "EMPLOYEEGROUPCODE",
    length: 45
  })
  employeegroupcode: string;

  @DefColumn("varchar", { primary: true, name: "EMPLOYEECODE", length: 45 })
  employeecode: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("int", { name: "QTY", nullable: true })
  qty: number | null;
}
