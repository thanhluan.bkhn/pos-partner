import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("WHSEID_UNIQUE", ["whseid"], { unique: true })
@Entity("backgroundwarehouse", { schema: "swm" })
export class Backgroundwarehouse {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", unique: true, length: 45 })
  whseid: string;

  @DefColumn("int", { name: "AREA" })
  area: number;

  @DefColumn("int", { name: "SIZEX" })
  sizex: number;

  @DefColumn("int", { name: "SIZEZ" })
  sizez: number;

  @DefColumn("varchar", { name: "COLOR", nullable: true, length: 45 })
  color: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;
}
