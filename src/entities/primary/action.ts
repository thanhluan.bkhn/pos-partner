import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("action", { schema: "swm" })
export class Action {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "TYPE", length: 45 })
  type: string;

  @DefColumn("int", { name: "STEP" })
  step: number;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 255 })
  description: string | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;
}
