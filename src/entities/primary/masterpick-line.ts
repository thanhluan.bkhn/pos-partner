import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("masterpick_line", { schema: "swm" })
export class MasterpickLine {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "MP", nullable: true, length: 50 })
  mp: string | null;

  @DefColumn("varchar", { name: "LINE", nullable: true, length: 50 })
  line: string | null;

  @DefColumn("datetime", { name: "CDATETIME", nullable: true })
  cdatetime: Date | null;
}
