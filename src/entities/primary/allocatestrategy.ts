import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ALL_10_UNIQUE", ["id"], { unique: true })
@Entity("allocatestrategy", { schema: "swm" })
export class Allocatestrategy {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "ALLOCATESTRATEGYKEY",
    length: 10
  })
  allocatestrategykey: string;

  @DefColumn("varchar", { name: "DESCR", nullable: true, length: 60 })
  descr: string | null;

  @DefColumn("decimal", {
    name: "RETRYIFQTYREMAIN",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  retryifqtyremain: string | null;

  @DefColumn("int", {
    name: "ALLOCATESTRATEGYTYPE",
    nullable: true,
    default: () => "'0'"
  })
  allocatestrategytype: number | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
