import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TRA_274_UNIQUE", ["id"], { unique: true })
@Entity("tariff", { schema: "swm" })
export class Tariff {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 30 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "TARIFFKEY", length: 10 })
  tariffkey: string;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 500 })
  description: string | null;

  @DefColumn("datetime", { name: "VALIDITYFROM", nullable: true })
  validityfrom: Date | null;

  @DefColumn("datetime", { name: "VALIDITYTO", nullable: true })
  validityto: Date | null;

  @DefColumn("varchar", { name: "SUPPORTFLAG", nullable: true, length: 50 })
  supportflag: string | null;

  @DefColumn("varchar", {
    name: "INITIALSTORAGEPERIOD",
    nullable: true,
    length: 50
  })
  initialstorageperiod: string | null;

  @DefColumn("varchar", {
    name: "RECURRINGSTORAGEPERIOD",
    nullable: true,
    length: 50
  })
  recurringstorageperiod: string | null;

  @DefColumn("int", { name: "SPLITMONTHDAY", nullable: true })
  splitmonthday: number | null;

  @DefColumn("decimal", {
    name: "SPLITMONTHPERCENT",
    nullable: true,
    precision: 12,
    scale: 6
  })
  splitmonthpercent: string | null;

  @DefColumn("varchar", { name: "PERIODTYPE", nullable: true, length: 1 })
  periodtype: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "CALENDARGROUP", nullable: true, length: 50 })
  calendargroup: string | null;

  @DefColumn("decimal", {
    name: "RSPERIODTYPE",
    nullable: true,
    precision: 12,
    scale: 6
  })
  rsperiodtype: string | null;

  @DefColumn("decimal", {
    name: "SPLITMONTHPERCENTBEFORE",
    nullable: true,
    precision: 12,
    scale: 6
  })
  splitmonthpercentbefore: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("tinyint", { name: "SETDEFAULT", nullable: true })
  setdefault: number | null;
}
