import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_OMS", ["tableMappingId", "columnOms"], { unique: true })
@Index("IX_TMS", ["tableMappingId", "columnTms"], { unique: true })
@Index("IX_WMS", ["tableMappingId", "columnWms"], { unique: true })
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("tablemappingdetail", { schema: "swm" })
export class Tablemappingdetail {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "tableMappingId", length: 36 })
  tableMappingId: string;

  @DefColumn("varchar", { name: "columnWms", nullable: true, length: 65 })
  columnWms: string | null;

  @DefColumn("varchar", { name: "columnTms", nullable: true, length: 65 })
  columnTms: string | null;

  @DefColumn("varchar", { name: "columnOms", nullable: true, length: 65 })
  columnOms: string | null;

  @DefColumn("varchar", { name: "description", nullable: true, length: 1000 })
  description: string | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;
}
