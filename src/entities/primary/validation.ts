import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("uq_id", ["id"], { unique: true })
@Index("ix_whseid", ["whseid"], {})
@Index("ix_adddate", ["adddate"], {})
@Entity("validation", { schema: "swm" })
export class Validation {
  @DefColumn("varchar", { name: "id", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "code", length: 100 })
  code: string;

  @DefColumn("varchar", { name: "name", nullable: true, length: 255 })
  name: string | null;

  @DefColumn("datetime", { name: "adddate", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "addwho", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 18 })
  editwho: string | null;
}
