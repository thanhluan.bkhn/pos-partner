import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("INV_120_UNIQUE", ["id"], { unique: true })
@Index("IX_LOT_LOC_LPNID", ["whseid", "lot", "loc", "lpnid"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Index("IX_STATUS", ["whseid", "status"], {})
@Entity("inventoryhold", { schema: "swm" })
export class Inventoryhold {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "INVENTORYHOLDKEY", length: 36 })
  inventoryholdkey: string;

  @DefColumn("varchar", { name: "LOT", nullable: true, length: 10 })
  lot: string | null;

  @DefColumn("varchar", { name: "LOC", nullable: true, length: 30 })
  loc: string | null;

  @DefColumn("varchar", { name: "LPNID", nullable: true, length: 18 })
  lpnid: string | null;

  @DefColumn("varchar", { name: "HOLD", length: 5 })
  hold: string;

  @DefColumn("varchar", { name: "STATUS", length: 20 })
  status: string;

  @DefColumn("datetime", { name: "DATEON", nullable: true })
  dateon: Date | null;

  @DefColumn("varchar", { name: "WHOON", length: 18 })
  whoon: string;

  @DefColumn("datetime", { name: "DATEOFF" })
  dateoff: Date;

  @DefColumn("varchar", { name: "WHOOFF", length: 18 })
  whooff: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;

  @DefColumn("varchar", { name: "REMARK1", nullable: true, length: 150 })
  remark1: string | null;

  @DefColumn("varchar", { name: "REMARK2", nullable: true, length: 200 })
  remark2: string | null;

  @DefColumn("varchar", { name: "REMARK3", nullable: true, length: 200 })
  remark3: string | null;
}
