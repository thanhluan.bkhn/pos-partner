import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID", ["id"], { unique: true })
@Entity("syssms", { schema: "swm" })
export class Syssms {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 65 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 65 })
  storerkey: string;

  @DefColumn("varchar", { name: "CODE", nullable: true, length: 40 })
  code: string | null;

  @DefColumn("varchar", { name: "HOST", nullable: true, length: 100 })
  host: string | null;

  @DefColumn("varchar", { name: "USERNAME", nullable: true, length: 255 })
  username: string | null;

  @DefColumn("varchar", { name: "PASSWORD", nullable: true, length: 255 })
  password: string | null;

  @DefColumn("varchar", { name: "BRANDNAME", nullable: true, length: 65 })
  brandname: string | null;

  @DefColumn("text", { name: "MESSAGE", nullable: true })
  message: string | null;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 10 })
  type: string | null;

  @DefColumn("varchar", { name: "METHOD", nullable: true, length: 20 })
  method: string | null;

  @DefColumn("varchar", { name: "SOAPENV", nullable: true, length: 255 })
  soapenv: string | null;

  @DefColumn("varchar", { name: "NS1", nullable: true, length: 255 })
  ns1: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 65 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 65 })
  editwho: string | null;
}
