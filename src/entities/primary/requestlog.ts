import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_Client", ["clientId"], {})
@Index("IX_ClientType", ["clientId", "type"], {})
@Index("IX_ClientUrl", ["clientId", "url"], {})
@Index("IX_Url", ["url"], {})
@Index("IX_Type", ["type"], {})
@Index("IX_Time", ["time"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("requestlog", { schema: "swm" })
export class Requestlog {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "url", nullable: true, length: 100 })
  url: string | null;

  @DefColumn("varchar", { name: "method", nullable: true, length: 10 })
  method: string | null;

  @DefColumn("longtext", { name: "header", nullable: true })
  header: string | null;

  @DefColumn("varchar", { name: "type", nullable: true, length: 10 })
  type: string | null;

  @DefColumn("varchar", { name: "ip", nullable: true, length: 20 })
  ip: string | null;

  @DefColumn("longtext", { name: "request", nullable: true })
  request: string | null;

  @DefColumn("longtext", { name: "response", nullable: true })
  response: string | null;

  @DefColumn("longtext", { name: "error", nullable: true })
  error: string | null;

  @DefColumn("longtext", { name: "stackTrace", nullable: true })
  stackTrace: string | null;

  @DefColumn("double", { name: "time", precision: 22 })
  time: number;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;
}
