import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("NCO_153_UNIQUE", ["id"], { unique: true })
@Entity("ncounter", { schema: "swm" })
export class Ncounter {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "KEYNAME", nullable: true, length: 30 })
  keyname: string | null;

  @DefColumn("int", { name: "KEYCOUNT", nullable: true, default: () => "'0'" })
  keycount: number | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;
}
