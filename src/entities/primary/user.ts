import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Client } from "./client";
import { Component } from "./component";
import { Connection } from "./connection";
import { File } from "./file";
import { Language } from "./language";
import { Languageresource } from "./languageresource";
import { Report } from "./report";
import { Reportconfigresult } from "./reportconfigresult";
import { Reportparam } from "./reportparam";
import { Reportrequest } from "./reportrequest";
import { Reportresult } from "./reportresult";
import { Usersetting } from "./usersetting";
import { Warehouse } from "./warehouse";

@Index("IX_Username", ["username"], { unique: true })
@Index("IX_clientId", ["clientId"], {})
@Index("IX_createdBy", ["createdBy"], {})
@Entity("user", { schema: "swm" })
export class User {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "clientId", nullable: true, length: 36 })
  clientId: string | null;

  @DefColumn("longtext", { name: "firstName", nullable: true })
  firstName: string | null;

  @DefColumn("longtext", { name: "lastName", nullable: true })
  lastName: string | null;

  @DefColumn("varchar", { name: "username", unique: true, length: 50 })
  username: string;

  @DefColumn("longtext", { name: "hashedPassword" })
  hashedPassword: string;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @OneToMany(
    () => Client,
    client => client.createdBy2
  )
  clients: Client[];

  @OneToMany(
    () => Client,
    client => client.modifiedBy2
  )
  clients2: Client[];

  @OneToMany(
    () => Component,
    component => component.createdBy2
  )
  components: Component[];

  @OneToMany(
    () => Component,
    component => component.modifiedBy2
  )
  components2: Component[];

  @OneToMany(
    () => Connection,
    connection => connection.createdBy2
  )
  connections: Connection[];

  @OneToMany(
    () => Connection,
    connection => connection.modifiedBy2
  )
  connections2: Connection[];

  @OneToMany(
    () => File,
    file => file.createdBy2
  )
  files: File[];

  @OneToMany(
    () => File,
    file => file.modifiedBy2
  )
  files2: File[];

  @OneToMany(
    () => Language,
    language => language.createdBy2
  )
  languages: Language[];

  @OneToMany(
    () => Language,
    language => language.modifiedBy2
  )
  languages2: Language[];

  @OneToMany(
    () => Languageresource,
    languageresource => languageresource.createdBy2
  )
  languageresources: Languageresource[];

  @OneToMany(
    () => Languageresource,
    languageresource => languageresource.modifiedBy2
  )
  languageresources2: Languageresource[];

  @OneToMany(
    () => Report,
    report => report.createdBy2
  )
  reports: Report[];

  @OneToMany(
    () => Report,
    report => report.modifiedBy2
  )
  reports2: Report[];

  @OneToMany(
    () => Reportconfigresult,
    reportconfigresult => reportconfigresult.createdBy2
  )
  reportconfigresults: Reportconfigresult[];

  @OneToMany(
    () => Reportconfigresult,
    reportconfigresult => reportconfigresult.modifiedBy2
  )
  reportconfigresults2: Reportconfigresult[];

  @OneToMany(
    () => Reportparam,
    reportparam => reportparam.createdBy2
  )
  reportparams: Reportparam[];

  @OneToMany(
    () => Reportparam,
    reportparam => reportparam.modifiedBy2
  )
  reportparams2: Reportparam[];

  @OneToMany(
    () => Reportrequest,
    reportrequest => reportrequest.createdBy2
  )
  reportrequests: Reportrequest[];

  @OneToMany(
    () => Reportrequest,
    reportrequest => reportrequest.modifiedBy2
  )
  reportrequests2: Reportrequest[];

  @OneToMany(
    () => Reportresult,
    reportresult => reportresult.createdBy2
  )
  reportresults: Reportresult[];

  @OneToMany(
    () => Reportresult,
    reportresult => reportresult.modifiedBy2
  )
  reportresults2: Reportresult[];

  @ManyToOne(
    () => Client,
    client => client.users,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "clientId", referencedColumnName: "id" }])
  client: Client;

  @ManyToOne(
    () => User,
    user => user.users,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @OneToMany(
    () => User,
    user => user.createdBy2
  )
  users: User[];

  @OneToMany(
    () => Usersetting,
    usersetting => usersetting.createdBy2
  )
  usersettings: Usersetting[];

  @OneToMany(
    () => Usersetting,
    usersetting => usersetting.modifiedBy2
  )
  usersettings2: Usersetting[];

  @OneToMany(
    () => Warehouse,
    warehouse => warehouse.createdBy2
  )
  warehouses: Warehouse[];

  @OneToMany(
    () => Warehouse,
    warehouse => warehouse.modifiedBy2
  )
  warehouses2: Warehouse[];
}
