import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("FK_EmailConfiguration_User_createdBy", ["createdBy"], {})
@Index("FK_EmailConfiguration_User_modifiedBy", ["modifiedBy"], {})
@Index("FK_EmailConfig_reportConfig_id", ["reportConfigId"], {})
@Index("FK_EmailConfig_sysMail_id", ["sysMailId"], {})
@Entity("emailconfiguration", { schema: "swm" })
export class Emailconfiguration {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "reportConfigId", length: 36 })
  reportConfigId: string;

  @DefColumn("char", { name: "sysMailId", length: 36 })
  sysMailId: string;

  @DefColumn("int", { name: "hours", nullable: true })
  hours: number | null;

  @DefColumn("int", { name: "dayOfMonth", nullable: true })
  dayOfMonth: number | null;

  @DefColumn("int", { name: "months", nullable: true })
  months: number | null;

  @DefColumn("int", { name: "dayOfWeek", nullable: true })
  dayOfWeek: number | null;

  @DefColumn("tinyint", { name: "isActived", nullable: true })
  isActived: number | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;
}
