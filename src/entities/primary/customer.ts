import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Shipto } from "./shipto";
import { Soldto } from "./soldto";

@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("IX_Customer", ["whseid", "code"], {})
@Entity("customer", { schema: "swm" })
export class Customer {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "whseid", length: 36 })
  whseid: string;

  @DefColumn("varchar", { name: "code", length: 65 })
  code: string;

  @DefColumn("varchar", { name: "name", length: 500 })
  name: string;

  @DefColumn("varchar", { name: "address", nullable: true, length: 1000 })
  address: string | null;

  @DefColumn("varchar", { name: "countryCode", nullable: true, length: 10 })
  countryCode: string | null;

  @DefColumn("varchar", { name: "provinceCode", nullable: true, length: 10 })
  provinceCode: string | null;

  @DefColumn("varchar", { name: "districtCode", nullable: true, length: 10 })
  districtCode: string | null;

  @DefColumn("varchar", { name: "wardCode", nullable: true, length: 10 })
  wardCode: string | null;

  @DefColumn("varchar", { name: "taxCode", nullable: true, length: 16 })
  taxCode: string | null;

  @DefColumn("varchar", { name: "phoneNumber", nullable: true, length: 20 })
  phoneNumber: string | null;

  @DefColumn("longtext", { name: "clientid" })
  clientid: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @DefColumn("datetime", { name: "wmsSyncDate", nullable: true })
  wmsSyncDate: Date | null;

  @DefColumn("varchar", { name: "wmsSyncStatus", nullable: true, length: 50 })
  wmsSyncStatus: string | null;

  @DefColumn("varchar", { name: "wmsSyncError", nullable: true, length: 5000 })
  wmsSyncError: string | null;

  @OneToMany(
    () => Shipto,
    shipto => shipto.customer
  )
  shiptos: Shipto[];

  @OneToMany(
    () => Soldto,
    soldto => soldto.customer
  )
  soldtos: Soldto[];
}
