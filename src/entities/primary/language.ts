import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { User } from "./user";
import { Languageresource } from "./languageresource";

@Index("IX_Code", ["code"], { unique: true })
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("language", { schema: "swm" })
export class Language {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "code", unique: true, length: 10 })
  code: string;

  @DefColumn("longtext", { name: "name", nullable: true })
  name: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @ManyToOne(
    () => User,
    user => user.languages,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.languages2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;

  @OneToMany(
    () => Languageresource,
    languageresource => languageresource.language
  )
  languageresources: Languageresource[];
}
