import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("masterpicktruck", { schema: "swm" })
export class Masterpicktruck {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 20 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("bigint", { name: "PICKTRUCKCODE" })
  picktruckcode: string;

  @DefColumn("bigint", {
    name: "MASTERCODE",
    nullable: true,
    default: () => "'0'"
  })
  mastercode: string | null;

  @DefColumn("varchar", { name: "PICKTRUCKDESRC", nullable: true, length: 500 })
  picktruckdesrc: string | null;

  @DefColumn("int", {
    name: "PICKTRUCKSTATUS",
    nullable: true,
    default: () => "'0'"
  })
  picktruckstatus: number | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;
}
