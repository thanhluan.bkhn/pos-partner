import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("laborteam", { schema: "swm" })
export class Laborteam {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TEAMCODE", length: 45 })
  teamcode: string;

  @DefColumn("varchar", { name: "TEAMNAME", length: 45 })
  teamname: string;

  @DefColumn("int", { name: "STATUS", nullable: true })
  status: number | null;

  @DefColumn("tinyint", { name: "HIDE", nullable: true })
  hide: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;
}
