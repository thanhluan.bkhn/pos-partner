import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("PUT_224_UNIQUE", ["id"], { unique: true })
@Entity("putawaystrategydetail", { schema: "swm" })
export class Putawaystrategydetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "PUTAWAYSTRATEGYKEY",
    length: 10
  })
  putawaystrategykey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "PUTAWAYSTRATEGYLINENUMBER",
    length: 5
  })
  putawaystrategylinenumber: string;

  @DefColumn("varchar", { name: "PATYPE", nullable: true, length: 5 })
  patype: string | null;

  @DefColumn("varchar", { name: "FROMLOC", nullable: true, length: 10 })
  fromloc: string | null;

  @DefColumn("varchar", { name: "TOLOC", nullable: true, length: 10 })
  toloc: string | null;

  @DefColumn("varchar", { name: "AREAKEY", nullable: true, length: 10 })
  areakey: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION05",
    nullable: true,
    length: 5
  })
  dimensionrestriction05: string | null;

  @DefColumn("varchar", { name: "ZONE", nullable: true, length: 10 })
  zone: string | null;

  @DefColumn("varchar", { name: "LOCTYPE", nullable: true, length: 10 })
  loctype: string | null;

  @DefColumn("varchar", { name: "LOCSEARCHTYPE", nullable: true, length: 10 })
  locsearchtype: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION01",
    nullable: true,
    length: 5
  })
  dimensionrestriction01: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION02",
    nullable: true,
    length: 5
  })
  dimensionrestriction02: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION03",
    nullable: true,
    length: 5
  })
  dimensionrestriction03: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION04",
    nullable: true,
    length: 5
  })
  dimensionrestriction04: string | null;

  @DefColumn("varchar", {
    name: "DIMENSIONRESTRICTION06",
    nullable: true,
    length: 5
  })
  dimensionrestriction06: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEEXCLUDE01",
    nullable: true,
    length: 10
  })
  locationtypeexclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEEXCLUDE02",
    nullable: true,
    length: 10
  })
  locationtypeexclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEEXCLUDE03",
    nullable: true,
    length: 10
  })
  locationtypeexclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEEXCLUDE04",
    nullable: true,
    length: 10
  })
  locationtypeexclude04: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPEEXCLUDE05",
    nullable: true,
    length: 10
  })
  locationtypeexclude05: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGEXCLUDE01",
    nullable: true,
    length: 10
  })
  locationflagexclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGEXCLUDE02",
    nullable: true,
    length: 10
  })
  locationflagexclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGEXCLUDE03",
    nullable: true,
    length: 10
  })
  locationflagexclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGINCLUDE01",
    nullable: true,
    length: 10
  })
  locationflaginclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGINCLUDE02",
    nullable: true,
    length: 10
  })
  locationflaginclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONFLAGINCLUDE03",
    nullable: true,
    length: 10
  })
  locationflaginclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGEXCLUDE01",
    nullable: true,
    length: 10
  })
  locationhandlingexclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGEXCLUDE02",
    nullable: true,
    length: 10
  })
  locationhandlingexclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGEXCLUDE03",
    nullable: true,
    length: 10
  })
  locationhandlingexclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGINCLUDE01",
    nullable: true,
    length: 10
  })
  locationhandlinginclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGINCLUDE02",
    nullable: true,
    length: 10
  })
  locationhandlinginclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLINGINCLUDE03",
    nullable: true,
    length: 10
  })
  locationhandlinginclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYINCLUDE01",
    nullable: true,
    length: 10
  })
  locationcategoryinclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYINCLUDE02",
    nullable: true,
    length: 10
  })
  locationcategoryinclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYINCLUDE03",
    nullable: true,
    length: 10
  })
  locationcategoryinclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYEXCLUDE01",
    nullable: true,
    length: 10
  })
  locationcategoryexclude01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYEXCLUDE02",
    nullable: true,
    length: 10
  })
  locationcategoryexclude02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORYEXCLUDE03",
    nullable: true,
    length: 10
  })
  locationcategoryexclude03: string | null;

  @DefColumn("varchar", {
    name: "AREATYPEEXCLUDE01",
    nullable: true,
    length: 10
  })
  areatypeexclude01: string | null;

  @DefColumn("varchar", {
    name: "AREATYPEEXCLUDE02",
    nullable: true,
    length: 10
  })
  areatypeexclude02: string | null;

  @DefColumn("varchar", {
    name: "AREATYPEEXCLUDE03",
    nullable: true,
    length: 10
  })
  areatypeexclude03: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPERESTRICTION01",
    nullable: true,
    length: 5
  })
  locationtyperestriction01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPERESTRICTION02",
    nullable: true,
    length: 5
  })
  locationtyperestriction02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONTYPERESTRICTION03",
    nullable: true,
    length: 5
  })
  locationtyperestriction03: string | null;

  @DefColumn("varchar", { name: "FITFULLRECEIPT", nullable: true, length: 5 })
  fitfullreceipt: string | null;

  @DefColumn("varchar", { name: "ORDERTYPE", nullable: true, length: 10 })
  ordertype: string | null;

  @DefColumn("int", {
    name: "NUMBEROFDAYSOFFSET",
    nullable: true,
    default: () => "'0'"
  })
  numberofdaysoffset: number | null;

  @DefColumn("varchar", {
    name: "LOCATIONSTATERESTRICTION01",
    nullable: true,
    length: 5
  })
  locationstaterestriction01: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONSTATERESTRICTION02",
    nullable: true,
    length: 5
  })
  locationstaterestriction02: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONSTATERESTRICTION03",
    nullable: true,
    length: 5
  })
  locationstaterestriction03: string | null;

  @DefColumn("varchar", { name: "ALLOWFULLPALLETS", nullable: true, length: 5 })
  allowfullpallets: string | null;

  @DefColumn("varchar", { name: "ALLOWFULLCASES", nullable: true, length: 5 })
  allowfullcases: string | null;

  @DefColumn("varchar", { name: "ALLOWPIECES", nullable: true, length: 5 })
  allowpieces: string | null;

  @DefColumn("varchar", {
    name: "CHECKEQUIPMENTPROFILEKEY",
    nullable: true,
    length: 5
  })
  checkequipmentprofilekey: string | null;

  @DefColumn("varchar", {
    name: "CHECKRESTRICTIONS",
    nullable: true,
    length: 5
  })
  checkrestrictions: string | null;

  @DefColumn("varchar", { name: "SKUABCEXCLUDE01", nullable: true, length: 10 })
  skuabcexclude01: string | null;

  @DefColumn("varchar", { name: "SKUABCEXCLUDE02", nullable: true, length: 10 })
  skuabcexclude02: string | null;

  @DefColumn("varchar", { name: "SKUABCEXCLUDE03", nullable: true, length: 10 })
  skuabcexclude03: string | null;

  @DefColumn("varchar", { name: "SKUABCINCLUDE01", nullable: true, length: 10 })
  skuabcinclude01: string | null;

  @DefColumn("varchar", { name: "SKUABCINCLUDE02", nullable: true, length: 10 })
  skuabcinclude02: string | null;

  @DefColumn("varchar", { name: "SKUABCINCLUDE03", nullable: true, length: 10 })
  skuabcinclude03: string | null;

  @DefColumn("varchar", { name: "LOCABCEXCLUDE01", nullable: true, length: 10 })
  locabcexclude01: string | null;

  @DefColumn("varchar", { name: "LOCABCEXCLUDE02", nullable: true, length: 10 })
  locabcexclude02: string | null;

  @DefColumn("varchar", { name: "LOCABCEXCLUDE03", nullable: true, length: 10 })
  locabcexclude03: string | null;

  @DefColumn("varchar", { name: "LOCABCINCLUDE01", nullable: true, length: 10 })
  locabcinclude01: string | null;

  @DefColumn("varchar", { name: "LOCABCINCLUDE02", nullable: true, length: 10 })
  locabcinclude02: string | null;

  @DefColumn("varchar", { name: "LOCABCINCLUDE03", nullable: true, length: 10 })
  locabcinclude03: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEEXCLUDE01",
    nullable: true,
    length: 10
  })
  receipttypeexclude01: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEEXCLUDE02",
    nullable: true,
    length: 10
  })
  receipttypeexclude02: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEEXCLUDE03",
    nullable: true,
    length: 10
  })
  receipttypeexclude03: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEINCLUDE01",
    nullable: true,
    length: 10
  })
  receipttypeinclude01: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEINCLUDE02",
    nullable: true,
    length: 10
  })
  receipttypeinclude02: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTTYPEINCLUDE03",
    nullable: true,
    length: 10
  })
  receipttypeinclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSEXCLUDE01",
    nullable: true,
    length: 10
  })
  skufreightclassexclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSEXCLUDE02",
    nullable: true,
    length: 10
  })
  skufreightclassexclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSEXCLUDE03",
    nullable: true,
    length: 10
  })
  skufreightclassexclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSINCLUDE01",
    nullable: true,
    length: 10
  })
  skufreightclassinclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSINCLUDE02",
    nullable: true,
    length: 10
  })
  skufreightclassinclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUFREIGHTCLASSINCLUDE03",
    nullable: true,
    length: 10
  })
  skufreightclassinclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr01Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr01Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr01Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr01Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr01Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR01INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr01Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr02Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr02Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr02Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr02Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr02Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR02INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr02Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr03Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr03Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr03Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr03Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr03Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR03INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr03Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr04Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr04Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr04Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr04Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr04Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR04INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr04Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr05Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr05Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr05Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr05Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr05Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR05INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr05Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr06Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr06Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr06Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr06Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr06Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR06INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr06Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr07Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr07Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr07Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr07Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr07Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR07INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr07Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr08Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr08Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr08Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr08Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr08Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR08INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr08Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr09Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr09Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr09Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr09Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr09Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR09INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr09Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10EXCLUDE01",
    nullable: true,
    length: 10
  })
  skususr10Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10EXCLUDE02",
    nullable: true,
    length: 10
  })
  skususr10Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10EXCLUDE03",
    nullable: true,
    length: 10
  })
  skususr10Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10INCLUDE01",
    nullable: true,
    length: 10
  })
  skususr10Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10INCLUDE02",
    nullable: true,
    length: 10
  })
  skususr10Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUSUSR10INCLUDE03",
    nullable: true,
    length: 10
  })
  skususr10Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr01Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr01Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr01Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr01Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr01Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR01INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr01Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr02Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr02Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr02Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr02Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr02Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR02INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr02Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr03Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr03Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr03Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr03Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr03Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR03INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr03Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr04Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr04Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr04Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr04Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr04Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR04INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr04Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr05Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr05Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr05Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr05Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr05Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR05INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr05Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr06Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr06Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr06Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr06Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr06Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR06INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr06Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr07Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr07Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr07Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr07Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr07Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR07INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr07Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr08Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr08Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr08Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr08Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr08Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR08INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr08Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr09Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr09Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr09Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr09Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr09Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR09INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr09Include03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10EXCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr10Exclude01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10EXCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr10Exclude02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10EXCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr10Exclude03: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10INCLUDE01",
    nullable: true,
    length: 10
  })
  skubusr10Include01: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10INCLUDE02",
    nullable: true,
    length: 10
  })
  skubusr10Include02: string | null;

  @DefColumn("varchar", {
    name: "SKUBUSR10INCLUDE03",
    nullable: true,
    length: 10
  })
  skubusr10Include03: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEINCLUDE01",
    nullable: true,
    length: 10
  })
  dispositiontypeinclude01: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEINCLUDE02",
    nullable: true,
    length: 10
  })
  dispositiontypeinclude02: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEINCLUDE03",
    nullable: true,
    length: 10
  })
  dispositiontypeinclude03: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEEXCLUDE01",
    nullable: true,
    length: 10
  })
  dispositiontypeexclude01: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEEXCLUDE02",
    nullable: true,
    length: 10
  })
  dispositiontypeexclude02: string | null;

  @DefColumn("varchar", {
    name: "DISPOSITIONTYPEEXCLUDE03",
    nullable: true,
    length: 10
  })
  dispositiontypeexclude03: string | null;

  @DefColumn("varchar", {
    name: "SINGLEPUTAWAYFORMULTIPALLETS",
    nullable: true,
    length: 1
  })
  singleputawayformultipallets: string | null;

  @DefColumn("int", {
    name: "STEPNUMBER",
    nullable: true,
    default: () => "'0'"
  })
  stepnumber: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
