import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { User } from "./user";
import { Reportparam } from "./reportparam";
import { Reportresult } from "./reportresult";

@Index("IX_Code", ["code"], { unique: true })
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Entity("report", { schema: "swm" })
export class Report {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "code", unique: true, length: 50 })
  code: string;

  @DefColumn("longtext", { name: "name", nullable: true })
  name: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("longtext", { name: "command", nullable: true })
  command: string | null;

  @DefColumn("longtext", { name: "defaultCommand", nullable: true })
  defaultCommand: string | null;

  @ManyToOne(
    () => User,
    user => user.reports,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.reports2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;

  @OneToMany(
    () => Reportparam,
    reportparam => reportparam.report
  )
  reportparams: Reportparam[];

  @OneToMany(
    () => Reportresult,
    reportresult => reportresult.report
  )
  reportresults: Reportresult[];
}
