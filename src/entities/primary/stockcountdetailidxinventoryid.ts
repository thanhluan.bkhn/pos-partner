import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("stockcountdetailidxinventoryid", { schema: "swm" })
export class Stockcountdetailidxinventoryid {
  @DefColumn("varchar", { name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", {
    primary: true,
    name: "stockcountdetailid",
    length: 36
  })
  stockcountdetailid: string;

  @DefColumn("varchar", { primary: true, name: "inventoryid", length: 36 })
  inventoryid: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 100 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 100 })
  editwho: string;
}
