import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("UQ_ID", ["id"], { unique: true })
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_ORDERKEY", ["whseid", "orderkey"], {})
@Index("IX_ORDERDETAIL", ["whseid", "storerkey", "sku"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("orderdetail", { schema: "swm" })
export class Orderdetail {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "ORDERKEY", length: 10 })
  orderkey: string;

  @DefColumn("varchar", { primary: true, name: "ORDERLINENUMBER", length: 5 })
  orderlinenumber: string;

  @DefColumn("int", {
    name: "ORDERDETAILSYSID",
    nullable: true,
    default: () => "'0'"
  })
  orderdetailsysid: number | null;

  @DefColumn("varchar", { name: "EXTERNORDERKEY", nullable: true, length: 32 })
  externorderkey: string | null;

  @DefColumn("varchar", { name: "EXTERNLINENO", nullable: true, length: 20 })
  externlineno: string | null;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "MANUFACTURERSKU", nullable: true, length: 50 })
  manufacturersku: string | null;

  @DefColumn("varchar", { name: "RETAILSKU", nullable: true, length: 50 })
  retailsku: string | null;

  @DefColumn("varchar", { name: "ALTSKU", nullable: true, length: 50 })
  altsku: string | null;

  @DefColumn("decimal", {
    name: "ORIGINALQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  originalqty: string | null;

  @DefColumn("decimal", {
    name: "OPENQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  openqty: string | null;

  @DefColumn("decimal", {
    name: "SHIPPEDQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  shippedqty: string | null;

  @DefColumn("decimal", {
    name: "ADJUSTEDQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  adjustedqty: string | null;

  @DefColumn("decimal", {
    name: "QTYPREALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypreallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypicked: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("varchar", { name: "PICKCODE", nullable: true, length: 10 })
  pickcode: string | null;

  @DefColumn("varchar", { name: "CARTONGROUP", nullable: true, length: 10 })
  cartongroup: string | null;

  @DefColumn("varchar", { name: "LOT", nullable: true, length: 10 })
  lot: string | null;

  @DefColumn("varchar", { name: "LPNID", nullable: true, length: 18 })
  lpnid: string | null;

  @DefColumn("varchar", { name: "FACILITY", nullable: true, length: 20 })
  facility: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("double", {
    name: "UNITPRICE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  unitprice: number | null;

  @DefColumn("double", {
    name: "TAX01",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  tax01: number | null;

  @DefColumn("double", {
    name: "TAX02",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  tax02: number | null;

  @DefColumn("double", {
    name: "EXTENDEDPRICE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  extendedprice: number | null;

  @DefColumn("varchar", { name: "UPDATESOURCE", nullable: true, length: 10 })
  updatesource: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "TARIFFKEY", nullable: true, length: 10 })
  tariffkey: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 2000 })
  notes: string | null;

  @DefColumn("varchar", { name: "WORKORDERKEY", nullable: true, length: 10 })
  workorderkey: string | null;

  @DefColumn("varchar", {
    name: "ALLOCATESTRATEGYKEY",
    nullable: true,
    length: 10
  })
  allocatestrategykey: string | null;

  @DefColumn("varchar", {
    name: "PREALLOCATESTRATEGYKEY",
    nullable: true,
    length: 10
  })
  preallocatestrategykey: string | null;

  @DefColumn("varchar", {
    name: "ALLOCATESTRATEGYTYPE",
    nullable: true,
    length: 10
  })
  allocatestrategytype: string | null;

  @DefColumn("varchar", { name: "SKUROTATION", nullable: true, length: 10 })
  skurotation: string | null;

  @DefColumn("int", { name: "SHELFLIFE", nullable: true, default: () => "'0'" })
  shelflife: number | null;

  @DefColumn("varchar", { name: "ROTATION", nullable: true, length: 1 })
  rotation: string | null;

  @DefColumn("varchar", { name: "PALLET_ID", nullable: true, length: 18 })
  palletId: string | null;

  @DefColumn("varchar", { name: "SUB_FLAG", nullable: true, length: 1 })
  subFlag: string | null;

  @DefColumn("double", {
    name: "PRODUCT_WEIGHT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  productWeight: number | null;

  @DefColumn("double", {
    name: "PRODUCT_CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  productCube: number | null;

  @DefColumn("decimal", {
    name: "ORIGCASEQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  origcaseqty: string | null;

  @DefColumn("decimal", {
    name: "ORIGPALLETQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  origpalletqty: string | null;

  @DefColumn("int", {
    name: "OKTOSUBSTITUTE",
    nullable: true,
    default: () => "'0'"
  })
  oktosubstitute: number | null;

  @DefColumn("int", {
    name: "ISSUBSTITUTE",
    nullable: true,
    default: () => "'0'"
  })
  issubstitute: number | null;

  @DefColumn("varchar", { name: "ORIGINALSKU", nullable: true, length: 50 })
  originalsku: string | null;

  @DefColumn("varchar", {
    name: "ORIGINALLINENUMBER",
    nullable: true,
    length: 5
  })
  originallinenumber: string | null;

  @DefColumn("varchar", { name: "SHIPGROUP01", nullable: true, length: 1 })
  shipgroup01: string | null;

  @DefColumn("varchar", { name: "SHIPGROUP02", nullable: true, length: 1 })
  shipgroup02: string | null;

  @DefColumn("varchar", { name: "SHIPGROUP03", nullable: true, length: 1 })
  shipgroup03: string | null;

  @DefColumn("datetime", { name: "ACTUALSHIPDATE", nullable: true })
  actualshipdate: Date | null;

  @DefColumn("varchar", {
    name: "INTERMODALVEHICLE",
    nullable: true,
    length: 10
  })
  intermodalvehicle: string | null;

  @DefColumn("varchar", {
    name: "PICKINGINSTRUCTIONS",
    nullable: true,
    length: 255
  })
  pickinginstructions: string | null;

  @DefColumn("varchar", { name: "CARTONBREAK", nullable: true, length: 10 })
  cartonbreak: string | null;

  @DefColumn("int", {
    name: "CARTONQTYBREAK",
    nullable: true,
    default: () => "'0'"
  })
  cartonqtybreak: number | null;

  @DefColumn("decimal", {
    name: "QTYINTRANSIT",
    nullable: true,
    precision: 22,
    scale: 5
  })
  qtyintransit: string | null;

  @DefColumn("varchar", { name: "OPPREQUEST", nullable: true, length: 1 })
  opprequest: string | null;

  @DefColumn("varchar", { name: "WPRELEASED", nullable: true, length: 1 })
  wpreleased: string | null;

  @DefColumn("varchar", { name: "EXTERNALLOT", nullable: true, length: 100 })
  externallot: string | null;

  @DefColumn("varchar", { name: "BUYERPO", nullable: true, length: 20 })
  buyerpo: string | null;

  @DefColumn("varchar", {
    name: "GenerateContainerDetail",
    nullable: true,
    length: 1
  })
  generateContainerDetail: string | null;

  @DefColumn("varchar", { name: "LABELNAME", nullable: true, length: 20 })
  labelname: string | null;

  @DefColumn("varchar", {
    name: "STDSSCCLABELNAME",
    nullable: true,
    length: 20
  })
  stdsscclabelname: string | null;

  @DefColumn("varchar", {
    name: "STDGTINLABELNAME",
    nullable: true,
    length: 20
  })
  stdgtinlabelname: string | null;

  @DefColumn("varchar", {
    name: "RFIDSSCCLABELNAME",
    nullable: true,
    length: 20
  })
  rfidsscclabelname: string | null;

  @DefColumn("varchar", {
    name: "RFIDGTINLABELNAME",
    nullable: true,
    length: 20
  })
  rfidgtinlabelname: string | null;

  @DefColumn("varchar", {
    name: "EXTERNALLOCSEQUENCE",
    nullable: true,
    length: 20
  })
  externallocsequence: string | null;

  @DefColumn("int", {
    name: "MINSHIPPERCENT",
    nullable: true,
    default: () => "'0'"
  })
  minshippercent: number | null;

  @DefColumn("varchar", { name: "LINETYPE", nullable: true, length: 1 })
  linetype: string | null;

  @DefColumn("decimal", {
    name: "COMPONENTQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  componentqty: string | null;

  @DefColumn("varchar", {
    name: "COMPONENTREFERENCE",
    nullable: true,
    length: 50
  })
  componentreference: string | null;

  @DefColumn("varchar", { name: "OQCREQUIRED", nullable: true, length: 1 })
  oqcrequired: string | null;

  @DefColumn("varchar", { name: "OQCAUTOADJUST", nullable: true, length: 1 })
  oqcautoadjust: string | null;

  @DefColumn("varchar", { name: "ORDERDETAILID", nullable: true, length: 32 })
  orderdetailid: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCETYPE", nullable: true, length: 64 })
  referencetype: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEDOCUMENT",
    nullable: true,
    length: 64
  })
  referencedocument: string | null;

  @DefColumn("varchar", {
    name: "REFERENCELOCATION",
    nullable: true,
    length: 64
  })
  referencelocation: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEVERSION",
    nullable: true,
    length: 20
  })
  referenceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCELINE", nullable: true, length: 20 })
  referenceline: string | null;

  @DefColumn("decimal", {
    name: "CUBICMETER",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  cubicmeter: string | null;

  @DefColumn("decimal", {
    name: "HUNDREDWEIGHT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  hundredweight: string | null;

  @DefColumn("varchar", { name: "StageLoc", nullable: true, length: 20 })
  stageLoc: string | null;

  @DefColumn("decimal", {
    name: "FULFILLQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  fulfillqty: string | null;

  @DefColumn("varchar", {
    name: "ReferenceAccountingEntity",
    nullable: true,
    length: 64
  })
  referenceAccountingEntity: string | null;

  @DefColumn("varchar", {
    name: "ReferenceScheduleLine",
    nullable: true,
    length: 20
  })
  referenceScheduleLine: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 100 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 100 })
  editwho: string;

  @DefColumn("decimal", {
    name: "UNIT_PRICE_SID",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  unitPriceSid: string | null;

  @DefColumn("decimal", {
    name: "AMOUNT_TOTAL_NEW",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountTotalNew: string | null;

  @DefColumn("double", {
    name: "PERCENT_OF_VAT_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfVatSid: number | null;

  @DefColumn("double", {
    name: "AMOUNT_VAT_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountVatSid: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_REDUCTION_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfReductionSid: number | null;

  @DefColumn("decimal", {
    name: "UNIT_PRICE_REDUCTION_SID",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  unitPriceReductionSid: string | null;

  @DefColumn("decimal", {
    name: "AMOUNT_OF_REDUCTION_SID",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountOfReductionSid: string | null;

  @DefColumn("decimal", {
    name: "AMOUNT_TOTAL",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountTotal: string | null;

  @DefColumn("varchar", { name: "NOTE_SID", nullable: true, length: 255 })
  noteSid: string | null;

  @DefColumn("decimal", {
    name: "DISCOUNT",
    nullable: true,
    precision: 18,
    scale: 4,
    default: () => "'0.0000'"
  })
  discount: string | null;

  @DefColumn("decimal", {
    name: "VAT",
    nullable: true,
    precision: 18,
    scale: 4,
    default: () => "'0.0000'"
  })
  vat: string | null;

  @DefColumn("double", {
    name: "STDGROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdgrosswgt: number | null;

  @DefColumn("double", {
    name: "STDNETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdnetwgt: number | null;

  @DefColumn("double", {
    name: "STDCUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdcube: number | null;

  @DefColumn("double", {
    name: "YARD",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yard: number | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 100 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 100 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 100 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 100 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 100 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 100 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 100 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 100 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("varchar", { name: "LOC", nullable: true, length: 30 })
  loc: string | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 30 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 30 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 20 })
  palletid: string | null;

  @DefColumn("varchar", { name: "UPCCODE", nullable: true, length: 50 })
  upccode: string | null;

  @DefColumn("decimal", {
    name: "GROSSWGT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  grosswgt: string | null;

  @DefColumn("decimal", {
    name: "NETWGT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  netwgt: string | null;

  @DefColumn("decimal", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  cube: string | null;

  @DefColumn("varchar", { name: "CONDITIONCODE", nullable: true, length: 45 })
  conditioncode: string | null;

  @DefColumn("varchar", { name: "SALEORDERKEY", nullable: true, length: 10 })
  saleorderkey: string | null;

  @DefColumn("varchar", {
    name: "SALEORDERLINENUMBER",
    nullable: true,
    length: 5
  })
  saleorderlinenumber: string | null;

  @DefColumn("decimal", {
    name: "QTYEXPECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyexpected: string | null;
}
