import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Setting } from "./setting";
import { Settingdetail } from "./settingdetail";
import { Userrole } from "./userrole";

@Index("IX_Code", ["mappingId", "code"], { unique: true })
@Index("IX_Mapping", ["key1", "key2", "key3", "key4", "key5"], { unique: true })
@Index("IX_MappingCode", ["mappingCode"], {})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("mappingdetail", { schema: "swm" })
export class Mappingdetail {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "mappingId", length: 36 })
  mappingId: string;

  @DefColumn("varchar", { name: "mappingCode", nullable: true, length: 20 })
  mappingCode: string | null;

  @DefColumn("varchar", { name: "code", length: 50 })
  code: string;

  @DefColumn("varchar", { name: "name", nullable: true, length: 200 })
  name: string | null;

  @DefColumn("varchar", { name: "key1", nullable: true, length: 50 })
  key1: string | null;

  @DefColumn("varchar", { name: "key2", nullable: true, length: 50 })
  key2: string | null;

  @DefColumn("varchar", { name: "key3", nullable: true, length: 50 })
  key3: string | null;

  @DefColumn("varchar", { name: "key4", nullable: true, length: 50 })
  key4: string | null;

  @DefColumn("varchar", { name: "key5", nullable: true, length: 50 })
  key5: string | null;

  @DefColumn("longtext", { name: "description", nullable: true })
  description: string | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @OneToMany(
    () => Setting,
    setting => setting.mappingDetail
  )
  settings: Setting[];

  @OneToMany(
    () => Settingdetail,
    settingdetail => settingdetail.mappingDetail
  )
  settingdetails: Settingdetail[];

  @OneToMany(
    () => Userrole,
    userrole => userrole.mappingDetail
  )
  userroles: Userrole[];
}
