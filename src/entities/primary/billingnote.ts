import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("billingnote", { schema: "swm" })
export class Billingnote {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("date", { name: "BILLDATE" })
  billdate: string;

  @DefColumn("varchar", { name: "SERVICECODE", nullable: true, length: 50 })
  servicecode: string | null;

  @DefColumn("varchar", { name: "SERVICENAME", nullable: true, length: 50 })
  servicename: string | null;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 255 })
  uom: string | null;

  @DefColumn("decimal", {
    name: "PRICE",
    nullable: true,
    precision: 15,
    scale: 5
  })
  price: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 18,
    scale: 5
  })
  qty: string | null;

  @DefColumn("varchar", { name: "WHSEID", nullable: true, length: 15 })
  whseid: string | null;

  @DefColumn("decimal", {
    name: "AMOUNT",
    nullable: true,
    precision: 18,
    scale: 5
  })
  amount: string | null;

  @DefColumn("decimal", {
    name: "VAT",
    nullable: true,
    precision: 18,
    scale: 5
  })
  vat: string | null;

  @DefColumn("decimal", {
    name: "GRANDTOTAL",
    nullable: true,
    precision: 18,
    scale: 5
  })
  grandtotal: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 20 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 255 })
  remark: string | null;

  @DefColumn("tinyint", { name: "STATUS", nullable: true })
  status: number | null;

  @DefColumn("varchar", { name: "BILL_TYPE", nullable: true, length: 50 })
  billType: string | null;

  @DefColumn("varchar", { name: "REMARK2", nullable: true, length: 50 })
  remark2: string | null;

  @DefColumn("varchar", { name: "CATEGORY", nullable: true, length: 40 })
  category: string | null;
}
