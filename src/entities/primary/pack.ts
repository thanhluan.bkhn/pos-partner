import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("PAC_177_UNIQUE", ["id"], { unique: true })
@Index("IX_PACK_SKU", ["whseid", "packkey"], {})
@Entity("pack", { schema: "swm" })
export class Pack {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "PACKKEY", length: 50 })
  packkey: string;

  @DefColumn("varchar", { name: "PACKDESCR", nullable: true, length: 200 })
  packdescr: string | null;

  @DefColumn("varchar", { name: "PACKUOM1", nullable: true, length: 10 })
  packuom1: string | null;

  @DefColumn("double", {
    name: "CASECNT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  casecnt: number | null;

  @DefColumn("varchar", { name: "ISWHQTY1", nullable: true, length: 1 })
  iswhqty1: string | null;

  @DefColumn("varchar", { name: "REPLENISHUOM1", nullable: true, length: 1 })
  replenishuom1: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE1", nullable: true, length: 10 })
  replenishzone1: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM1", nullable: true, length: 1 })
  cartonizeuom1: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom1: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom1: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom1: number | null;

  @DefColumn("double", {
    name: "CUBEUOM1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeuom1: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM1",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom1: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM1",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom1: number | null;

  @DefColumn("varchar", { name: "PACKUOM2", nullable: true, length: 10 })
  packuom2: string | null;

  @DefColumn("double", {
    name: "INNERPACK",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  innerpack: number | null;

  @DefColumn("tinyint", {
    name: "ISWHQTY2",
    nullable: true,
    default: () => "'0'"
  })
  iswhqty2: number | null;

  @DefColumn("varchar", { name: "REPLENISHUOM2", nullable: true, length: 1 })
  replenishuom2: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE2", nullable: true, length: 10 })
  replenishzone2: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM2", nullable: true, length: 1 })
  cartonizeuom2: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom2: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom2: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom2: number | null;

  @DefColumn("double", {
    name: "CUBEUOM2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeuom2: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM2",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom2: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM2",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom2: number | null;

  @DefColumn("varchar", { name: "PACKUOM3", nullable: true, length: 10 })
  packuom3: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qty: string | null;

  @DefColumn("tinyint", {
    name: "ISWHQTY3",
    nullable: true,
    default: () => "'0'"
  })
  iswhqty3: number | null;

  @DefColumn("varchar", { name: "REPLENISHUOM3", nullable: true, length: 1 })
  replenishuom3: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE3", nullable: true, length: 10 })
  replenishzone3: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM3", nullable: true, length: 1 })
  cartonizeuom3: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM3",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom3: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM3",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom3: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM3",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom3: number | null;

  @DefColumn("double", {
    name: "CUBEUOM3",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeuom3: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM3",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom3: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM3",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom3: number | null;

  @DefColumn("varchar", { name: "PACKUOM4", nullable: true, length: 10 })
  packuom4: string | null;

  @DefColumn("double", {
    name: "PALLET",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  pallet: number | null;

  @DefColumn("tinyint", {
    name: "ISWHQTY4",
    nullable: true,
    default: () => "'0'"
  })
  iswhqty4: number | null;

  @DefColumn("varchar", { name: "REPLENISHUOM4", nullable: true, length: 1 })
  replenishuom4: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE4", nullable: true, length: 10 })
  replenishzone4: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM4", nullable: true, length: 1 })
  cartonizeuom4: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM4",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom4: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM4",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom4: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM4",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom4: number | null;

  @DefColumn("double", {
    name: "CUBEUOM4",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeuom4: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM4",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom4: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM4",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom4: number | null;

  @DefColumn("double", {
    name: "PALLETWOODLENGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  palletwoodlength: number | null;

  @DefColumn("double", {
    name: "PALLETWOODWIDTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  palletwoodwidth: number | null;

  @DefColumn("double", {
    name: "PALLETWOODHEIGHT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  palletwoodheight: number | null;

  @DefColumn("decimal", {
    name: "PALLETTI",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  palletti: string | null;

  @DefColumn("decimal", {
    name: "PALLETHI",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  pallethi: string | null;

  @DefColumn("varchar", { name: "PACKUOM5", nullable: true, length: 10 })
  packuom5: string | null;

  @DefColumn("double", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cube: number | null;

  @DefColumn("varchar", { name: "ISWHQTY5", nullable: true, length: 1 })
  iswhqty5: string | null;

  @DefColumn("int", {
    name: "FilterValueUOM5",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom5: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM5",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom5: number | null;

  @DefColumn("varchar", { name: "PACKUOM6", nullable: true, length: 10 })
  packuom6: string | null;

  @DefColumn("double", {
    name: "GROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgt: number | null;

  @DefColumn("varchar", { name: "ISWHQTY6", nullable: true, length: 1 })
  iswhqty6: string | null;

  @DefColumn("int", {
    name: "FilterValueUOM6",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom6: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM6",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom6: number | null;

  @DefColumn("varchar", { name: "PACKUOM7", nullable: true, length: 10 })
  packuom7: string | null;

  @DefColumn("double", {
    name: "NETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgt: number | null;

  @DefColumn("varchar", { name: "ISWHQTY7", nullable: true, length: 1 })
  iswhqty7: string | null;

  @DefColumn("int", {
    name: "FilterValueUOM7",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom7: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM7",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom7: number | null;

  @DefColumn("varchar", { name: "PACKUOM8", nullable: true, length: 10 })
  packuom8: string | null;

  @DefColumn("double", {
    name: "OTHERUNIT1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit1: number | null;

  @DefColumn("varchar", { name: "ISWHQTY8", nullable: true, length: 1 })
  iswhqty8: string | null;

  @DefColumn("varchar", { name: "REPLENISHUOM8", nullable: true, length: 1 })
  replenishuom8: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE8", nullable: true, length: 10 })
  replenishzone8: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM8", nullable: true, length: 1 })
  cartonizeuom8: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM8",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom8: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM8",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom8: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM8",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom8: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM8",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom8: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM8",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom8: number | null;

  @DefColumn("varchar", { name: "PACKUOM9", nullable: true, length: 10 })
  packuom9: string | null;

  @DefColumn("double", {
    name: "OTHERUNIT2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit2: number | null;

  @DefColumn("varchar", { name: "ISWHQTY9", nullable: true, length: 1 })
  iswhqty9: string | null;

  @DefColumn("varchar", { name: "REPLENISHUOM9", nullable: true, length: 1 })
  replenishuom9: string | null;

  @DefColumn("varchar", { name: "REPLENISHZONE9", nullable: true, length: 10 })
  replenishzone9: string | null;

  @DefColumn("varchar", { name: "CARTONIZEUOM9", nullable: true, length: 1 })
  cartonizeuom9: string | null;

  @DefColumn("double", {
    name: "LENGTHUOM9",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  lengthuom9: number | null;

  @DefColumn("double", {
    name: "WIDTHUOM9",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  widthuom9: number | null;

  @DefColumn("double", {
    name: "HEIGHTUOM9",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heightuom9: number | null;

  @DefColumn("int", {
    name: "FilterValueUOM9",
    nullable: true,
    default: () => "'0'"
  })
  filterValueUom9: number | null;

  @DefColumn("int", {
    name: "IndicatorDigitUOM9",
    nullable: true,
    default: () => "'0'"
  })
  indicatorDigitUom9: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
