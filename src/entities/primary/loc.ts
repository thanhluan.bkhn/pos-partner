import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("LOC_136_UNIQUE", ["id"], { unique: true })
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("loc", { schema: "swm" })
export class Loc {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "LOC", length: 30 })
  loc: string;

  @DefColumn("int", { name: "ORDER", nullable: true, default: () => "'0'" })
  order: number | null;

  @DefColumn("double", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cube: number | null;

  @DefColumn("double", {
    name: "LENGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  length: number | null;

  @DefColumn("double", {
    name: "WIDTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  width: number | null;

  @DefColumn("double", {
    name: "HEIGHT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  height: number | null;

  @DefColumn("varchar", { name: "LOCATIONTYPE", nullable: true, length: 10 })
  locationtype: string | null;

  @DefColumn("varchar", { name: "LOCATIONFLAG", nullable: true, length: 10 })
  locationflag: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONHANDLING",
    nullable: true,
    length: 10
  })
  locationhandling: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONCATEGORY",
    nullable: true,
    length: 10
  })
  locationcategory: string | null;

  @DefColumn("varchar", { name: "LOGICALLOCATION", nullable: true, length: 18 })
  logicallocation: string | null;

  @DefColumn("double", {
    name: "CUBICCAPACITY",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubiccapacity: number | null;

  @DefColumn("double", {
    name: "WEIGHTCAPACITY",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  weightcapacity: number | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("varchar", { name: "LOSEID", nullable: true, length: 1 })
  loseid: string | null;

  @DefColumn("varchar", { name: "FACILITY", nullable: true, length: 20 })
  facility: string | null;

  @DefColumn("varchar", { name: "SECTION", nullable: true, length: 10 })
  section: string | null;

  @DefColumn("varchar", { name: "ABC", nullable: true, length: 1 })
  abc: string | null;

  @DefColumn("varchar", { name: "PICKZONE", nullable: true, length: 10 })
  pickzone: string | null;

  @DefColumn("varchar", { name: "PUTAWAYZONE", nullable: true, length: 10 })
  putawayzone: string | null;

  @DefColumn("varchar", { name: "SECTIONKEY", nullable: true, length: 10 })
  sectionkey: string | null;

  @DefColumn("varchar", { name: "PICKMETHOD", nullable: true, length: 1 })
  pickmethod: string | null;

  @DefColumn("tinyint", {
    name: "COMMINGLESKU",
    nullable: true,
    default: () => "'0'"
  })
  comminglesku: number | null;

  @DefColumn("tinyint", {
    name: "COMMINGLELOT",
    nullable: true,
    default: () => "'0'"
  })
  comminglelot: number | null;

  @DefColumn("int", { name: "LOCLEVEL", nullable: true, default: () => "'0'" })
  loclevel: number | null;

  @DefColumn("int", { name: "XCOORD", nullable: true, default: () => "'0'" })
  xcoord: number | null;

  @DefColumn("int", { name: "YCOORD", nullable: true, default: () => "'0'" })
  ycoord: number | null;

  @DefColumn("int", { name: "ZCOORD", nullable: true, default: () => "'0'" })
  zcoord: number | null;

  @DefColumn("varchar", { name: "OPTLOC", nullable: true, length: 10 })
  optloc: string | null;

  @DefColumn("int", {
    name: "STACKLIMIT",
    nullable: true,
    default: () => "'0'"
  })
  stacklimit: number | null;

  @DefColumn("int", { name: "FOOTPRINT", nullable: true, default: () => "'0'" })
  footprint: number | null;

  @DefColumn("int", {
    name: "ORIENTATION",
    nullable: true,
    default: () => "'0'"
  })
  orientation: number | null;

  @DefColumn("int", {
    name: "INTERLEAVINGSEQUENCE",
    nullable: true,
    default: () => "'0'"
  })
  interleavingsequence: number | null;

  @DefColumn("int", { name: "AutoShip", nullable: true, default: () => "'0'" })
  autoShip: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("int", {
    name: "LOCGROUPID",
    nullable: true,
    default: () => "'0'"
  })
  locgroupid: number | null;

  @DefColumn("varchar", { name: "STORAGETYPE", nullable: true, length: 30 })
  storagetype: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
