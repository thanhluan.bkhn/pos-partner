import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ADJ_5_UNIQUE", ["id"], { unique: true })
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("adjustment", { schema: "swm" })
export class Adjustment {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "ADJUSTMENTKEY", length: 10 })
  adjustmentkey: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("datetime", { name: "EFFECTIVEDATE" })
  effectivedate: Date;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;
}
