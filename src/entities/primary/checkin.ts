import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_CHECKCODE", ["checkcode", "whseid"], { unique: true })
@Index("IX_WHSEID_STATUS", ["whseid", "status"], {})
@Index(
  "IX_BEGINTIME_ENDTIME_STATUS",
  ["whseid", "status", "begintime", "endtime"],
  {}
)
@Entity("checkin", { schema: "swm" })
export class Checkin {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID" })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "TAGID", nullable: true, length: 200 })
  tagid: string | null;

  @DefColumn("varchar", { name: "GATENO", length: 30 })
  gateno: string;

  @DefColumn("varchar", { name: "FULLNAME", nullable: true, length: 200 })
  fullname: string | null;

  @DefColumn("datetime", { name: "CHECKDATE", nullable: true })
  checkdate: Date | null;

  @DefColumn("varchar", { name: "PASSPORTNUMBER", nullable: true, length: 50 })
  passportnumber: string | null;

  @DefColumn("datetime", { name: "CHECKIN", nullable: true })
  checkin: Date | null;

  @DefColumn("datetime", { name: "CHECKOUT", nullable: true })
  checkout: Date | null;

  @DefColumn("varchar", { name: "TRUCKNO", nullable: true, length: 50 })
  truckno: string | null;

  @DefColumn("varchar", { name: "TRUCKTYPE", nullable: true, length: 45 })
  trucktype: string | null;

  @DefColumn("varchar", { name: "CONTAINER", nullable: true, length: 100 })
  container: string | null;

  @DefColumn("int", { name: "QTY", nullable: true, default: () => "'0'" })
  qty: number | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 50 })
  status: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 200 })
  notes: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("tinyint", { name: "DELETED", nullable: true })
  deleted: number | null;

  @DefColumn("varchar", { name: "CHECKCODE", length: 36 })
  checkcode: string;

  @DefColumn("datetime", { name: "BEGINTIME", nullable: true })
  begintime: Date | null;

  @DefColumn("datetime", { name: "ENDTIME", nullable: true })
  endtime: Date | null;

  @DefColumn("varchar", { name: "DOOR", nullable: true, length: 45 })
  door: string | null;

  @DefColumn("varchar", { name: "LABORTEAM", nullable: true, length: 100 })
  laborteam: string | null;

  @DefColumn("varchar", { name: "EMPLOYEE", nullable: true, length: 200 })
  employee: string | null;

  @DefColumn("varchar", { name: "SMSPHONENUMBER", nullable: true, length: 50 })
  smsphonenumber: string | null;
}
