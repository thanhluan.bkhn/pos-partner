import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("IX_Files", ["whseid", "storerkey", "filename"], { unique: true })
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("files", { schema: "swm" })
export class Files {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "whseid", length: 36 })
  whseid: string;

  @DefColumn("varchar", { name: "storerkey", length: 36 })
  storerkey: string;

  @DefColumn("varchar", { name: "filename", length: 200 })
  filename: string;

  @DefColumn("varchar", { name: "extensions", length: 10 })
  extensions: string;

  @DefColumn("varchar", { name: "type", nullable: true, length: 50 })
  type: string | null;

  @DefColumn("int", { name: "size" })
  size: number;

  @DefColumn("varchar", { name: "frompath", nullable: true, length: 200 })
  frompath: string | null;

  @DefColumn("varchar", { name: "topath", nullable: true, length: 200 })
  topath: string | null;

  @DefColumn("datetime", { name: "syncDate", nullable: true })
  syncDate: Date | null;

  @DefColumn("datetime", { name: "wmsSyncDate", nullable: true })
  wmsSyncDate: Date | null;

  @DefColumn("varchar", { name: "wmsSyncStatus", nullable: true, length: 50 })
  wmsSyncStatus: string | null;

  @DefColumn("varchar", { name: "wmsSyncError", nullable: true, length: 5000 })
  wmsSyncError: string | null;

  @DefColumn("datetime", { name: "tmsSyncDate", nullable: true })
  tmsSyncDate: Date | null;

  @DefColumn("varchar", { name: "tmsSyncStatus", nullable: true, length: 50 })
  tmsSyncStatus: string | null;

  @DefColumn("varchar", { name: "tmsSyncError", nullable: true, length: 5000 })
  tmsSyncError: string | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;
}
