import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("USERNAME_UNIQUE", ["referencekey", "referencesource", "whseid"], {
  unique: true
})
@Index("TASKKEY_INDEX", ["taskkey"], {})
@Entity("task", { schema: "swm" })
export class Task {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "TASKKEY", length: 30 })
  taskkey: string;

  @DefColumn("char", { name: "REFERENCEID", nullable: true, length: 36 })
  referenceid: string | null;

  @DefColumn("varchar", { name: "REFERENCEKEY", length: 30 })
  referencekey: string;

  @DefColumn("varchar", { name: "REFERENCESOURCE", length: 45 })
  referencesource: string;

  @DefColumn("varchar", { name: "TASKTYPE", nullable: true, length: 10 })
  tasktype: string | null;

  @DefColumn("datetime", { name: "STARTDATE", nullable: true })
  startdate: Date | null;

  @DefColumn("datetime", { name: "ENDDATE", nullable: true })
  enddate: Date | null;

  @DefColumn("datetime", { name: "ACTUALSTARTDATE", nullable: true })
  actualstartdate: Date | null;

  @DefColumn("datetime", { name: "ACTUALENDDATE", nullable: true })
  actualenddate: Date | null;

  @DefColumn("varchar", { name: "STATUS", length: 45 })
  status: string;

  @DefColumn("varchar", { name: "PRIORITY", length: 45 })
  priority: string;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 200 })
  description: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;
}
