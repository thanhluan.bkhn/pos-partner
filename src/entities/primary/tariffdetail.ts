import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TRA_274_UNIQUE", ["id"], { unique: true })
@Entity("tariffdetail", { schema: "swm" })
export class Tariffdetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TARIFFDETAILKEY", length: 10 })
  tariffdetailkey: string;

  @DefColumn("varchar", { primary: true, name: "TARIFFKEY", length: 10 })
  tariffkey: string;

  @DefColumn("varchar", { primary: true, name: "CHARGETYPE", length: 10 })
  chargetype: string;

  @DefColumn("varchar", { primary: true, name: "CHARGECODE", length: 50 })
  chargecode: string;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 500 })
  description: string | null;

  @DefColumn("decimal", {
    name: "RATE",
    nullable: true,
    precision: 10,
    scale: 0
  })
  rate: string | null;

  @DefColumn("varchar", { name: "BASE", nullable: true, length: 30 })
  base: string | null;

  @DefColumn("varchar", { name: "MASTERUNITS", nullable: true, length: 50 })
  masterunits: string | null;

  @DefColumn("varchar", {
    name: "ROUNDMASTERUNITS",
    nullable: true,
    length: 10
  })
  roundmasterunits: string | null;

  @DefColumn("varchar", { name: "UOMSHOW", nullable: true, length: 10 })
  uomshow: string | null;

  @DefColumn("varchar", { name: "TAXGROUPKEY", nullable: true, length: 10 })
  taxgroupkey: string | null;

  @DefColumn("varchar", {
    name: "GLDISTRIBUTIONKEY",
    nullable: true,
    length: 10
  })
  gldistributionkey: string | null;

  @DefColumn("decimal", {
    name: "MINIMUMCHARGE",
    nullable: true,
    precision: 10,
    scale: 0
  })
  minimumcharge: string | null;

  @DefColumn("varchar", { name: "MINIMUMGROUP", nullable: true, length: 10 })
  minimumgroup: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "COSTRATE", nullable: true, length: 50 })
  costrate: string | null;

  @DefColumn("varchar", { name: "COSTBASE", nullable: true, length: 50 })
  costbase: string | null;

  @DefColumn("varchar", { name: "COSTMASTERUNITS", nullable: true, length: 50 })
  costmasterunits: string | null;

  @DefColumn("varchar", { name: "COSTUOMSHOW", nullable: true, length: 50 })
  costuomshow: string | null;

  @DefColumn("varchar", { name: "UOM1MULT", nullable: true, length: 50 })
  uom1Mult: string | null;

  @DefColumn("varchar", { name: "UOM2MULT", nullable: true, length: 50 })
  uom2Mult: string | null;

  @DefColumn("varchar", { name: "UOM3MULT", nullable: true, length: 50 })
  uom3Mult: string | null;

  @DefColumn("varchar", { name: "UOM4MULT", nullable: true, length: 50 })
  uom4Mult: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("tinyint", { name: "SETDEFAULT", nullable: true })
  setdefault: number | null;

  @DefColumn("int", { name: "MIN", nullable: true })
  min: number | null;

  @DefColumn("int", { name: "MAX", nullable: true })
  max: number | null;

  @DefColumn("varchar", { name: "CATEGORY", nullable: true, length: 40 })
  category: string | null;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;
}
