import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Mappingdetail } from "./mappingdetail";
import { Role } from "./role";

@Index("IX_UserRole", ["userId", "roleId", "mappingDetailId"], { unique: true })
@Index("IX_User", ["userId"], {})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("FK_UserRole_Role_roleId", ["roleId"], {})
@Index("FK_UserRole_MappingDetail_mappingDetailId", ["mappingDetailId"], {})
@Entity("userrole", { schema: "swm" })
export class Userrole {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "userId", length: 36 })
  userId: string;

  @DefColumn("char", { name: "roleId", length: 36 })
  roleId: string;

  @DefColumn("char", { name: "mappingDetailId", nullable: true, length: 36 })
  mappingDetailId: string | null;

  @DefColumn("varchar", {
    name: "mappingDetailCode",
    nullable: true,
    length: 50
  })
  mappingDetailCode: string | null;

  @DefColumn("char", { name: "mappingId", nullable: true, length: 36 })
  mappingId: string | null;

  @DefColumn("varchar", { name: "mappingCode", nullable: true, length: 20 })
  mappingCode: string | null;

  @DefColumn("varchar", { name: "roleCode", nullable: true, length: 20 })
  roleCode: string | null;

  @DefColumn("tinyint", { name: "isActivated", width: 1 })
  isActivated: boolean;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @ManyToOne(
    () => Mappingdetail,
    mappingdetail => mappingdetail.userroles,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "mappingDetailId", referencedColumnName: "id" }])
  mappingDetail: Mappingdetail;

  @ManyToOne(
    () => Role,
    role => role.userroles,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "roleId", referencedColumnName: "id" }])
  role: Role;
}
