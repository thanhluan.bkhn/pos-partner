import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ICH_312_UNIQUE", ["id"], { unique: true })
@Entity("inventoryholdcode", { schema: "swm" })
export class Inventoryholdcode {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "CODE", length: 10 })
  code: string;

  @DefColumn("int", { name: "RANK", nullable: true, default: () => "'0'" })
  rank: number | null;

  @DefColumn("varchar", { name: "DESCRIPTION", nullable: true, length: 250 })
  description: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 30 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 30 })
  editwho: string | null;
}
