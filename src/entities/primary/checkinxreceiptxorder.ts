import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_CHECKCODE", ["checkcode"], {})
@Index("IX_RECEIPTKEY", ["whseid", "storerkey", "receiptkey"], {})
@Entity("checkinxreceiptxorder", { schema: "swm" })
export class Checkinxreceiptxorder {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "CHECKCODE", length: 36 })
  checkcode: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTKEY", length: 10 })
  receiptkey: string;

  @DefColumn("varchar", { primary: true, name: "ORDERKEY", length: 10 })
  orderkey: string;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 45 })
  type: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;
}
