import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("replenishment", { schema: "swm" })
export class Replenishment {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "IMPORTCODE", length: 45 })
  importcode: string;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 3072 })
  remark: string | null;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("varchar", { name: "EDITWHO", length: 50 })
  editwho: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;
}
