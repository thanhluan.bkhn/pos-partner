import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("CITYCODE_UNIQUE", ["citycode", "districtcode"], { unique: true })
@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("district", { schema: "swm" })
export class District {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "DISTRICTCODE", length: 65 })
  districtcode: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "DISTRICTNAME", length: 65 })
  districtname: string;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 65 })
  type: string | null;

  @DefColumn("varchar", { name: "CITYCODE", length: 65 })
  citycode: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("tinyint", { name: "DELETED", nullable: true })
  deleted: number | null;
}
