import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("__migrationhistory", { schema: "swm" })
export class Migrationhistory {
  @DefColumn("varchar", { primary: true, name: "MigrationId", length: 150 })
  migrationId: string;

  @DefColumn("varchar", { name: "ContextKey", length: 300 })
  contextKey: string;

  @DefColumn("longblob", { name: "Model" })
  model: Buffer;

  @DefColumn("varchar", { name: "ProductVersion", length: 32 })
  productVersion: string;
}
