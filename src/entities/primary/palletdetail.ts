import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index("IX_PACLLETDETAIL", ["whseid", "palletid", "lpnid"], {})
@Entity("palletdetail", { schema: "swm" })
export class Palletdetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 45 })
  palletid: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 0
  })
  qty: string | null;

  @DefColumn("varchar", { name: "BOOKINGNO", nullable: true, length: 50 })
  bookingno: string | null;

  @DefColumn("varchar", { name: "PONO", nullable: true, length: 50 })
  pono: string | null;

  @DefColumn("varchar", { name: "STYLE", nullable: true, length: 50 })
  style: string | null;

  @DefColumn("varchar", { name: "SIZE", nullable: true, length: 45 })
  size: string | null;

  @DefColumn("double", { name: "CBM", precision: 22 })
  cbm: number;

  @DefColumn("double", { name: "GROSWEIGHT", precision: 22 })
  grosweight: number;

  @DefColumn("double", { name: "NETWEIGHT", nullable: true, precision: 22 })
  netweight: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;
}
