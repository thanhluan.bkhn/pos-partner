import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Index(
  "DECLARATION_WHSEID_STORERKEY_DECLARATIONNO",
  ["whseid", "storerkey", "declarationno"],
  { unique: true }
)
@Index(
  "WHSEID_STORERKEY_DECLARATION",
  ["whseid", "storerkey", "declarationno"],
  {}
)
@Entity("declaration", { schema: "swm" })
export class Declaration {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID", unique: true })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "DECLARATIONNO", length: 100 })
  declarationno: string;

  @DefColumn("varchar", { name: "SUPPLIERCODE", nullable: true, length: 45 })
  suppliercode: string | null;

  @DefColumn("varchar", {
    name: "DECLARATIONPLACE",
    nullable: true,
    length: 45
  })
  declarationplace: string | null;

  @DefColumn("datetime", { name: "DECLARATIONDATE" })
  declarationdate: Date;

  @DefColumn("varchar", { name: "DECLARATIONTYPE", nullable: true, length: 45 })
  declarationtype: string | null;

  @DefColumn("varchar", {
    name: "PARENTDECLARATION",
    nullable: true,
    length: 100
  })
  parentdeclaration: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 65 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 65 })
  editwho: string | null;
}
