import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("replenishmentstrategy", { schema: "swm" })
export class Replenishmentstrategy {
  @DefColumn("varchar", { name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "whseid", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "replenishmentstrategykey",
    length: 30
  })
  replenishmentstrategykey: string;

  @DefColumn("varchar", { name: "descr", length: 200 })
  descr: string;

  @DefColumn("datetime", { name: "adddate" })
  adddate: Date;

  @DefColumn("varchar", { name: "addwho", length: 50 })
  addwho: string;

  @DefColumn("datetime", { name: "editdate", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "editwho", nullable: true, length: 50 })
  editwho: string | null;
}
