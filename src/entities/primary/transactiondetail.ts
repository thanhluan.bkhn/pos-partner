import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Transaction } from "./transaction";

@Index("IX_TransactionIdxPropertyName", ["transactionId", "propertyName"], {
  unique: true
})
@Index("IX_ReferenceId", ["referenceId"], {})
@Index("IX_ReferenceIdxPropertyName", ["referenceId", "propertyName"], {})
@Index("IX_PropertyName", ["propertyName"], {})
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Entity("transactiondetail", { schema: "swm" })
export class Transactiondetail {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "transactionId", length: 36 })
  transactionId: string;

  @DefColumn("char", { name: "referenceId", length: 36 })
  referenceId: string;

  @DefColumn("varchar", { name: "propertyName", length: 50 })
  propertyName: string;

  @DefColumn("longtext", { name: "oldValue", nullable: true })
  oldValue: string | null;

  @DefColumn("longtext", { name: "newValue", nullable: true })
  newValue: string | null;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @ManyToOne(
    () => Transaction,
    transaction => transaction.transactiondetails,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "transactionId", referencedColumnName: "id" }])
  transaction: Transaction;
}
