import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("REC_226_UNIQUE", ["id"], { unique: true })
@Index("IX_RECEIPTDATE", ["whseid", "receiptkey", "receiptdate"], {})
@Index("IX_POKEY", ["whseid", "pokey"], {})
@Index("IX_GEN_ASN_BY_SO", ["whseid", "generatedfromkey", "generatedfrom"], {})
@Index("IX_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Index(
  "IX_WHSEID_EXPECTEDRECEIPTDATE",
  ["whseid", "storerkey", "expectedreceiptdate"],
  {}
)
@Index("IX_EXPECTEDRECEIPTDATE", ["expectedreceiptdate"], {})
@Entity("receipt", { schema: "swm" })
export class Receipt {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTKEY", length: 10 })
  receiptkey: string;

  @DefColumn("varchar", {
    name: "EXTERNRECEIPTKEY",
    nullable: true,
    length: 32
  })
  externreceiptkey: string | null;

  @DefColumn("varchar", { name: "RECEIPTGROUP", nullable: true, length: 20 })
  receiptgroup: string | null;

  @DefColumn("varchar", { name: "STORERKEY", nullable: true, length: 40 })
  storerkey: string | null;

  @DefColumn("datetime", { name: "RECEIPTDATE", nullable: true })
  receiptdate: Date | null;

  @DefColumn("varchar", { name: "POKEY", nullable: true, length: 18 })
  pokey: string | null;

  @DefColumn("varchar", { name: "CARRIERKEY", nullable: true, length: 40 })
  carrierkey: string | null;

  @DefColumn("varchar", { name: "CARRIERNAME", nullable: true, length: 200 })
  carriername: string | null;

  @DefColumn("varchar", {
    name: "CARRIERADDRESS1",
    nullable: true,
    length: 200
  })
  carrieraddress1: string | null;

  @DefColumn("varchar", { name: "CARRIERADDRESS2", nullable: true, length: 45 })
  carrieraddress2: string | null;

  @DefColumn("varchar", { name: "CARRIERCITY", nullable: true, length: 45 })
  carriercity: string | null;

  @DefColumn("varchar", { name: "CARRIERSTATE", nullable: true, length: 25 })
  carrierstate: string | null;

  @DefColumn("varchar", { name: "CARRIERZIP", nullable: true, length: 18 })
  carrierzip: string | null;

  @DefColumn("varchar", {
    name: "CARRIERREFERENCE",
    nullable: true,
    length: 18
  })
  carrierreference: string | null;

  @DefColumn("varchar", {
    name: "WAREHOUSEREFERENCE",
    nullable: true,
    length: 18
  })
  warehousereference: string | null;

  @DefColumn("varchar", { name: "ORIGINCOUNTRY", nullable: true, length: 30 })
  origincountry: string | null;

  @DefColumn("varchar", {
    name: "DESTINATIONCOUNTRY",
    nullable: true,
    length: 30
  })
  destinationcountry: string | null;

  @DefColumn("varchar", { name: "VEHICLENUMBER", nullable: true, length: 18 })
  vehiclenumber: string | null;

  @DefColumn("datetime", { name: "VEHICLEDATE", nullable: true })
  vehicledate: Date | null;

  @DefColumn("varchar", { name: "PLACEOFLOADING", nullable: true, length: 18 })
  placeofloading: string | null;

  @DefColumn("varchar", {
    name: "PLACEOFDISCHARGE",
    nullable: true,
    length: 30
  })
  placeofdischarge: string | null;

  @DefColumn("varchar", { name: "PLACEOFDELIVERY", nullable: true, length: 30 })
  placeofdelivery: string | null;

  @DefColumn("varchar", { name: "INCOTERMS", nullable: true, length: 10 })
  incoterms: string | null;

  @DefColumn("varchar", { name: "TERMSNOTE", nullable: true, length: 18 })
  termsnote: string | null;

  @DefColumn("varchar", { name: "CONTAINERKEY", nullable: true, length: 100 })
  containerkey: string | null;

  @DefColumn("varchar", { name: "SIGNATORY", nullable: true, length: 18 })
  signatory: string | null;

  @DefColumn("varchar", { name: "PLACEOFISSUE", nullable: true, length: 18 })
  placeofissue: string | null;

  @DefColumn("decimal", {
    name: "OPENQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  openqty: string | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "CONTAINERTYPE", nullable: true, length: 20 })
  containertype: string | null;

  @DefColumn("decimal", {
    name: "CONTAINERQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  containerqty: string | null;

  @DefColumn("decimal", {
    name: "BILLEDCONTAINERQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  billedcontainerqty: string | null;

  @DefColumn("varchar", {
    name: "TRANSPORTATIONMODE",
    nullable: true,
    length: 30
  })
  transportationmode: string | null;

  @DefColumn("varchar", {
    name: "EXTERNALRECEIPTKEY2",
    nullable: true,
    length: 20
  })
  externalreceiptkey2: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "TYPE", length: 25 })
  type: string;

  @DefColumn("varchar", { name: "RMA", nullable: true, length: 30 })
  rma: string | null;

  @DefColumn("datetime", { name: "EXPECTEDRECEIPTDATE", nullable: true })
  expectedreceiptdate: Date | null;

  @DefColumn("varchar", { name: "ALLOWAUTORECEIPT", nullable: true, length: 1 })
  allowautoreceipt: string | null;

  @DefColumn("datetime", { name: "CLOSEDDATE", nullable: true })
  closeddate: Date | null;

  @DefColumn("varchar", { name: "TRACKINVENTORYBY", nullable: true, length: 1 })
  trackinventoryby: string | null;

  @DefColumn("varchar", { name: "CarrierCountry", nullable: true, length: 30 })
  carrierCountry: string | null;

  @DefColumn("varchar", { name: "CarrierPhone", nullable: true, length: 50 })
  carrierPhone: string | null;

  @DefColumn("varchar", { name: "DriverName", nullable: true, length: 25 })
  driverName: string | null;

  @DefColumn("varchar", { name: "TrailerNumber", nullable: true, length: 100 })
  trailerNumber: string | null;

  @DefColumn("varchar", { name: "TrailerOwner", nullable: true, length: 25 })
  trailerOwner: string | null;

  @DefColumn("varchar", { name: "TrailerType", nullable: true, length: 10 })
  trailerType: string | null;

  @DefColumn("datetime", { name: "ArrivalDateTime", nullable: true })
  arrivalDateTime: Date | null;

  @DefColumn("varchar", {
    name: "LottableMatchRequired",
    nullable: true,
    length: 1
  })
  lottableMatchRequired: string | null;

  @DefColumn("varchar", { name: "ADVICENUMBER", nullable: true, length: 30 })
  advicenumber: string | null;

  @DefColumn("datetime", { name: "ADVICEDATE", nullable: true })
  advicedate: Date | null;

  @DefColumn("varchar", {
    name: "PACKINGSLIPNUMBER",
    nullable: true,
    length: 50
  })
  packingslipnumber: string | null;

  @DefColumn("varchar", { name: "RECEIPTID", nullable: true, length: 32 })
  receiptid: string | null;

  @DefColumn("varchar", { name: "SUPPLIERCODE", nullable: true, length: 40 })
  suppliercode: string | null;

  @DefColumn("varchar", { name: "SUPPLIERNAME", nullable: true, length: 200 })
  suppliername: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMADDRESSLINE1",
    nullable: true,
    length: 200
  })
  shipfromaddressline1: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMADDRESSLINE2",
    nullable: true,
    length: 45
  })
  shipfromaddressline2: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMADDRESSLINE3",
    nullable: true,
    length: 45
  })
  shipfromaddressline3: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMADDRESSLINE4",
    nullable: true,
    length: 45
  })
  shipfromaddressline4: string | null;

  @DefColumn("varchar", { name: "SHIPFROMCITY", nullable: true, length: 45 })
  shipfromcity: string | null;

  @DefColumn("varchar", { name: "SHIPFROMSTATE", nullable: true, length: 25 })
  shipfromstate: string | null;

  @DefColumn("varchar", { name: "SHIPFROMZIP", nullable: true, length: 18 })
  shipfromzip: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMISOCOUNTRY",
    nullable: true,
    length: 50
  })
  shipfromisocountry: string | null;

  @DefColumn("varchar", {
    name: "SHIPFROMCONTACT",
    nullable: true,
    length: 200
  })
  shipfromcontact: string | null;

  @DefColumn("varchar", { name: "SHIPFROMPHONE", nullable: true, length: 50 })
  shipfromphone: string | null;

  @DefColumn("varchar", { name: "SHIPFROMEMAIL", nullable: true, length: 60 })
  shipfromemail: string | null;

  @DefColumn("datetime", { name: "SCHEDULEDSHIPDATE", nullable: true })
  scheduledshipdate: Date | null;

  @DefColumn("datetime", { name: "ACTUALSHIPDATE", nullable: true })
  actualshipdate: Date | null;

  @DefColumn("decimal", {
    name: "GROSSWEIGHT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  grossweight: string | null;

  @DefColumn("decimal", {
    name: "TOTALVOLUME",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  totalvolume: string | null;

  @DefColumn("varchar", { name: "SOURCELOCATION", nullable: true, length: 64 })
  sourcelocation: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCETYPE", nullable: true, length: 64 })
  referencetype: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEDOCUMENT",
    nullable: true,
    length: 64
  })
  referencedocument: string | null;

  @DefColumn("varchar", {
    name: "REFERENCELOCATION",
    nullable: true,
    length: 64
  })
  referencelocation: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEVERSION",
    nullable: true,
    length: 20
  })
  referenceversion: string | null;

  @DefColumn("varchar", { name: "DOOR", nullable: true, length: 10 })
  door: string | null;

  @DefColumn("varchar", { name: "APPOINTMENTKEY", nullable: true, length: 10 })
  appointmentkey: string | null;

  @DefColumn("varchar", {
    name: "ReferenceAccountingEntity",
    nullable: true,
    length: 64
  })
  referenceAccountingEntity: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 3072 })
  notes: string | null;

  @DefColumn("varchar", { name: "DRIVER", nullable: true, length: 100 })
  driver: string | null;

  @DefColumn("varchar", { name: "KEEPER", nullable: true, length: 100 })
  keeper: string | null;

  @DefColumn("varchar", { name: "LIFTER", nullable: true, length: 100 })
  lifter: string | null;

  @DefColumn("varchar", { name: "TRANSNO", nullable: true, length: 10 })
  transno: string | null;

  @DefColumn("varchar", { name: "GATE", nullable: true, length: 50 })
  gate: string | null;

  @DefColumn("datetime", { name: "BEGINTIME", nullable: true })
  begintime: Date | null;

  @DefColumn("datetime", { name: "ENDTIME", nullable: true })
  endtime: Date | null;

  @DefColumn("varchar", { name: "IMPORT", nullable: true, length: 50 })
  import: string | null;

  @DefColumn("varchar", { name: "ID_CUR", nullable: true, length: 3 })
  idCur: string | null;

  @DefColumn("double", {
    name: "RATE_CUR",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  rateCur: number | null;

  @DefColumn("decimal", {
    name: "AMOUNT_GOOD",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountGood: string | null;

  @DefColumn("decimal", {
    name: "TOTAL_AMOUNT_REDUCTION",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  totalAmountReduction: string | null;

  @DefColumn("double", {
    name: "AMOUNT_VAT_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountVatTax: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_REDUCTION_PAYMENT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfReductionPayment: number | null;

  @DefColumn("double", {
    name: "AMOUNT_REDUCTION_PAYMENT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountReductionPayment: number | null;

  @DefColumn("varchar", {
    name: "ID_DECLARATION",
    nullable: true,
    length: 1024
  })
  idDeclaration: string | null;

  @DefColumn("datetime", { name: "DATE_DECLARATION", nullable: true })
  dateDeclaration: Date | null;

  @DefColumn("varchar", { name: "ID_ENT_CUS", nullable: true, length: 31 })
  idEntCus: string | null;

  @DefColumn("varchar", { name: "ID_ENT_VOU", nullable: true, length: 31 })
  idEntVou: string | null;

  @DefColumn("varchar", { name: "TAXCODE_VOU", nullable: true, length: 31 })
  taxcodeVou: string | null;

  @DefColumn("varchar", { name: "INVOICE_NO_SAI", nullable: true, length: 31 })
  invoiceNoSai: string | null;

  @DefColumn("varchar", {
    name: "INVOICE_SERIE_SAI",
    nullable: true,
    length: 31
  })
  invoiceSerieSai: string | null;

  @DefColumn("varchar", { name: "INVOICE_TYPE", nullable: true, length: 127 })
  invoiceType: string | null;

  @DefColumn("datetime", { name: "INVOICE_DATE_SAI", nullable: true })
  invoiceDateSai: Date | null;

  @DefColumn("varchar", { name: "NAME_CUS_VOU", nullable: true, length: 127 })
  nameCusVou: string | null;

  @DefColumn("double", {
    name: "PERCENT_OF_IMPORT_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfImportTax: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_VAT_NK",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfVatNk: number | null;

  @DefColumn("double", {
    name: "AMOUNT_BE_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountBeTax: number | null;

  @DefColumn("varchar", { name: "TRANSFERSTATUS", nullable: true, length: 10 })
  transferstatus: string | null;

  @DefColumn("varchar", { name: "BILLOFLADING", nullable: true, length: 20 })
  billoflading: string | null;

  @DefColumn("varchar", { name: "BOOKINGNO", nullable: true, length: 30 })
  bookingno: string | null;

  @DefColumn("varchar", { name: "CONTRACT", nullable: true, length: 50 })
  contract: string | null;

  @DefColumn("datetime", { name: "CONTRACTDATE", nullable: true })
  contractdate: Date | null;

  @DefColumn("varchar", { name: "CONTTYPE", nullable: true, length: 100 })
  conttype: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTKEY_OWNER",
    nullable: true,
    length: 20
  })
  receiptkeyOwner: string | null;

  @DefColumn("varchar", { name: "SEAL", nullable: true, length: 30 })
  seal: string | null;

  @DefColumn("decimal", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  cube: string | null;

  @DefColumn("varchar", { name: "RECEIPTTIME", nullable: true, length: 30 })
  receipttime: string | null;

  @DefColumn("varchar", { name: "FCDS", nullable: true, length: 30 })
  fcds: string | null;

  @DefColumn("varchar", { name: "INCDS", nullable: true, length: 30 })
  incds: string | null;

  @DefColumn("varchar", { name: "CATEGORY", nullable: true, length: 30 })
  category: string | null;

  @DefColumn("varchar", {
    name: "BONDEDWAREHOUSECONTRACT",
    nullable: true,
    length: 30
  })
  bondedwarehousecontract: string | null;

  @DefColumn("datetime", { name: "BWCDate1", nullable: true })
  bwcDate1: Date | null;

  @DefColumn("datetime", { name: "BWCDate2", nullable: true })
  bwcDate2: Date | null;

  @DefColumn("varchar", { name: "HTSCODE", nullable: true, length: 30 })
  htscode: string | null;

  @DefColumn("varchar", { name: "FACTORY", nullable: true, length: 100 })
  factory: string | null;

  @DefColumn("varchar", { name: "VENDORCODE", nullable: true, length: 40 })
  vendorcode: string | null;

  @DefColumn("varchar", { name: "VENDORNAME", nullable: true, length: 200 })
  vendorname: string | null;

  @DefColumn("datetime", { name: "RECEIPTDAY", nullable: true })
  receiptday: Date | null;

  @DefColumn("datetime", { name: "PODATE", nullable: true })
  podate: Date | null;

  @DefColumn("datetime", { name: "GACDATE", nullable: true })
  gacdate: Date | null;

  @DefColumn("varchar", { name: "BUYER", nullable: true, length: 45 })
  buyer: string | null;

  @DefColumn("varchar", { name: "CONSIGNEEKEY", nullable: true, length: 45 })
  consigneekey: string | null;

  @DefColumn("varchar", { name: "NOTIFYPARTYKEY", nullable: true, length: 45 })
  notifypartykey: string | null;

  @DefColumn("varchar", { name: "DESTCODE", nullable: true, length: 45 })
  destcode: string | null;

  @DefColumn("varchar", { name: "SHIPTO", nullable: true, length: 45 })
  shipto: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("datetime", { name: "DOCUMENTTIME", nullable: true })
  documenttime: Date | null;

  @DefColumn("varchar", { name: "SHIPPINGLINES", nullable: true, length: 100 })
  shippinglines: string | null;

  @DefColumn("varchar", { name: "VESSELKEY", nullable: true, length: 45 })
  vesselkey: string | null;

  @DefColumn("varchar", { name: "VOYAGEKEY", nullable: true, length: 45 })
  voyagekey: string | null;

  @DefColumn("varchar", { name: "PHONENUMBER", nullable: true, length: 50 })
  phonenumber: string | null;

  @DefColumn("varchar", { name: "ADDRESS", nullable: true, length: 200 })
  address: string | null;

  @DefColumn("varchar", { name: "XDOCKKEY", nullable: true, length: 30 })
  xdockkey: string | null;

  @DefColumn("varchar", { name: "POKEY2", nullable: true, length: 100 })
  pokey2: string | null;

  @DefColumn("varchar", {
    name: "GENERATEDFROMKEY",
    nullable: true,
    length: 45
  })
  generatedfromkey: string | null;

  @DefColumn("varchar", { name: "GENERATEDFROM", nullable: true, length: 45 })
  generatedfrom: string | null;

  @DefColumn("varchar", { name: "NOTES2", nullable: true, length: 3072 })
  notes2: string | null;

  @DefColumn("varchar", { name: "GENERATEDCODE", nullable: true, length: 50 })
  generatedcode: string | null;

  @DefColumn("tinyint", {
    name: "RECEIVEDOCUMENT",
    nullable: true,
    default: () => "'0'"
  })
  receivedocument: number | null;

  @DefColumn("tinyint", {
    name: "APPROVE",
    nullable: true,
    default: () => "'0'"
  })
  approve: number | null;
}
