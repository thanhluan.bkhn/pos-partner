import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TRA_274_UNIQUE", ["id"], { unique: true })
@Index("IX_ADDDATE", ["adddate"], {})
@Entity("transfer", { schema: "swm" })
export class Transfer {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TRANSFERKEY", length: 10 })
  transferkey: string;

  @DefColumn("varchar", { name: "FROMWHSEID", nullable: true, length: 30 })
  fromwhseid: string | null;

  @DefColumn("varchar", { name: "TOWHSEID", nullable: true, length: 30 })
  towhseid: string | null;

  @DefColumn("varchar", { name: "FROMSTORERKEY", length: 30 })
  fromstorerkey: string;

  @DefColumn("varchar", { name: "TOSTORERKEY", length: 30 })
  tostorerkey: string;

  @DefColumn("varchar", { name: "TYPE", length: 18 })
  type: string;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("decimal", {
    name: "OPENQTY",
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  openqty: string;

  @DefColumn("varchar", {
    name: "GENERATEHOCHARGES",
    nullable: true,
    length: 10
  })
  generatehocharges: string | null;

  @DefColumn("varchar", {
    name: "GENERATEIS_HICHARGES",
    nullable: true,
    length: 10
  })
  generateisHicharges: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE" })
  effectivedate: Date;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "RELOT", nullable: true, length: 1 })
  relot: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
