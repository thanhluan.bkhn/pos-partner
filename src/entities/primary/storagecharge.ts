import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("storagecharge", { schema: "swm" })
export class Storagecharge {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "WHSEID", nullable: true, length: 20 })
  whseid: string | null;

  @DefColumn("varchar", { name: "STORERKEY", nullable: true, length: 40 })
  storerkey: string | null;

  @DefColumn("varchar", { name: "RECEIPTKEY", nullable: true, length: 255 })
  receiptkey: string | null;

  @DefColumn("varchar", { name: "ORDERKEY", nullable: true, length: 255 })
  orderkey: string | null;

  @DefColumn("varchar", { name: "CONTAINERTYPE", nullable: true, length: 255 })
  containertype: string | null;

  @DefColumn("int", { name: "TOTALQTY", nullable: true })
  totalqty: number | null;

  @DefColumn("int", { name: "PRODUCT", nullable: true })
  product: number | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 255 })
  addwho: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 255 })
  editwho: string | null;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 245 })
  remark: string | null;

  @DefColumn("varchar", { name: "BILLTYPE", nullable: true, length: 50 })
  billtype: string | null;
}
