import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TAS_262_UNIQUE", ["id"], { unique: true })
@Index("IX_LPNID", ["whseid", "lpnid"], {})
@Index("IX_SOURCEKEY", ["whseid", "sourcekey"], {})
@Entity("taskdetail", { schema: "swm" })
export class Taskdetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "TASKDETAILKEY", length: 15 })
  taskdetailkey: string;

  @DefColumn("varchar", { name: "TASKTYPE", length: 10 })
  tasktype: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "LOT", length: 10 })
  lot: string;

  @DefColumn("varchar", { name: "UOM", length: 10 })
  uom: string;

  @DefColumn("decimal", { name: "UOMQTY", precision: 22, scale: 5 })
  uomqty: string;

  @DefColumn("decimal", { name: "QTY", precision: 22, scale: 5 })
  qty: string;

  @DefColumn("varchar", { name: "FROMLOC", length: 30 })
  fromloc: string;

  @DefColumn("varchar", { name: "LOGICALFROMLOC", nullable: true, length: 30 })
  logicalfromloc: string | null;

  @DefColumn("varchar", { name: "FROMID", length: 18 })
  fromid: string;

  @DefColumn("varchar", { name: "TOLOC", length: 30 })
  toloc: string;

  @DefColumn("varchar", { name: "LOGICALTOLOC", nullable: true, length: 30 })
  logicaltoloc: string | null;

  @DefColumn("varchar", { name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { name: "CASEID", nullable: true, length: 20 })
  caseid: string | null;

  @DefColumn("varchar", { name: "PICKMETHOD", nullable: true, length: 10 })
  pickmethod: string | null;

  @DefColumn("varchar", { name: "STATUS", length: 10 })
  status: string;

  @DefColumn("varchar", { name: "STATUSMSG", nullable: true, length: 255 })
  statusmsg: string | null;

  @DefColumn("varchar", { name: "PRIORITY", nullable: true, length: 10 })
  priority: string | null;

  @DefColumn("varchar", { name: "SOURCEPRIORITY", nullable: true, length: 10 })
  sourcepriority: string | null;

  @DefColumn("varchar", { name: "HOLDKEY", nullable: true, length: 10 })
  holdkey: string | null;

  @DefColumn("varchar", { name: "USERKEY", nullable: true, length: 18 })
  userkey: string | null;

  @DefColumn("varchar", { name: "USERPOSITION", nullable: true, length: 10 })
  userposition: string | null;

  @DefColumn("varchar", { name: "USERKEYOVERRIDE", nullable: true, length: 18 })
  userkeyoverride: string | null;

  @DefColumn("datetime", { name: "STARTTIME" })
  starttime: Date;

  @DefColumn("datetime", { name: "ENDTIME" })
  endtime: Date;

  @DefColumn("varchar", { name: "SOURCETYPE", nullable: true, length: 30 })
  sourcetype: string | null;

  @DefColumn("varchar", { name: "SOURCEKEY", nullable: true, length: 30 })
  sourcekey: string | null;

  @DefColumn("varchar", { name: "PICKDETAILKEY", nullable: true, length: 10 })
  pickdetailkey: string | null;

  @DefColumn("varchar", { name: "ORDERKEY", nullable: true, length: 10 })
  orderkey: string | null;

  @DefColumn("varchar", { name: "ORDERLINENUMBER", nullable: true, length: 5 })
  orderlinenumber: string | null;

  @DefColumn("varchar", { name: "LISTKEY", nullable: true, length: 10 })
  listkey: string | null;

  @DefColumn("varchar", { name: "WAVEKEY", nullable: true, length: 10 })
  wavekey: string | null;

  @DefColumn("varchar", { name: "REASONKEY", nullable: true, length: 10 })
  reasonkey: string | null;

  @DefColumn("varchar", { name: "MESSAGE01", nullable: true, length: 20 })
  message01: string | null;

  @DefColumn("varchar", { name: "MESSAGE02", nullable: true, length: 20 })
  message02: string | null;

  @DefColumn("varchar", { name: "MESSAGE03", nullable: true, length: 20 })
  message03: string | null;

  @DefColumn("varchar", { name: "FINALTOLOC", nullable: true, length: 10 })
  finaltoloc: string | null;

  @DefColumn("datetime", { name: "RELEASEDATE", nullable: true })
  releasedate: Date | null;

  @DefColumn("varchar", { name: "OPTBATCHID", nullable: true, length: 10 })
  optbatchid: string | null;

  @DefColumn("varchar", { name: "OPTTASKSEQUENCE", nullable: true, length: 10 })
  opttasksequence: string | null;

  @DefColumn("varchar", {
    name: "OPTREPLENISHMENTUOM",
    nullable: true,
    length: 10
  })
  optreplenishmentuom: string | null;

  @DefColumn("decimal", {
    name: "OPTQTYLOCMINIMUM",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  optqtylocminimum: string | null;

  @DefColumn("varchar", { name: "OPTLOCATIONTYPE", nullable: true, length: 10 })
  optlocationtype: string | null;

  @DefColumn("decimal", {
    name: "OPTQTYLOCLIMIT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  optqtyloclimit: string | null;

  @DefColumn("int", { name: "SEQNO", nullable: true, default: () => "'0'" })
  seqno: number | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 25 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 25 })
  editwho: string;

  @DefColumn("varchar", { name: "DOOR", nullable: true, length: 10 })
  door: string | null;

  @DefColumn("varchar", { name: "ROUTE", nullable: true, length: 10 })
  route: string | null;

  @DefColumn("varchar", { name: "STOP", nullable: true, length: 10 })
  stop: string | null;

  @DefColumn("varchar", { name: "PUTAWAYZONE", nullable: true, length: 10 })
  putawayzone: string | null;

  @DefColumn("varchar", { name: "UASSGNNUMBER", nullable: true, length: 10 })
  uassgnnumber: string | null;

  @DefColumn("varchar", { name: "EQUIPMENT", nullable: true, length: 10 })
  equipment: string | null;

  @DefColumn("varchar", { name: "EQUIPMENTID", nullable: true, length: 50 })
  equipmentid: string | null;

  @DefColumn("decimal", {
    name: "STANDARD",
    nullable: true,
    precision: 12,
    scale: 2,
    default: () => "'0.00'"
  })
  standard: string | null;

  @DefColumn("varchar", { name: "SUBTASK", nullable: true, length: 10 })
  subtask: string | null;

  @DefColumn("varchar", { name: "CLUSTERKEY", nullable: true, length: 10 })
  clusterkey: string | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 32 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 32 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 32 })
  palletid: string | null;

  @DefColumn("varchar", { name: "conditioncode", nullable: true, length: 50 })
  conditioncode: string | null;
}
