import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Connection } from "./connection";
import { User } from "./user";
import { File } from "./file";
import { Warehouse } from "./warehouse";

@Index("IX_Code", ["code"], { unique: true })
@Index("IX_createdBy", ["createdBy"], {})
@Index("IX_modifiedBy", ["modifiedBy"], {})
@Index("IX_connectionId", ["connectionId"], {})
@Entity("client", { schema: "swm" })
export class Client {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "code", unique: true, length: 50 })
  code: string;

  @DefColumn("longtext", { name: "name", nullable: true })
  name: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("char", { name: "connectionId", nullable: true, length: 36 })
  connectionId: string | null;

  @ManyToOne(
    () => Connection,
    connection => connection.clients,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "connectionId", referencedColumnName: "id" }])
  connection: Connection;

  @ManyToOne(
    () => User,
    user => user.clients,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "createdBy", referencedColumnName: "id" }])
  createdBy2: User;

  @ManyToOne(
    () => User,
    user => user.clients2,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "modifiedBy", referencedColumnName: "id" }])
  modifiedBy2: User;

  @OneToMany(
    () => File,
    file => file.client
  )
  files: File[];

  @OneToMany(
    () => User,
    user => user.client
  )
  users: User[];

  @OneToMany(
    () => Warehouse,
    warehouse => warehouse.client
  )
  warehouses: Warehouse[];
}
