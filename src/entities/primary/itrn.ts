import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ITR_121_UNIQUE", ["id"], { unique: true })
@Index("ITR_WHSEID_STORERKEY_TRANTYPE", ["whseid", "storerkey", "trantype"], {})
@Index("ITR_WHSEID_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_WHSEID_FROMLPNID", ["whseid", "fromlpnid"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Index(
  "IX_ADDDATE_ADDWHO_TRANTYPE",
  ["whseid", "trantype", "adddate", "addwho"],
  {}
)
@Index(
  "IX_TOWHSEID_TOLPNID_EDITDATE_TRANTYPE",
  ["towhseid", "tolpnid", "editdate", "trantype"],
  {}
)
@Index("idx_itrn_WHSEID_TOLPNID", ["whseid", "tolpnid"], {})
@Entity("itrn", { schema: "swm" })
export class Itrn {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "ITRNKEY", length: 15 })
  itrnkey: string;

  @DefColumn("varchar", { name: "TRANTYPE", nullable: true, length: 20 })
  trantype: string | null;

  @DefColumn("varchar", { name: "FROMWHSEID", nullable: true, length: 30 })
  fromwhseid: string | null;

  @DefColumn("varchar", { name: "TOWHSEID", nullable: true, length: 30 })
  towhseid: string | null;

  @DefColumn("varchar", { name: "STORERKEY", nullable: true, length: 40 })
  storerkey: string | null;

  @DefColumn("varchar", { name: "TOSTORERKEY", nullable: true, length: 30 })
  tostorerkey: string | null;

  @DefColumn("varchar", { name: "SKU", nullable: true, length: 50 })
  sku: string | null;

  @DefColumn("varchar", { name: "TOSKU", nullable: true, length: 50 })
  tosku: string | null;

  @DefColumn("varchar", { name: "SOURCEKEY", nullable: true, length: 50 })
  sourcekey: string | null;

  @DefColumn("varchar", { name: "SOURCETYPE", nullable: true, length: 30 })
  sourcetype: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qty: string | null;

  @DefColumn("decimal", {
    name: "TOQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  toqty: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "TOUOM", nullable: true, length: 10 })
  touom: string | null;

  @DefColumn("decimal", {
    name: "UOMQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  uomqty: string | null;

  @DefColumn("decimal", {
    name: "TOUOMQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  touomqty: string | null;

  @DefColumn("varchar", { name: "LOT", nullable: true, length: 10 })
  lot: string | null;

  @DefColumn("varchar", { name: "FROMLOC", nullable: true, length: 30 })
  fromloc: string | null;

  @DefColumn("varchar", { name: "TOLOC", nullable: true, length: 30 })
  toloc: string | null;

  @DefColumn("varchar", { name: "FROMLPNID", nullable: true, length: 18 })
  fromlpnid: string | null;

  @DefColumn("varchar", { name: "TOLPNID", nullable: true, length: 18 })
  tolpnid: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 50 })
  status: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 100 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 100 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 100 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 100 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 100 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 100 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 100 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 100 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("timestamp", {
    name: "ADDDATE",
    default: () => "'CURRENT_TIMESTAMP(6)'"
  })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 50 })
  addwho: string;

  @DefColumn("timestamp", {
    name: "EDITDATE",
    default: () => "'0000-00-00 00:00:00.000000'"
  })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "INTRANSIT", nullable: true, length: 1 })
  intransit: string | null;

  @DefColumn("varchar", { name: "Remark", nullable: true, length: 4000 })
  remark: string | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 30 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 30 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 32 })
  palletid: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE01", nullable: true, length: 100 })
  tolottable01: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE02", nullable: true, length: 100 })
  tolottable02: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE03", nullable: true, length: 100 })
  tolottable03: string | null;

  @DefColumn("datetime", { name: "TOLOTTABLE04", nullable: true })
  tolottable04: Date | null;

  @DefColumn("datetime", { name: "TOLOTTABLE05", nullable: true })
  tolottable05: Date | null;

  @DefColumn("varchar", { name: "TOLOTTABLE06", nullable: true, length: 100 })
  tolottable06: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE07", nullable: true, length: 100 })
  tolottable07: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE08", nullable: true, length: 100 })
  tolottable08: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE09", nullable: true, length: 100 })
  tolottable09: string | null;

  @DefColumn("varchar", { name: "TOLOTTABLE10", nullable: true, length: 100 })
  tolottable10: string | null;

  @DefColumn("datetime", { name: "TOLOTTABLE11", nullable: true })
  tolottable11: Date | null;

  @DefColumn("datetime", { name: "TOLOTTABLE12", nullable: true })
  tolottable12: Date | null;

  @DefColumn("varchar", { name: "TOUNITID", nullable: true, length: 30 })
  tounitid: string | null;

  @DefColumn("varchar", { name: "TOCARTONID", nullable: true, length: 30 })
  tocartonid: string | null;

  @DefColumn("varchar", { name: "TOPALLETID", nullable: true, length: 32 })
  topalletid: string | null;

  @DefColumn("varchar", { name: "TOLOT", nullable: true, length: 10 })
  tolot: string | null;

  @DefColumn("varchar", { name: "TOSTATUS", nullable: true, length: 50 })
  tostatus: string | null;
}
