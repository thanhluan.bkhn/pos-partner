import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("TASKID_UNIQUE", ["taskid", "assignee"], { unique: true })
@Entity("taskassignee", { schema: "swm" })
export class Taskassignee {
  @DefColumn("char", { primary: true, name: "ID", length: 36 })
  id: string;

  @DefColumn("char", { name: "TASKID", length: 36 })
  taskid: string;

  @DefColumn("varchar", { name: "TASKKEY", length: 30 })
  taskkey: string;

  @DefColumn("varchar", { name: "ASSIGNEE", length: 45 })
  assignee: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;
}
