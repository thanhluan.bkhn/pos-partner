import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("UQ_ID", ["id"], { unique: true })
@Index("IX_WHSEID", ["whseid"], {})
@Index("IX_PACKKEY", ["whseid", "packkey"], {})
@Index("IX_STORERKEY", ["whseid", "storerkey"], {})
@Index("IX_RECEIPTKEY", ["whseid", "receiptkey"], {})
@Index("IX_SKU", ["whseid", "storerkey", "sku"], {})
@Index(
  "IX_SKU_DATERECEIVED",
  ["whseid", "storerkey", "sku", "datereceived"],
  {}
)
@Index("IX_SKU_TOLOT", ["whseid", "storerkey", "sku", "tolot"], {})
@Index("IX_DATERECEIVED", ["whseid", "datereceived"], {})
@Index("IX_DATERECEIVED_STATUS", ["whseid", "datereceived", "status"], {})
@Index("IX_PODETAIL", ["whseid", "pokey", "polinenumber"], {})
@Index("IX_LPNID", ["whseid", "lpnid"], {})
@Index("IX_ADDDATE", ["adddate"], {})
@Index(
  "IX_LOTTABLE",
  [
    "whseid",
    "sku",
    "lottable01",
    "lottable02",
    "lottable03",
    "lottable04",
    "lottable05",
    "lottable06",
    "lottable07",
    "lottable08",
    "lottable09",
    "lottable10",
    "lottable11",
    "lottable12"
  ],
  {}
)
@Entity("receiptdetail", { schema: "swm" })
export class Receiptdetail {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTKEY", length: 10 })
  receiptkey: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTLINENUMBER", length: 5 })
  receiptlinenumber: string;

  @DefColumn("varchar", { name: "SUBLINENUMBER", nullable: true, length: 20 })
  sublinenumber: string | null;

  @DefColumn("varchar", {
    name: "EXTERNRECEIPTKEY",
    nullable: true,
    length: 32
  })
  externreceiptkey: string | null;

  @DefColumn("varchar", { name: "EXTERNLINENO", nullable: true, length: 20 })
  externlineno: string | null;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "POKEY", nullable: true, length: 18 })
  pokey: string | null;

  @DefColumn("varchar", { name: "TARIFFKEY", nullable: true, length: 10 })
  tariffkey: string | null;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "ALTSKU", nullable: true, length: 50 })
  altsku: string | null;

  @DefColumn("varchar", { name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("datetime", { name: "DATERECEIVED", nullable: true })
  datereceived: Date | null;

  @DefColumn("decimal", {
    name: "QTYEXPECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyexpected: string | null;

  @DefColumn("decimal", {
    name: "QTYADJUSTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyadjusted: string | null;

  @DefColumn("decimal", {
    name: "QTYRECEIVED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyreceived: string | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("varchar", { name: "VESSELKEY", nullable: true, length: 18 })
  vesselkey: string | null;

  @DefColumn("varchar", { name: "VOYAGEKEY", nullable: true, length: 18 })
  voyagekey: string | null;

  @DefColumn("varchar", { name: "XDOCKKEY", nullable: true, length: 18 })
  xdockkey: string | null;

  @DefColumn("varchar", { name: "CONTAINERKEY", nullable: true, length: 40 })
  containerkey: string | null;

  @DefColumn("varchar", { name: "TOLOC", nullable: true, length: 30 })
  toloc: string | null;

  @DefColumn("varchar", { name: "TOLOT", nullable: true, length: 10 })
  tolot: string | null;

  @DefColumn("varchar", { name: "CONDITIONCODE", nullable: true, length: 50 })
  conditioncode: string | null;

  @DefColumn("decimal", {
    name: "CASECNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  casecnt: string | null;

  @DefColumn("decimal", {
    name: "INNERPACK",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  innerpack: string | null;

  @DefColumn("decimal", {
    name: "PALLET",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  pallet: string | null;

  @DefColumn("double", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cube: number | null;

  @DefColumn("double", {
    name: "GROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgt: number | null;

  @DefColumn("double", {
    name: "NETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgt: number | null;

  @DefColumn("double", {
    name: "OTHERUNIT1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit1: number | null;

  @DefColumn("double", {
    name: "OTHERUNIT2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit2: number | null;

  @DefColumn("double", {
    name: "UNITPRICE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  unitprice: number | null;

  @DefColumn("double", {
    name: "EXTENDEDPRICE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  extendedprice: number | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 2000 })
  notes: string | null;

  @DefColumn("varchar", { name: "REASONCODE", nullable: true, length: 20 })
  reasoncode: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 32 })
  palletid: string | null;

  @DefColumn("decimal", {
    name: "QTYREJECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyrejected: string | null;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 10 })
  type: string | null;

  @DefColumn("varchar", { name: "RETURNTYPE", nullable: true, length: 10 })
  returntype: string | null;

  @DefColumn("varchar", { name: "RETURNREASON", nullable: true, length: 10 })
  returnreason: string | null;

  @DefColumn("varchar", { name: "DISPOSITIONTYPE", nullable: true, length: 10 })
  dispositiontype: string | null;

  @DefColumn("varchar", { name: "DISPOSITIONCODE", nullable: true, length: 10 })
  dispositioncode: string | null;

  @DefColumn("varchar", { name: "RETURNCONDITION", nullable: true, length: 10 })
  returncondition: string | null;

  @DefColumn("varchar", { name: "QCREQUIRED", nullable: true, length: 1 })
  qcrequired: string | null;

  @DefColumn("decimal", {
    name: "QCQTYINSPECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qcqtyinspected: string | null;

  @DefColumn("decimal", {
    name: "QCQTYREJECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qcqtyrejected: string | null;

  @DefColumn("varchar", { name: "QCREJREASON", nullable: true, length: 10 })
  qcrejreason: string | null;

  @DefColumn("varchar", { name: "QCSTATUS", nullable: true, length: 1 })
  qcstatus: string | null;

  @DefColumn("varchar", { name: "QCUSER", nullable: true, length: 18 })
  qcuser: string | null;

  @DefColumn("varchar", { name: "QCAUTOADJUST", nullable: true, length: 1 })
  qcautoadjust: string | null;

  @DefColumn("varchar", { name: "EXTERNALLOT", nullable: true, length: 100 })
  externallot: string | null;

  @DefColumn("varchar", { name: "RMA", nullable: true, length: 16 })
  rma: string | null;

  @DefColumn("decimal", {
    name: "PACKINGSLIPQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  packingslipqty: string | null;

  @DefColumn("varchar", { name: "IPSKey", nullable: true, length: 10 })
  ipsKey: string | null;

  @DefColumn("varchar", { name: "SupplierName", nullable: true, length: 25 })
  supplierName: string | null;

  @DefColumn("varchar", { name: "SupplierKey", nullable: true, length: 40 })
  supplierKey: string | null;

  @DefColumn("varchar", { name: "MatchLottable", nullable: true, length: 1 })
  matchLottable: string | null;

  @DefColumn("varchar", { name: "RECEIPTDETAILID", nullable: true, length: 32 })
  receiptdetailid: string | null;

  @DefColumn("varchar", { name: "POLINENUMBER", nullable: true, length: 15 })
  polinenumber: string | null;

  @DefColumn("varchar", { name: "SOURCELOCATION", nullable: true, length: 64 })
  sourcelocation: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCETYPE", nullable: true, length: 64 })
  referencetype: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEDOCUMENT",
    nullable: true,
    length: 64
  })
  referencedocument: string | null;

  @DefColumn("varchar", {
    name: "REFERENCELOCATION",
    nullable: true,
    length: 64
  })
  referencelocation: string | null;

  @DefColumn("varchar", {
    name: "REFERENCEVERSION",
    nullable: true,
    length: 20
  })
  referenceversion: string | null;

  @DefColumn("varchar", { name: "REFERENCELINE", nullable: true, length: 20 })
  referenceline: string | null;

  @DefColumn("decimal", {
    name: "CUBICMETER",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  cubicmeter: string | null;

  @DefColumn("decimal", {
    name: "HUNDREDWEIGHT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  hundredweight: string | null;

  @DefColumn("varchar", {
    name: "ReferenceAccountingEntity",
    nullable: true,
    length: 64
  })
  referenceAccountingEntity: string | null;

  @DefColumn("varchar", {
    name: "ReferenceScheduleLine",
    nullable: true,
    length: 20
  })
  referenceScheduleLine: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("decimal", {
    name: "UNIT_PRICE_SID",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  unitPriceSid: string | null;

  @DefColumn("decimal", {
    name: "AMOUNT_TOTAL_NEW",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  amountTotalNew: string | null;

  @DefColumn("double", {
    name: "PERCENT_OF_VAT_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfVatSid: number | null;

  @DefColumn("double", {
    name: "AMOUNT_VAT_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountVatSid: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_IMPORT_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfImportTax: number | null;

  @DefColumn("double", {
    name: "AMOUNT_OF_IMPORT_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountOfImportTax: number | null;

  @DefColumn("double", {
    name: "PERCENT_OF_VAT_NK",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  percentOfVatNk: number | null;

  @DefColumn("double", {
    name: "AMOUNT_VAT_NK",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountVatNk: number | null;

  @DefColumn("double", {
    name: "AMOUNT_BE_TAX",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  amountBeTax: number | null;

  @DefColumn("double", {
    name: "UNIT_PRICE_REDUCTION_SID",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  unitPriceReductionSid: number | null;

  @DefColumn("varchar", { name: "ID_ENT_GOO", nullable: true, length: 31 })
  idEntGoo: string | null;

  @DefColumn("decimal", {
    name: "QUANTITY_SID",
    nullable: true,
    precision: 22,
    scale: 0,
    default: () => "'0'"
  })
  quantitySid: string | null;

  @DefColumn("varchar", { name: "ID_MEA", nullable: true, length: 15 })
  idMea: string | null;

  @DefColumn("varchar", { name: "NOTE_SID", nullable: true, length: 225 })
  noteSid: string | null;

  @DefColumn("decimal", {
    name: "DISCOUNT",
    nullable: true,
    precision: 18,
    scale: 4,
    default: () => "'0.0000'"
  })
  discount: string | null;

  @DefColumn("decimal", {
    name: "VAT",
    nullable: true,
    precision: 18,
    scale: 4,
    default: () => "'0.0000'"
  })
  vat: string | null;

  @DefColumn("double", {
    name: "YARD",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yard: number | null;

  @DefColumn("double", {
    name: "CUBEEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeexpected: number | null;

  @DefColumn("double", {
    name: "YARDEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yardexpected: number | null;

  @DefColumn("double", {
    name: "GROSSWGTEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgtexpected: number | null;

  @DefColumn("double", {
    name: "NETWGTEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgtexpected: number | null;

  @DefColumn("double", {
    name: "LENGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  length: number | null;

  @DefColumn("double", {
    name: "WIDTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  width: number | null;

  @DefColumn("double", {
    name: "HEIGHT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  height: number | null;

  @DefColumn("varchar", { name: "UPCCODE", nullable: true, length: 50 })
  upccode: string | null;

  @DefColumn("varchar", { name: "GROUPOFCARGO", nullable: true, length: 30 })
  groupofcargo: string | null;

  @DefColumn("varchar", { name: "HTSCODE", nullable: true, length: 30 })
  htscode: string | null;

  @DefColumn("varchar", { name: "HSCODE", nullable: true, length: 30 })
  hscode: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 100 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 100 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 100 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 100 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 100 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 100 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 100 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 100 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    width: 1,
    default: () => "'0'"
  })
  deleted: boolean | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 30 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 30 })
  cartonid: string | null;

  @DefColumn("datetime", { name: "ID_DECLARATION_DATE", nullable: true })
  idDeclarationDate: Date | null;

  @DefColumn("varchar", {
    name: "ID_DECLARATION",
    nullable: true,
    length: 1024
  })
  idDeclaration: string | null;
}
