import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("STORERKEY_UNIQUE", ["whseid", "storerkey", "sku", "componentsku"], {
  unique: true
})
@Entity("billofmaterial", { schema: "swm" })
export class Billofmaterial {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "COMPONENTSKU", length: 50 })
  componentsku: string;

  @DefColumn("int", { name: "SEQUENCE", nullable: true })
  sequence: number | null;

  @DefColumn("tinyint", { name: "BOMONLY", nullable: true })
  bomonly: number | null;

  @DefColumn("decimal", { name: "QTY", precision: 22, scale: 5 })
  qty: string;

  @DefColumn("varchar", { name: "NOTES", nullable: true, length: 255 })
  notes: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;
}
