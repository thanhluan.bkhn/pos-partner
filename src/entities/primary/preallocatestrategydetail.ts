import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("PRE_220_UNIQUE", ["id"], { unique: true })
@Entity("preallocatestrategydetail", { schema: "swm" })
export class Preallocatestrategydetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", {
    primary: true,
    name: "PREALLOCATESTRATEGYKEY",
    length: 10
  })
  preallocatestrategykey: string;

  @DefColumn("varchar", {
    primary: true,
    name: "PREALLOCATESTRATEGYLINENUMBER",
    length: 5
  })
  preallocatestrategylinenumber: string;

  @DefColumn("varchar", { name: "DESCR", length: 60 })
  descr: string;

  @DefColumn("varchar", { name: "UOM", length: 10 })
  uom: string;

  @DefColumn("varchar", { name: "PREALLOCATEPICKCODE", length: 10 })
  preallocatepickcode: string;

  @DefColumn("varchar", { name: "PICKCODESQL", nullable: true, length: 2000 })
  pickcodesql: string | null;

  @DefColumn("int", {
    name: "STEPNUMBER",
    nullable: true,
    default: () => "'0'"
  })
  stepnumber: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;
}
