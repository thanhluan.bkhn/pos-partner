import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("SKU_245_UNIQUE", ["id"], { unique: true })
@Index("SKU_WHSEID_STORERKEY_SKU", ["whseid", "storerkey", "sku"], {
  unique: true
})
@Index("IX_ADDDATE", ["adddate"], {})
@Index("IX_UPCCODE", ["whseid", "storerkey", "sku", "upccode"], {})
@Entity("sku", { schema: "swm" })
export class Sku {
  @DefColumn("varchar", { name: "ID", unique: true, length: 36 })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { name: "LAYERKEY", nullable: true, length: 50 })
  layerkey: string | null;

  @DefColumn("int", {
    name: "SOURCEVERSION",
    nullable: true,
    default: () => "'0'"
  })
  sourceversion: number | null;

  @DefColumn("varchar", { name: "HAZMATCODESKEY", nullable: true, length: 10 })
  hazmatcodeskey: string | null;

  @DefColumn("varchar", { name: "ITEMREFERENCE", nullable: true, length: 8 })
  itemreference: string | null;

  @DefColumn("decimal", {
    name: "SERIALNUMBERSTART",
    nullable: true,
    precision: 15,
    scale: 0,
    default: () => "'0'"
  })
  serialnumberstart: string | null;

  @DefColumn("decimal", {
    name: "SERIALNUMBEREND",
    nullable: true,
    precision: 15,
    scale: 0,
    default: () => "'0'"
  })
  serialnumberend: string | null;

  @DefColumn("decimal", {
    name: "SERIALNUMBERNEXT",
    nullable: true,
    precision: 15,
    scale: 0,
    default: () => "'0'"
  })
  serialnumbernext: string | null;

  @DefColumn("varchar", { name: "DESCR", nullable: true, length: 250 })
  descr: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 128 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 128 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 128 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 128 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 128 })
  susr5: string | null;

  @DefColumn("varchar", { name: "MANUFACTURERSKU", nullable: true, length: 50 })
  manufacturersku: string | null;

  @DefColumn("varchar", { name: "RETAILSKU", nullable: true, length: 50 })
  retailsku: string | null;

  @DefColumn("varchar", { name: "ALTSKU", nullable: true, length: 50 })
  altsku: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("double", {
    name: "STDGROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdgrosswgt: number | null;

  @DefColumn("double", {
    name: "STDNETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdnetwgt: number | null;

  @DefColumn("double", {
    name: "STDCUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdcube: number | null;

  @DefColumn("double", {
    name: "STDLENGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdlength: number | null;

  @DefColumn("double", {
    name: "STDWIDTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdwidth: number | null;

  @DefColumn("double", {
    name: "STDHEIGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdheigth: number | null;

  @DefColumn("double", {
    name: "TARE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  tare: number | null;

  @DefColumn("varchar", { name: "CLASS", nullable: true, length: 10 })
  class: string | null;

  @DefColumn("varchar", { name: "ACTIVE", nullable: true, length: 10 })
  active: string | null;

  @DefColumn("varchar", { name: "SKUGROUP", nullable: true, length: 10 })
  skugroup: string | null;

  @DefColumn("varchar", { name: "TARIFFKEY", nullable: true, length: 10 })
  tariffkey: string | null;

  @DefColumn("varchar", { name: "BUSR1", nullable: true, length: 30 })
  busr1: string | null;

  @DefColumn("varchar", { name: "BUSR2", nullable: true, length: 30 })
  busr2: string | null;

  @DefColumn("varchar", { name: "BUSR3", nullable: true, length: 30 })
  busr3: string | null;

  @DefColumn("varchar", { name: "BUSR4", nullable: true, length: 30 })
  busr4: string | null;

  @DefColumn("varchar", { name: "BUSR5", nullable: true, length: 30 })
  busr5: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01LABEL", nullable: true, length: 20 })
  lottable01Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02LABEL", nullable: true, length: 20 })
  lottable02Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03LABEL", nullable: true, length: 20 })
  lottable03Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE04LABEL", nullable: true, length: 20 })
  lottable04Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE05LABEL", nullable: true, length: 20 })
  lottable05Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE06LABEL", nullable: true, length: 20 })
  lottable06Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07LABEL", nullable: true, length: 20 })
  lottable07Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08LABEL", nullable: true, length: 20 })
  lottable08Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09LABEL", nullable: true, length: 20 })
  lottable09Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10LABEL", nullable: true, length: 20 })
  lottable10Label: string | null;

  @DefColumn("varchar", { name: "PICKCODE", nullable: true, length: 10 })
  pickcode: string | null;

  @DefColumn("varchar", { name: "STRATEGYKEY", nullable: true, length: 10 })
  strategykey: string | null;

  @DefColumn("varchar", { name: "CARTONGROUP", nullable: true, length: 10 })
  cartongroup: string | null;

  @DefColumn("varchar", { name: "PUTCODE", nullable: true, length: 10 })
  putcode: string | null;

  @DefColumn("varchar", { name: "PUTAWAYLOC", nullable: true, length: 10 })
  putawayloc: string | null;

  @DefColumn("varchar", { name: "PUTAWAYZONE", nullable: true, length: 10 })
  putawayzone: string | null;

  @DefColumn("decimal", {
    name: "INNERPACK",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  innerpack: string | null;

  @DefColumn("double", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cube: number | null;

  @DefColumn("double", {
    name: "GROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgt: number | null;

  @DefColumn("double", {
    name: "NETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgt: number | null;

  @DefColumn("double", {
    name: "LENGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  length: number | null;

  @DefColumn("double", {
    name: "WIDTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  width: number | null;

  @DefColumn("double", {
    name: "HEIGTH",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  heigth: number | null;

  @DefColumn("varchar", { name: "ABC", nullable: true, length: 5 })
  abc: string | null;

  @DefColumn("int", {
    name: "CYCLECOUNTFREQUENCY",
    nullable: true,
    default: () => "'0'"
  })
  cyclecountfrequency: number | null;

  @DefColumn("datetime", { name: "LASTCYCLECOUNT", nullable: true })
  lastcyclecount: Date | null;

  @DefColumn("int", {
    name: "REORDERPOINT",
    nullable: true,
    default: () => "'0'"
  })
  reorderpoint: number | null;

  @DefColumn("decimal", {
    name: "REORDERQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  reorderqty: string | null;

  @DefColumn("double", {
    name: "STDORDERCOST",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  stdordercost: number | null;

  @DefColumn("double", {
    name: "CARRYCOST",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  carrycost: number | null;

  @DefColumn("decimal", {
    name: "PRICE",
    nullable: true,
    precision: 19,
    scale: 4,
    default: () => "'0.0000'"
  })
  price: string | null;

  @DefColumn("decimal", {
    name: "COST",
    nullable: true,
    precision: 19,
    scale: 4,
    default: () => "'0.0000'"
  })
  cost: string | null;

  @DefColumn("varchar", {
    name: "ONRECEIPTCOPYPACKKEY",
    nullable: true,
    length: 10
  })
  onreceiptcopypackkey: string | null;

  @DefColumn("varchar", { name: "RECEIPTHOLDCODE", nullable: true, length: 10 })
  receiptholdcode: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTINSPECTIONLOC",
    nullable: true,
    length: 10
  })
  receiptinspectionloc: string | null;

  @DefColumn("varchar", { name: "ROTATEBY", nullable: true, length: 30 })
  rotateby: string | null;

  @DefColumn("varchar", { name: "ROTATEFIELD", nullable: true, length: 45 })
  rotatefield: string | null;

  @DefColumn("int", {
    name: "DATECODEDAYS",
    nullable: true,
    default: () => "'0'"
  })
  datecodedays: number | null;

  @DefColumn("varchar", { name: "DEFAULTROTATION", nullable: true, length: 1 })
  defaultrotation: string | null;

  @DefColumn("varchar", {
    name: "SHIPPABLECONTAINER",
    nullable: true,
    length: 10
  })
  shippablecontainer: string | null;

  @DefColumn("varchar", { name: "IOFLAG", nullable: true, length: 5 })
  ioflag: string | null;

  @DefColumn("decimal", {
    name: "TAREWEIGHT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  tareweight: string | null;

  @DefColumn("varchar", {
    name: "LOTXIDDETAILOTHERLABEL1",
    nullable: true,
    length: 30
  })
  lotxiddetailotherlabel1: string | null;

  @DefColumn("varchar", {
    name: "LOTXIDDETAILOTHERLABEL2",
    nullable: true,
    length: 30
  })
  lotxiddetailotherlabel2: string | null;

  @DefColumn("varchar", {
    name: "LOTXIDDETAILOTHERLABEL3",
    nullable: true,
    length: 30
  })
  lotxiddetailotherlabel3: string | null;

  @DefColumn("decimal", {
    name: "AVGCASEWEIGHT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  avgcaseweight: string | null;

  @DefColumn("decimal", {
    name: "TOLERANCEPCT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  tolerancepct: string | null;

  @DefColumn("varchar", {
    name: "SHELFLIFEINDICATOR",
    nullable: true,
    length: 10
  })
  shelflifeindicator: string | null;

  @DefColumn("int", { name: "SHELFLIFE", nullable: true, default: () => "'0'" })
  shelflife: number | null;

  @DefColumn("varchar", {
    name: "TRANSPORTATIONMODE",
    nullable: true,
    length: 30
  })
  transportationmode: string | null;

  @DefColumn("varchar", { name: "SKUGROUP2", nullable: true, length: 30 })
  skugroup2: string | null;

  @DefColumn("varchar", { name: "SUSR6", nullable: true, length: 30 })
  susr6: string | null;

  @DefColumn("varchar", { name: "SUSR7", nullable: true, length: 30 })
  susr7: string | null;

  @DefColumn("varchar", { name: "SUSR8", nullable: true, length: 30 })
  susr8: string | null;

  @DefColumn("varchar", { name: "SUSR9", nullable: true, length: 30 })
  susr9: string | null;

  @DefColumn("varchar", { name: "SUSR10", nullable: true, length: 30 })
  susr10: string | null;

  @DefColumn("varchar", { name: "BUSR6", nullable: true, length: 30 })
  busr6: string | null;

  @DefColumn("varchar", { name: "BUSR7", nullable: true, length: 30 })
  busr7: string | null;

  @DefColumn("varchar", { name: "BUSR8", nullable: true, length: 30 })
  busr8: string | null;

  @DefColumn("varchar", { name: "BUSR9", nullable: true, length: 30 })
  busr9: string | null;

  @DefColumn("varchar", { name: "BUSR10", nullable: true, length: 30 })
  busr10: string | null;

  @DefColumn("decimal", {
    name: "MINIMUMSHELFLIFEONRFPICKING",
    nullable: true,
    precision: 10,
    scale: 0,
    default: () => "'0'"
  })
  minimumshelflifeonrfpicking: string | null;

  @DefColumn("varchar", { name: "FREIGHTCLASS", nullable: true, length: 10 })
  freightclass: string | null;

  @DefColumn("varchar", { name: "ICWFLAG", nullable: true, length: 1 })
  icwflag: string | null;

  @DefColumn("varchar", { name: "ICWBY", nullable: true, length: 1 })
  icwby: string | null;

  @DefColumn("varchar", { name: "IDEWEIGHT", nullable: true, length: 1 })
  ideweight: string | null;

  @DefColumn("varchar", { name: "ICDFLAG", nullable: true, length: 1 })
  icdflag: string | null;

  @DefColumn("varchar", { name: "ICDLABEL1", nullable: true, length: 5 })
  icdlabel1: string | null;

  @DefColumn("varchar", { name: "ICDLABEL2", nullable: true, length: 5 })
  icdlabel2: string | null;

  @DefColumn("varchar", { name: "ICDLABEL3", nullable: true, length: 5 })
  icdlabel3: string | null;

  @DefColumn("varchar", { name: "OCWFLAG", nullable: true, length: 1 })
  ocwflag: string | null;

  @DefColumn("varchar", { name: "OCWBY", nullable: true, length: 1 })
  ocwby: string | null;

  @DefColumn("varchar", { name: "ODEWEIGHT", nullable: true, length: 1 })
  odeweight: string | null;

  @DefColumn("varchar", { name: "OACOVERRIDE", nullable: true, length: 1 })
  oacoverride: string | null;

  @DefColumn("varchar", { name: "OCDFLAG", nullable: true, length: 1 })
  ocdflag: string | null;

  @DefColumn("varchar", { name: "OCDLABEL1", nullable: true, length: 5 })
  ocdlabel1: string | null;

  @DefColumn("varchar", { name: "OCDLABEL2", nullable: true, length: 5 })
  ocdlabel2: string | null;

  @DefColumn("varchar", { name: "OCDLABEL3", nullable: true, length: 5 })
  ocdlabel3: string | null;

  @DefColumn("decimal", {
    name: "OTAREWEIGHT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  otareweight: string | null;

  @DefColumn("decimal", {
    name: "OAVGCASEWEIGHT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  oavgcaseweight: string | null;

  @DefColumn("decimal", {
    name: "OTOLERANCEPCT",
    nullable: true,
    precision: 12,
    scale: 6,
    default: () => "'0.000000'"
  })
  otolerancepct: string | null;

  @DefColumn("varchar", { name: "RFDEFAULTPACK", nullable: true, length: 50 })
  rfdefaultpack: string | null;

  @DefColumn("varchar", { name: "RFDEFAULTUOM", nullable: true, length: 10 })
  rfdefaultuom: string | null;

  @DefColumn("varchar", {
    name: "SHELFLIFECODETYPE",
    nullable: true,
    length: 1
  })
  shelflifecodetype: string | null;

  @DefColumn("int", {
    name: "SHELFLIFEONRECEIVING",
    nullable: true,
    default: () => "'0'"
  })
  shelflifeonreceiving: number | null;

  @DefColumn("varchar", {
    name: "ALLOWCONSOLIDATION",
    nullable: true,
    length: 1
  })
  allowconsolidation: string | null;

  @DefColumn("decimal", {
    name: "MINIMUMWAVEQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  minimumwaveqty: string | null;

  @DefColumn("varchar", { name: "BULKCARTONGROUP", nullable: true, length: 10 })
  bulkcartongroup: string | null;

  @DefColumn("varchar", { name: "PICKUOM", nullable: true, length: 2 })
  pickuom: string | null;

  @DefColumn("varchar", { name: "EACHKEY", nullable: true, length: 10 })
  eachkey: string | null;

  @DefColumn("varchar", { name: "CASEKEY", nullable: true, length: 10 })
  casekey: string | null;

  @DefColumn("varchar", { name: "TYPE", nullable: true, length: 32 })
  type: string | null;

  @DefColumn("datetime", { name: "EFFECSTARTDATE", nullable: true })
  effecstartdate: Date | null;

  @DefColumn("datetime", { name: "EFFECENDDATE", nullable: true })
  effecenddate: Date | null;

  @DefColumn("varchar", { name: "CONVEYABLE", nullable: true, length: 1 })
  conveyable: string | null;

  @DefColumn("varchar", { name: "FLOWTHRUITEM", nullable: true, length: 1 })
  flowthruitem: string | null;

  @DefColumn("varchar", { name: "NOTES1", nullable: true, length: 3072 })
  notes1: string | null;

  @DefColumn("varchar", { name: "NOTES2", nullable: true, length: 3072 })
  notes2: string | null;

  @DefColumn("varchar", { name: "VERT_STORAGE", nullable: true, length: 1 })
  vertStorage: string | null;

  @DefColumn("varchar", { name: "CWFLAG", nullable: true, length: 10 })
  cwflag: string | null;

  @DefColumn("varchar", { name: "VERIFYLOT04LOT05", nullable: true, length: 1 })
  verifylot04Lot05: string | null;

  @DefColumn("varchar", {
    name: "PUTAWAYSTRATEGYKEY",
    nullable: true,
    length: 10
  })
  putawaystrategykey: string | null;

  @DefColumn("varchar", { name: "RETURNSLOC", nullable: true, length: 10 })
  returnsloc: string | null;

  @DefColumn("varchar", { name: "QCLOC", nullable: true, length: 10 })
  qcloc: string | null;

  @DefColumn("varchar", {
    name: "RECEIPTVALIDATIONTEMPLATE",
    nullable: true,
    length: 18
  })
  receiptvalidationtemplate: string | null;

  @DefColumn("varchar", { name: "SKUTYPE", nullable: true, length: 1 })
  skutype: string | null;

  @DefColumn("int", {
    name: "StackLimit",
    nullable: true,
    default: () => "'0'"
  })
  stackLimit: number | null;

  @DefColumn("int", {
    name: "MaxPalletsPerZone",
    nullable: true,
    default: () => "'0'"
  })
  maxPalletsPerZone: number | null;

  @DefColumn("varchar", {
    name: "CCDISCREPANCYRULE",
    nullable: true,
    length: 10
  })
  ccdiscrepancyrule: string | null;

  @DefColumn("varchar", {
    name: "MANUALSETUPREQUIRED",
    nullable: true,
    length: 1
  })
  manualsetuprequired: string | null;

  @DefColumn("varchar", { name: "QCLOCOUT", nullable: true, length: 10 })
  qclocout: string | null;

  @DefColumn("varchar", { name: "OCDCATCHWHEN", nullable: true, length: 10 })
  ocdcatchwhen: string | null;

  @DefColumn("varchar", { name: "OCDCATCHQTY1", nullable: true, length: 10 })
  ocdcatchqty1: string | null;

  @DefColumn("varchar", { name: "OCDCATCHQTY2", nullable: true, length: 10 })
  ocdcatchqty2: string | null;

  @DefColumn("varchar", { name: "OCDCATCHQTY3", nullable: true, length: 10 })
  ocdcatchqty3: string | null;

  @DefColumn("varchar", { name: "HASIMAGE", nullable: true, length: 1 })
  hasimage: string | null;

  @DefColumn("varchar", { name: "GUID", nullable: true, length: 32 })
  guid: string | null;

  @DefColumn("varchar", {
    name: "NONSTOCKEDINDICATOR",
    nullable: true,
    length: 1
  })
  nonstockedindicator: string | null;

  @DefColumn("varchar", { name: "LOTTABLE11LABEL", nullable: true, length: 20 })
  lottable11Label: string | null;

  @DefColumn("varchar", { name: "LOTTABLE12LABEL", nullable: true, length: 20 })
  lottable12Label: string | null;

  @DefColumn("int", {
    name: "TOEXPIREDAYS",
    nullable: true,
    default: () => "'0'"
  })
  toexpiredays: number | null;

  @DefColumn("int", {
    name: "TODELIVERBYDAYS",
    nullable: true,
    default: () => "'0'"
  })
  todeliverbydays: number | null;

  @DefColumn("int", {
    name: "TOBESTBYDAYS",
    nullable: true,
    default: () => "'0'"
  })
  tobestbydays: number | null;

  @DefColumn("int", {
    name: "SNUM_POSITION",
    nullable: true,
    default: () => "'0'"
  })
  snumPosition: number | null;

  @DefColumn("int", {
    name: "SNUM_LENGTH",
    nullable: true,
    default: () => "'0'"
  })
  snumLength: number | null;

  @DefColumn("varchar", { name: "SNUM_DELIMITER", nullable: true, length: 1 })
  snumDelimiter: string | null;

  @DefColumn("int", {
    name: "SNUM_DELIM_COUNT",
    nullable: true,
    default: () => "'0'"
  })
  snumDelimCount: number | null;

  @DefColumn("int", {
    name: "SNUM_QUANTITY",
    nullable: true,
    default: () => "'0'"
  })
  snumQuantity: number | null;

  @DefColumn("int", {
    name: "SNUM_INCR_POS",
    nullable: true,
    default: () => "'0'"
  })
  snumIncrPos: number | null;

  @DefColumn("int", {
    name: "SNUM_INCR_LENGTH",
    nullable: true,
    default: () => "'0'"
  })
  snumIncrLength: number | null;

  @DefColumn("int", {
    name: "SNUM_ENDTOEND",
    nullable: true,
    default: () => "'0'"
  })
  snumEndtoend: number | null;

  @DefColumn("varchar", { name: "SNUM_MASK", nullable: true, length: 50 })
  snumMask: string | null;

  @DefColumn("varchar", { name: "ICDLABEL4", nullable: true, length: 5 })
  icdlabel4: string | null;

  @DefColumn("varchar", { name: "ICDLABEL5", nullable: true, length: 5 })
  icdlabel5: string | null;

  @DefColumn("varchar", { name: "OCDLABEL4", nullable: true, length: 5 })
  ocdlabel4: string | null;

  @DefColumn("varchar", { name: "OCDLABEL5", nullable: true, length: 5 })
  ocdlabel5: string | null;

  @DefColumn("varchar", { name: "ICD1UNIQUE", nullable: true, length: 1 })
  icd1Unique: string | null;

  @DefColumn("varchar", { name: "OCD1UNIQUE", nullable: true, length: 1 })
  ocd1Unique: string | null;

  @DefColumn("varchar", {
    name: "SNUMLONG_DELIMITER",
    nullable: true,
    length: 5
  })
  snumlongDelimiter: string | null;

  @DefColumn("int", {
    name: "SNUMLONG_FIXED",
    nullable: true,
    default: () => "'0'"
  })
  snumlongFixed: number | null;

  @DefColumn("int", {
    name: "SNUM_AUTOINCREMENT",
    nullable: true,
    default: () => "'0'"
  })
  snumAutoincrement: number | null;

  @DefColumn("varchar", {
    name: "ACCOUNTINGENTITY",
    nullable: true,
    length: 64
  })
  accountingentity: string | null;

  @DefColumn("varchar", { name: "VOICEGROUPINGID", nullable: true, length: 1 })
  voicegroupingid: string | null;

  @DefColumn("int", {
    name: "COUNTSEQUENCE",
    nullable: true,
    default: () => "'0'"
  })
  countsequence: number | null;

  @DefColumn("varchar", { name: "FILLQTYUOM", nullable: true, length: 2 })
  fillqtyuom: string | null;

  @DefColumn("varchar", {
    name: "LOCATIONPICKCASE",
    nullable: true,
    length: 100
  })
  locationpickcase: string | null;

  @DefColumn("varchar", { name: "ZONEPICKCASE", nullable: true, length: 100 })
  zonepickcase: string | null;

  @DefColumn("varchar", {
    name: "STORAGETYPEPICKCASE",
    nullable: true,
    length: 100
  })
  storagetypepickcase: string | null;

  @DefColumn("tinyint", {
    name: "PRIORITYUOM",
    nullable: true,
    default: () => "'0'"
  })
  priorityuom: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 18 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 18 })
  editwho: string | null;

  @DefColumn("varchar", { name: "ORIGIN", nullable: true, length: 50 })
  origin: string | null;

  @DefColumn("varchar", { name: "COLOR", nullable: true, length: 20 })
  color: string | null;

  @DefColumn("varchar", { name: "CATEGORY", nullable: true, length: 40 })
  category: string | null;

  @DefColumn("varchar", { name: "UPCCODE", nullable: true, length: 20 })
  upccode: string | null;

  @DefColumn("varchar", { name: "UPC", nullable: true, length: 50 })
  upc: string | null;

  @DefColumn("varchar", { name: "SIZE", nullable: true, length: 30 })
  size: string | null;

  @DefColumn("varchar", { name: "UNITOFISSUE", nullable: true, length: 50 })
  unitofissue: string | null;

  @DefColumn("varchar", { name: "BATCH_INDICATOR", nullable: true, length: 50 })
  batchIndicator: string | null;

  @DefColumn("int", { name: "NUMERATOR", nullable: true, default: () => "'0'" })
  numerator: number | null;

  @DefColumn("varchar", { name: "UNITOFMEASURE", nullable: true, length: 50 })
  unitofmeasure: string | null;

  @DefColumn("decimal", {
    name: "DENOMINATOR",
    nullable: true,
    precision: 10,
    scale: 3,
    default: () => "'0.000'"
  })
  denominator: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("int", { name: "PROVISIONDAYS", nullable: true })
  provisiondays: number | null;

  @DefColumn("varchar", { name: "PROVISIONFIELD", nullable: true, length: 45 })
  provisionfield: string | null;

  @DefColumn("tinyint", { name: "PROVISIONACTIVE", nullable: true })
  provisionactive: number | null;

  @DefColumn("int", { name: "EXPIRED", nullable: true, default: () => "'0'" })
  expired: number | null;

  @DefColumn("int", {
    name: "NEARLYEXPIRED",
    nullable: true,
    default: () => "'0'"
  })
  nearlyexpired: number | null;

  @DefColumn("int", { name: "USABLE", nullable: true, default: () => "'0'" })
  usable: number | null;

  @DefColumn("int", { name: "MIN", nullable: true })
  min: number | null;

  @DefColumn("int", { name: "MAX", nullable: true })
  max: number | null;

  @DefColumn("tinyint", { name: "STOCKDATE", nullable: true })
  stockdate: number | null;

  @DefColumn("tinyint", {
    name: "ISACTIVE",
    nullable: true,
    default: () => "'0'"
  })
  isactive: number | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENTSTRATEGYKEY",
    nullable: true,
    length: 50
  })
  replenishmentstrategykey: string | null;

  @DefColumn("varchar", { name: "validationcode", nullable: true, length: 45 })
  validationcode: string | null;

  @DefColumn("datetime", { name: "SYNCDATE", nullable: true })
  syncdate: Date | null;

  @DefColumn("varchar", { name: "SYNCSTATUS", nullable: true, length: 45 })
  syncstatus: string | null;

  @DefColumn("varchar", { name: "SYNCERROR", nullable: true, length: 3072 })
  syncerror: string | null;

  @DefColumn("int", {
    name: "ALLOCATEBYSHELFLIFE",
    nullable: true,
    default: () => "'0'"
  })
  allocatebyshelflife: number | null;
}
