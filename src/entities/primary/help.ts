import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Entity("help", { schema: "swm" })
export class Help {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "TITLE", length: 45 })
  title: string;

  @DefColumn("varchar", { name: "NAME", length: 45 })
  name: string;

  @DefColumn("varchar", { name: "DESCRIPTION", length: 255 })
  description: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 45 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    width: 1,
    default: () => "'0'"
  })
  deleted: boolean | null;
}
