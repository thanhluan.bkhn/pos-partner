import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("STOCKCOUNTCODE_UNIQUE", ["whseid", "stockcountcode"], { unique: true })
@Entity("stockcount", { schema: "swm" })
export class Stockcount {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { name: "WHSEID", length: 50 })
  whseid: string;

  @DefColumn("varchar", { name: "STOCKCOUNTCODE", length: 11 })
  stockcountcode: string;

  @DefColumn("varchar", { name: "STOCKCOUNTNAME", length: 50 })
  stockcountname: string;

  @DefColumn("int", { name: "STATUS" })
  status: number;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 200 })
  remark: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;
}
