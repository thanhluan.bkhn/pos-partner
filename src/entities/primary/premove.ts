import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("premove", { schema: "swm" })
export class Premove {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID" })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 65 })
  whseid: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "MOVECODE", nullable: true, length: 50 })
  movecode: string | null;

  @DefColumn("varchar", { name: "LPNID", nullable: true, length: 18 })
  lpnid: string | null;

  @DefColumn("varchar", { name: "TOLPNID", nullable: true, length: 18 })
  tolpnid: string | null;

  @DefColumn("varchar", { name: "SKU", nullable: true, length: 50 })
  sku: string | null;

  @DefColumn("varchar", { name: "FROMLOC", nullable: true, length: 10 })
  fromloc: string | null;

  @DefColumn("varchar", { name: "TOLOC", nullable: true, length: 10 })
  toloc: string | null;

  @DefColumn("int", { name: "QTY", nullable: true, default: () => "'0'" })
  qty: number | null;

  @DefColumn("int", { name: "DONE", nullable: true, default: () => "'0'" })
  done: number | null;

  @DefColumn("int", { name: "CONFIRMED", nullable: true, default: () => "'0'" })
  confirmed: number | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("varchar", { name: "REMARK", nullable: true, length: 250 })
  remark: string | null;

  @DefColumn("varchar", { name: "LOT", nullable: true, length: 50 })
  lot: string | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;

  @DefColumn("varchar", { name: "UNITID", nullable: true, length: 18 })
  unitid: string | null;

  @DefColumn("varchar", { name: "CARTONID", nullable: true, length: 18 })
  cartonid: string | null;

  @DefColumn("varchar", { name: "PALLETID", nullable: true, length: 18 })
  palletid: string | null;

  @DefColumn("varchar", { name: "TOUNITID", nullable: true, length: 18 })
  tounitid: string | null;

  @DefColumn("varchar", { name: "TOCARTONID", nullable: true, length: 18 })
  tocartonid: string | null;

  @DefColumn("varchar", { name: "TOPALLETID", nullable: true, length: 18 })
  topalletid: string | null;
}
