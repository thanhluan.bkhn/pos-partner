import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ORD_175_UNIQUE", ["id"], { unique: true })
@Entity("orderstatussetup", { schema: "swm" })
export class Orderstatussetup {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "CODE", length: 3 })
  code: string;

  @DefColumn("int", { name: "SHOWSEQ" })
  showseq: number;

  @DefColumn("varchar", { name: "DESCRIPTION", length: 30 })
  description: string;

  @DefColumn("char", { name: "EDITABLE", length: 1 })
  editable: string;

  @DefColumn("char", { name: "ENABLED", length: 1 })
  enabled: string;

  @DefColumn("char", { name: "ORDERFLAG", length: 1 })
  orderflag: string;

  @DefColumn("char", { name: "XORDERFLAG", length: 1 })
  xorderflag: string;

  @DefColumn("char", { name: "TRANSSHIPFLAG", length: 1 })
  transshipflag: string;

  @DefColumn("char", { name: "HEADERFLAG", length: 1 })
  headerflag: string;

  @DefColumn("char", { name: "DETAILFLAG", length: 1 })
  detailflag: string;

  @DefColumn("char", { name: "HEADERHISTORYFLAG", length: 1 })
  headerhistoryflag: string;

  @DefColumn("char", { name: "DETAILHISTORYFLAG", length: 1 })
  detailhistoryflag: string;

  @DefColumn("varchar", { name: "RULES", length: 255 })
  rules: string;

  @DefColumn("varchar", { name: "COMMENTS", length: 255 })
  comments: string;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;
}
