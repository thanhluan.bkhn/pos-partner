import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("dropid", { schema: "swm" })
export class Dropid {
  @DefPrimaryGeneratedColumn({
    type: "bigint",
    name: "ID",
    unique: true,
    unsigned: true
  })
  id: string;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "RECEIPTKEY", length: 30 })
  receiptkey: string;

  @DefColumn("varchar", { primary: true, name: "ORDERKEY", length: 30 })
  orderkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 65 })
  sku: string;

  @DefColumn("varchar", { primary: true, name: "LPNID", length: 18 })
  lpnid: string;

  @DefColumn("varchar", { primary: true, name: "UNITID", length: 30 })
  unitid: string;

  @DefColumn("varchar", { primary: true, name: "CARTONID", length: 18 })
  cartonid: string;

  @DefColumn("varchar", { primary: true, name: "PALLETID", length: 18 })
  palletid: string;

  @DefColumn("decimal", { name: "QTY", precision: 10, scale: 0 })
  qty: string;

  @DefColumn("varchar", { primary: true, name: "DROPTYPE", length: 45 })
  droptype: string;

  @DefColumn("varchar", { primary: true, name: "COMMANDTYPE", length: 45 })
  commandtype: string;

  @DefColumn("int", { primary: true, name: "STATUS" })
  status: number;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 45 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 45 })
  editwho: string | null;

  @DefColumn("tinyint", { name: "DELETED", nullable: true })
  deleted: number | null;
}
