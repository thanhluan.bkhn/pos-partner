import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("LOT_137_UNIQUE", ["id"], { unique: true })
@Entity("lot", { schema: "swm" })
export class Lot {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "LOT", length: 15 })
  lot: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("decimal", {
    name: "CASECNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  casecnt: string | null;

  @DefColumn("decimal", {
    name: "INNERPACK",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  innerpack: string | null;

  @DefColumn("decimal", {
    name: "QTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qty: string | null;

  @DefColumn("decimal", {
    name: "PALLET",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  pallet: string | null;

  @DefColumn("double", {
    name: "CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cube: number | null;

  @DefColumn("double", {
    name: "GROSSWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgt: number | null;

  @DefColumn("double", {
    name: "NETWGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgt: number | null;

  @DefColumn("double", {
    name: "OTHERUNIT1",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit1: number | null;

  @DefColumn("double", {
    name: "OTHERUNIT2",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  otherunit2: number | null;

  @DefColumn("decimal", {
    name: "QTYPREALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypreallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypicked: string | null;

  @DefColumn("decimal", {
    name: "QTYONHOLD",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyonhold: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 10 })
  status: string | null;

  @DefColumn("decimal", {
    name: "ARCHIVEQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  archiveqty: string | null;

  @DefColumn("datetime", { name: "ARCHIVEDATE", nullable: true })
  archivedate: Date | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;

  @DefColumn("double", {
    name: "GROSSWGTPREALLOCATED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgtpreallocated: number | null;

  @DefColumn("double", {
    name: "NETWGTPREALLOCATED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgtpreallocated: number | null;

  @DefColumn("double", {
    name: "GROSSWGTALLOCATED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgtallocated: number | null;

  @DefColumn("double", {
    name: "NETWGTALLOCATED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgtallocated: number | null;

  @DefColumn("double", {
    name: "GROSSWGTPICKED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgtpicked: number | null;

  @DefColumn("double", {
    name: "NETWGTPICKED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgtpicked: number | null;

  @DefColumn("double", {
    name: "YARD",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yard: number | null;

  @DefColumn("double", {
    name: "YARDALLOCATED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yardallocated: number | null;

  @DefColumn("double", {
    name: "YARDPICKED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yardpicked: number | null;

  @DefColumn("double", {
    name: "CUBEEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  cubeexpected: number | null;

  @DefColumn("double", {
    name: "YARDEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  yardexpected: number | null;

  @DefColumn("double", {
    name: "GROSSWGTEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  grosswgtexpected: number | null;

  @DefColumn("double", {
    name: "NETWGTEXPECTED",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  netwgtexpected: number | null;

  @DefColumn("tinyint", {
    name: "DELETED",
    nullable: true,
    default: () => "'0'"
  })
  deleted: number | null;
}
