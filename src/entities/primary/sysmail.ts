import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID", ["id"], { unique: true })
@Entity("sysmail", { schema: "swm" })
export class Sysmail {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("varchar", { name: "description", nullable: true, length: 255 })
  description: string | null;

  @DefColumn("varchar", { name: "userName", nullable: true, length: 255 })
  userName: string | null;

  @DefColumn("varchar", { name: "password", nullable: true, length: 255 })
  password: string | null;

  @DefColumn("varchar", { name: "from", nullable: true, length: 100 })
  from: string | null;

  @DefColumn("varchar", { name: "to", nullable: true, length: 100 })
  to: string | null;

  @DefColumn("text", { name: "title", nullable: true })
  title: string | null;

  @DefColumn("text", { name: "message", nullable: true })
  message: string | null;

  @DefColumn("varchar", { name: "type", nullable: true, length: 10 })
  type: string | null;

  @DefColumn("varchar", { name: "host", nullable: true, length: 100 })
  host: string | null;

  @DefColumn("int", { name: "port", nullable: true })
  port: number | null;

  @DefColumn("tinyint", {
    name: "isDefault",
    nullable: true,
    default: () => "'0'"
  })
  isDefault: number | null;

  @DefColumn("tinyint", {
    name: "secure",
    nullable: true,
    default: () => "'0'"
  })
  secure: number | null;

  @DefColumn("datetime", { name: "createdDate", nullable: true })
  createdDate: Date | null;

  @DefColumn("varchar", { name: "createdBy", nullable: true, length: 65 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate", nullable: true })
  modifiedDate: Date | null;

  @DefColumn("varchar", { name: "modifiedBy", nullable: true, length: 65 })
  modifiedBy: string | null;
}
