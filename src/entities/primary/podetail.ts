import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("POD_208_UNIQUE", ["id"], { unique: true })
@Entity("podetail", { schema: "swm" })
export class Podetail {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "POKEY", length: 18 })
  pokey: string;

  @DefColumn("varchar", { primary: true, name: "POLINENUMBER", length: 5 })
  polinenumber: string;

  @DefColumn("varchar", { name: "STORERKEY", length: 40 })
  storerkey: string;

  @DefColumn("varchar", { name: "PODETAILKEY", nullable: true, length: 50 })
  podetailkey: string | null;

  @DefColumn("varchar", { name: "EXTERNPOKEY", nullable: true, length: 20 })
  externpokey: string | null;

  @DefColumn("varchar", { name: "EXTERNLINENO", nullable: true, length: 20 })
  externlineno: string | null;

  @DefColumn("varchar", { name: "MARKSCONTAINER", nullable: true, length: 18 })
  markscontainer: string | null;

  @DefColumn("varchar", { name: "SKU", nullable: true, length: 50 })
  sku: string | null;

  @DefColumn("varchar", { name: "SKUDESCRIPTION", nullable: true, length: 60 })
  skudescription: string | null;

  @DefColumn("varchar", { name: "MANUFACTURERSKU", nullable: true, length: 50 })
  manufacturersku: string | null;

  @DefColumn("varchar", { name: "RETAILSKU", nullable: true, length: 50 })
  retailsku: string | null;

  @DefColumn("varchar", { name: "ALTSKU", nullable: true, length: 50 })
  altsku: string | null;

  @DefColumn("decimal", {
    name: "ORIGINALQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  originalqty: string | null;

  @DefColumn("decimal", {
    name: "QTYORDERED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyordered: string | null;

  @DefColumn("decimal", {
    name: "QTYADJUSTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyadjusted: string | null;

  @DefColumn("decimal", {
    name: "QTYRECEIVED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyreceived: string | null;

  @DefColumn("varchar", { name: "PACKKEY", nullable: true, length: 50 })
  packkey: string | null;

  @DefColumn("double", {
    name: "UNITPRICE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  unitprice: number | null;

  @DefColumn("varchar", { name: "UOM", nullable: true, length: 10 })
  uom: string | null;

  @DefColumn("datetime", { name: "EFFECTIVEDATE", nullable: true })
  effectivedate: Date | null;

  @DefColumn("varchar", { name: "FORTE_FLAG", nullable: true, length: 6 })
  forteFlag: string | null;

  @DefColumn("int", {
    name: "ITEM_NUMBER",
    nullable: true,
    default: () => "'0'"
  })
  itemNumber: number | null;

  @DefColumn("double", {
    name: "SKU_WGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  skuWgt: number | null;

  @DefColumn("double", {
    name: "SKU_CUBE",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  skuCube: number | null;

  @DefColumn("double", {
    name: "SKU_HGT",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  skuHgt: number | null;

  @DefColumn("double", {
    name: "UNIT_COST",
    nullable: true,
    precision: 22,
    default: () => "'0'"
  })
  unitCost: number | null;

  @DefColumn("decimal", {
    name: "UNIT_SHIP",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  unitShip: string | null;

  @DefColumn("varchar", { name: "SUSR1", nullable: true, length: 30 })
  susr1: string | null;

  @DefColumn("varchar", { name: "SUSR2", nullable: true, length: 30 })
  susr2: string | null;

  @DefColumn("varchar", { name: "SUSR3", nullable: true, length: 30 })
  susr3: string | null;

  @DefColumn("varchar", { name: "SUSR4", nullable: true, length: 30 })
  susr4: string | null;

  @DefColumn("varchar", { name: "SUSR5", nullable: true, length: 30 })
  susr5: string | null;

  @DefColumn("decimal", {
    name: "QTYREJECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyrejected: string | null;

  @DefColumn("varchar", { name: "STATUS", nullable: true, length: 2 })
  status: string | null;

  @DefColumn("varchar", { name: "QCAUTOADJUST", nullable: true, length: 1 })
  qcautoadjust: string | null;

  @DefColumn("varchar", { name: "QCREQUIRED", nullable: true, length: 1 })
  qcrequired: string | null;

  @DefColumn("varchar", { name: "PODETAILID", nullable: true, length: 32 })
  podetailid: string | null;

  @DefColumn("varchar", { name: "SOURCELOCATION", nullable: true, length: 64 })
  sourcelocation: string | null;

  @DefColumn("varchar", { name: "SOURCEVERSION", nullable: true, length: 20 })
  sourceversion: string | null;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 50 })
  addwho: string | null;

  @DefColumn("datetime", { name: "EDITDATE", nullable: true })
  editdate: Date | null;

  @DefColumn("varchar", { name: "EDITWHO", nullable: true, length: 50 })
  editwho: string | null;

  @DefColumn("longtext", { name: "NOTES", nullable: true })
  notes: string | null;

  @DefColumn("varchar", { name: "LOTTABLE01", nullable: true, length: 50 })
  lottable01: string | null;

  @DefColumn("varchar", { name: "LOTTABLE02", nullable: true, length: 50 })
  lottable02: string | null;

  @DefColumn("varchar", { name: "LOTTABLE03", nullable: true, length: 50 })
  lottable03: string | null;

  @DefColumn("datetime", { name: "LOTTABLE04", nullable: true })
  lottable04: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE05", nullable: true })
  lottable05: Date | null;

  @DefColumn("varchar", { name: "LOTTABLE06", nullable: true, length: 50 })
  lottable06: string | null;

  @DefColumn("varchar", { name: "LOTTABLE07", nullable: true, length: 50 })
  lottable07: string | null;

  @DefColumn("varchar", { name: "LOTTABLE08", nullable: true, length: 50 })
  lottable08: string | null;

  @DefColumn("varchar", { name: "LOTTABLE09", nullable: true, length: 50 })
  lottable09: string | null;

  @DefColumn("varchar", { name: "LOTTABLE10", nullable: true, length: 50 })
  lottable10: string | null;

  @DefColumn("datetime", { name: "LOTTABLE11", nullable: true })
  lottable11: Date | null;

  @DefColumn("datetime", { name: "LOTTABLE12", nullable: true })
  lottable12: Date | null;

  @DefColumn("varchar", { name: "conditioncode", nullable: true, length: 45 })
  conditioncode: string | null;
}
