import { DefColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";
import { Mappingdetail } from "./mappingdetail";
import { Settingdetail } from "./settingdetail";

@Index("IX_Code", ["clientId", "mappingDetailId", "code"], { unique: true })
@Index("IX_Client", ["clientId"], {})
@Index("IX_CreatedBy", ["createdBy"], {})
@Index("IX_CreatedDate", ["createdDate"], {})
@Index("IX_Creator", ["creator"], {})
@Index("IX_ModifiedBy", ["modifiedBy"], {})
@Index("IX_ModifiedDate", ["modifiedDate"], {})
@Index("IX_Modifier", ["modifier"], {})
@Index("FK_Setting_MappingDetail_mappingDetailId", ["mappingDetailId"], {})
@Entity("setting", { schema: "swm" })
export class Setting {
  @DefColumn("char", { primary: true, name: "id", length: 36 })
  id: string;

  @DefColumn("char", { name: "clientId", length: 36 })
  clientId: string;

  @DefColumn("char", { name: "mappingDetailId", nullable: true, length: 36 })
  mappingDetailId: string | null;

  @DefColumn("varchar", {
    name: "mappingDetailCode",
    nullable: true,
    length: 50
  })
  mappingDetailCode: string | null;

  @DefColumn("char", { name: "mappingId", nullable: true, length: 36 })
  mappingId: string | null;

  @DefColumn("varchar", { name: "mappingCode", nullable: true, length: 20 })
  mappingCode: string | null;

  @DefColumn("varchar", { name: "code", length: 20 })
  code: string;

  @DefColumn("varchar", { name: "name", nullable: true, length: 200 })
  name: string | null;

  @DefColumn("varchar", { name: "note", nullable: true, length: 1000 })
  note: string | null;

  @DefColumn("tinyint", { name: "isActivated", width: 1 })
  isActivated: boolean;

  @DefColumn("varchar", { name: "clientCode", nullable: true, length: 20 })
  clientCode: string | null;

  @DefColumn("char", { name: "createdBy", nullable: true, length: 36 })
  createdBy: string | null;

  @DefColumn("datetime", { name: "createdDate" })
  createdDate: Date;

  @DefColumn("varchar", { name: "creator", nullable: true, length: 50 })
  creator: string | null;

  @DefColumn("char", { name: "modifiedBy", nullable: true, length: 36 })
  modifiedBy: string | null;

  @DefColumn("datetime", { name: "modifiedDate" })
  modifiedDate: Date;

  @DefColumn("varchar", { name: "modifier", nullable: true, length: 50 })
  modifier: string | null;

  @ManyToOne(
    () => Mappingdetail,
    mappingdetail => mappingdetail.settings,
    { onDelete: "RESTRICT", onUpdate: "RESTRICT" }
  )
  @JoinColumn([{ name: "mappingDetailId", referencedColumnName: "id" }])
  mappingDetail: Mappingdetail;

  @OneToMany(
    () => Settingdetail,
    settingdetail => settingdetail.setting
  )
  settingdetails: Settingdetail[];
}
