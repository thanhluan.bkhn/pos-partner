import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("ID_UNIQUE", ["id"], { unique: true })
@Entity("unitid", { schema: "swm" })
export class Unitid {
  @DefPrimaryGeneratedColumn({ type: "bigint", name: "ID" })
  id: string;

  @DefColumn("varchar", { name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { name: "CODE", length: 30 })
  code: string;

  @DefColumn("varchar", { name: "STORERKEY", nullable: true, length: 30 })
  storerkey: string | null;

  @DefColumn("varchar", { name: "SKU", nullable: true, length: 65 })
  sku: string | null;

  @DefColumn("varchar", { name: "UNITID", length: 30 })
  unitid: string;

  @DefColumn("datetime", { name: "ADDDATE", nullable: true })
  adddate: Date | null;

  @DefColumn("varchar", { name: "ADDWHO", nullable: true, length: 30 })
  addwho: string | null;
}
