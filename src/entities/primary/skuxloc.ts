import { DefColumn, DefPrimaryGeneratedColumn } from "~/@core/decorator";
import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from "typeorm";

@Index("SKU_246_UNIQUE", ["id"], { unique: true })
@Entity("skuxloc", { schema: "swm" })
export class Skuxloc {
  @DefPrimaryGeneratedColumn({ type: "int", name: "ID", unique: true })
  id: number;

  @DefColumn("varchar", { primary: true, name: "WHSEID", length: 30 })
  whseid: string;

  @DefColumn("varchar", { primary: true, name: "STORERKEY", length: 15 })
  storerkey: string;

  @DefColumn("varchar", { primary: true, name: "SKU", length: 50 })
  sku: string;

  @DefColumn("varchar", { primary: true, name: "LOC", length: 30 })
  loc: string;

  @DefColumn("decimal", { name: "QTY", precision: 22, scale: 5 })
  qty: string;

  @DefColumn("decimal", {
    name: "QTYALLOCATED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyallocated: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypicked: string | null;

  @DefColumn("decimal", {
    name: "QTYEXPECTED",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyexpected: string | null;

  @DefColumn("decimal", {
    name: "QTYLOCATIONLIMIT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtylocationlimit: string | null;

  @DefColumn("decimal", {
    name: "QTYLOCATIONMINIMUM",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtylocationminimum: string | null;

  @DefColumn("decimal", {
    name: "QTYPICKINPROCESS",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtypickinprocess: string | null;

  @DefColumn("decimal", {
    name: "QTYREPLENISHMENTOVERRIDE",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  qtyreplenishmentoverride: string | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENTPRIORITY",
    nullable: true,
    length: 5
  })
  replenishmentpriority: string | null;

  @DefColumn("decimal", {
    name: "REPLENISHMENTSEVERITY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  replenishmentseverity: string | null;

  @DefColumn("decimal", {
    name: "REPLENISHMENTCASECNT",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  replenishmentcasecnt: string | null;

  @DefColumn("varchar", { name: "LOCATIONTYPE", nullable: true, length: 10 })
  locationtype: string | null;

  @DefColumn("varchar", { name: "LOCATIONUOM", nullable: true, length: 10 })
  locationuom: string | null;

  @DefColumn("varchar", {
    name: "REPLENISHMENTUOM",
    nullable: true,
    length: 10
  })
  replenishmentuom: string | null;

  @DefColumn("decimal", {
    name: "REPLENISHMENTQTY",
    nullable: true,
    precision: 22,
    scale: 5,
    default: () => "'0.00000'"
  })
  replenishmentqty: string | null;

  @DefColumn("varchar", { name: "TODOUSER", nullable: true, length: 18 })
  todouser: string | null;

  @DefColumn("int", {
    name: "ALLOWREPLENISHFROMCASEPICK",
    nullable: true,
    default: () => "'0'"
  })
  allowreplenishfromcasepick: number | null;

  @DefColumn("int", {
    name: "ALLOWREPLENISHFROMBULK",
    nullable: true,
    default: () => "'0'"
  })
  allowreplenishfrombulk: number | null;

  @DefColumn("int", {
    name: "ALLOWREPLENISHFROMPIECEPICK",
    nullable: true,
    default: () => "'0'"
  })
  allowreplenishfrompiecepick: number | null;

  @DefColumn("varchar", { name: "OPTBATCHID", nullable: true, length: 10 })
  optbatchid: string | null;

  @DefColumn("varchar", { name: "OPTSEQUENCE", nullable: true, length: 10 })
  optsequence: string | null;

  @DefColumn("varchar", { name: "OPTADDDELFLAG", nullable: true, length: 1 })
  optadddelflag: string | null;

  @DefColumn("datetime", { name: "ADDDATE" })
  adddate: Date;

  @DefColumn("varchar", { name: "ADDWHO", length: 18 })
  addwho: string;

  @DefColumn("datetime", { name: "EDITDATE" })
  editdate: Date;

  @DefColumn("varchar", { name: "EDITWHO", length: 18 })
  editwho: string;
}
