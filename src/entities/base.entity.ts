import { CreateDateColumn, ObjectID, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

// PostgreSQL
export abstract class BaseEntityPostgres  {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @CreateDateColumn({ nullable: false, type: 'timestamptz' })
    createdDate: Date;

    @UpdateDateColumn({ nullable: false, type: 'timestamptz' })
    updateDate: Date;

}

export abstract class BaseEntityMongo {

    @PrimaryGeneratedColumn('uuid')
    id: ObjectID;

    @CreateDateColumn({ nullable: false, type: 'timestamptz' })
    createdDate: Date;

    @UpdateDateColumn({ nullable: false, type: 'timestamptz' })
    updateDate: Date;

}
export abstract class BaseEntitySql {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

}