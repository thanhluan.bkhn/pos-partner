import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as morgan from 'morgan';
import { join } from 'path';
import { configEnv, IEnvConfig } from './@config';
import { HttpExceptionFilter } from './@systems/exceptions';
import { GlobalPrefix } from './common/constants';
import redisService from './common/helpers/redis.service';
import socketService from './common/helpers/socket.service';
import { GlobalValidate } from './common/utilities/validate/global.validate';


const pathConfigJsonFile = join(__dirname, './../../config.json');
console.log('-------------------');
console.log(pathConfigJsonFile);
console.log('-------------------');
const envConfig = configEnv(pathConfigJsonFile);
export class BootServer {
    public envConfig: IEnvConfig;
    constructor(envConfig: IEnvConfig) {
        this.envConfig = envConfig;
    }
    async runApp(appModule: any) {
        const app = await NestFactory.create(appModule);
        await this.setup(app);

    }

    async setup(app: INestApplication) {
        const { APP, DBS, NAME, REDIS } = this.envConfig
        const { port } = APP;

        app.use(cookieParser());
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
        app.use(compression());
        app.use(express.static('public'));

        app.use(morgan('dev'));
        app.use(cors());

        app.useGlobalFilters(new HttpExceptionFilter());
        app.setGlobalPrefix(GlobalPrefix.V1);

        app.useGlobalPipes(new GlobalValidate({ transform: true,whitelist: false}));

        const server = await app.listen(port || 8002);
        setTimeout(() => {
            socketService.init(server);
        }, 1000);
        try {
            redisService.init(REDIS);
        } catch (error) {
        }
        console.log(`Server start on port ${port}. Open http://localhost:${port} to see results`);
    }

}
export default new BootServer(envConfig);
