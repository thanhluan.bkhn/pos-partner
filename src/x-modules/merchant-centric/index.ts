import { ChildModule } from '~/@core/decorator';

import { GeoController } from './controllers/geo.controller';
import { MerchantController } from './controllers/merchant.controller';
import { ResourceController } from './controllers/resource.controller';
import { TransactionController } from './controllers/transaction.controller';
import { ContractController } from './controllers/contract.controller';


@ChildModule({
  prefix: "merchant-centric",
  controllers: [
    GeoController,
    ResourceController,
    TransactionController,
    MerchantController,
    ContractController
  ],
})
export class MerchantCentricModule {
}
