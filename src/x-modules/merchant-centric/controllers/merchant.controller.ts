import { Body, Controller } from "@nestjs/common";
import { DefPost, InjectService } from "~/@core/decorator";
import { ContractService } from "~/services";
import { MerchantCentricInfoDto } from "~/services/standards/contract/dto/merchant-centric-info.dto";


@Controller("merchant")
export class MerchantController {

    @InjectService("ContractService")
    private contractService: ContractService


    @DefPost("create-contract")
    async createContract(@Body() body: MerchantCentricInfoDto) {
        return this.contractService.create(body);
    }

    
}