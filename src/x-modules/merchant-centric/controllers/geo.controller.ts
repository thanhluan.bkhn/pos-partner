import { Controller, Query } from '@nestjs/common';
import { DefGet, InjectService } from '~/@core/decorator';
import { GeoWardService, GeoCityService, GeoDistrictService } from '~/services';
import { GeoWardDto } from '~/services/standards/geo-ward/geo-ward.dto';
import { GeoCityDto } from '~/services/standards/geo-city/geo-city.dto';
import { GeoDistrict } from '~/entities/primary';
import { GeoDistrictDto } from '~/services/standards/geo-district/geo-district.dto';

@Controller("geo")
export class GeoController {

    @InjectService('GeoWardService')
    geoWardService: GeoWardService

    @InjectService('GeoCityService')
    geoCityService: GeoCityService

    @InjectService('GeoDistrictService')
    geoDistrictService: GeoDistrictService


    @DefGet("cities")
    async cities(@Query() pageRequest: GeoCityDto) {
        return this.geoCityService.findByPage(pageRequest)
    }
    @DefGet("wards")
    async wards(@Query() pageRequest: GeoWardDto) {
        return this.geoWardService.findByPage(pageRequest)
    }
    @DefGet("districts")
    async findByPage(@Query() pageRequest: GeoDistrictDto) {
        return this.geoDistrictService.findByPage(pageRequest)
    }

}
