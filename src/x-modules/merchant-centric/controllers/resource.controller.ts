import { Controller, Body, Query } from "@nestjs/common";
import { DefPost, InjectService, DefGet } from "~/@core/decorator";
import { AppParamService, ProductService, BusinessTypeService, ContractTypeService } from "~/services";
import { BusinessTypeStatusDto } from "~/services/standards/business-type/dto/business-type-status.dto";
import { ProductStatusDto } from "~/services/standards/product/dto/product-status.dto";

@Controller("resource")
export class ResourceController {

    @InjectService('AppParamService')
    appParamService: AppParamService

    @InjectService('ProductService')
    productService: ProductService

    @InjectService("BusinessTypeService")
    businessTypeService: BusinessTypeService

    @InjectService("ContractTypeService")
    contractTypeService: ContractTypeService
    

    @DefGet("master-data")
    async appParams(@Query() query: {  materDataKey: string }) {
        const {materDataKey = ''} = query
        const listKey = materDataKey.split(/,/g).map(item => item.trim());
        const data = await this.appParamService.listByType(listKey);
        return { data }
    }


    @DefGet("products")
    async products(@Query() query: ProductStatusDto) {
        const { status } = query;
        const data = await this.productService.findByStatus(status)
        return { data }
    }

    @DefGet('business-type')
    async businessType(@Query() query: BusinessTypeStatusDto) {
        const {status} = query;
        const data = await this.businessTypeService.findByStatus(status)
        return {data}
    }

    @DefGet('categories')
    async categories() {
        const data = await this.contractTypeService.all()
        return {data}
    }

    @DefGet("payment-methods")
    async paymentMethods() {
        const data = await this.productService.paymentMethods();
        return { data }
    }
}