import { Controller, Query } from '@nestjs/common';
import { DefGet, InjectService } from '~/@core/decorator';
import { GeoWardService, GeoCityService, GeoDistrictService, ContractService } from '~/services';
import { GeoWardDto } from '~/services/standards/geo-ward/geo-ward.dto';
import { GeoCityDto } from '~/services/standards/geo-city/geo-city.dto';
import { GeoDistrict } from '~/entities/primary';
import { GeoDistrictDto } from '~/services/standards/geo-district/geo-district.dto';

@Controller("contract")
export class ContractController {

    @InjectService('ContractService')
    contractService: ContractService

    @DefGet("detail")
    async cities(@Query("contractId") contractId: number) {
        return this.contractService.findByContractId(contractId);
    }

}
