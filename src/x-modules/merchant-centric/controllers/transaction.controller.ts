import { Controller, Query } from "@nestjs/common";
import { DefGet, InjectService } from "~/@core/decorator";
import { PosBillService } from "~/services";
import { TransactionDto } from "~/services/standards/pos-bill/dto/transaction.dto";


@Controller("transactions")
export class TransactionController {

    @InjectService("PosBillService")
    private posBillService: PosBillService


    @DefGet()
    async list(@Query() query: TransactionDto) {
        
        return this.posBillService.transactionByMerchant(query);
    }

    
}