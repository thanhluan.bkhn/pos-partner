import { DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { ConnectionOptions } from 'typeorm';

import * as envs from './envs';

// const rootDir = join(__dirname, './../');

export interface IEnvConfig {
    NAME?: string,
    BASE_MEDIA_URL?: string,
    APP: {
        port: number
    },
    REDIS?: {
        host: string,
        port: number,
        password: string,
        hostLocal?: string,
        portLocal?: number
        expires?: number
    }
    DBS: ConnectionOptions [],
    APIKEY?: {
        MERCHANT_CENTRIC: string
    }
}

export function configEnv(pathResourceConfig?: string): IEnvConfig {
    const envName = process.env.NODE_ENV || 'development';
    const currentConfig = (envs)[envName];
    if (pathResourceConfig /*&& process.env.NODE_ENV === 'production'*/ ) {
        try {
            const resourceConfig = require(pathResourceConfig);
            Object.keys(currentConfig).forEach((key: string, index: number) => {
                Object.assign(currentConfig[key], resourceConfig[key] || {})
            })
        } catch (error) {
            console.log(error.message);
        }

    }
    return {
        ...currentConfig,
        NAME: envName
    }
};
export function dbModules(dbs: ConnectionOptions []) {
    const multipleDatabaseModule: DynamicModule [] = [];
    dbs.forEach( item => {
        const typeormModule = TypeOrmModule.forRoot({
            ...item
        })
        multipleDatabaseModule.push(typeormModule)
    })
    return multipleDatabaseModule;
}
