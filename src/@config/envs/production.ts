import { IEnvConfig } from '..';

const config: IEnvConfig = {
    APP: {
        port: 8002
    },
    BASE_MEDIA_URL: "http://qa.pos.kootoro.com:8085/Files/upload/",
    REDIS: {
        host: '127.0.0.1',
        port: 6379,
        password: '123456',
        expires: 60000
    },
    DBS: [
        {
            name: 'default',
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'qltb2',
            password: 'qltb2passowrd',
            database: 'qltb2',
            entities: [
                'build/entities/' + 'primary' + '/**/*.js'
            ],
            synchronize: false,
            logging: false,
            timezone: 'local'
        }
    ],
    APIKEY: {
        MERCHANT_CENTRIC: '5ac429ed-1b53-4123-b9af-f04d6c13e7a1'
    }
}

export default config;
