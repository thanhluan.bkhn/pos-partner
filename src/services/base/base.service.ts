import { FindManyOptions } from 'typeorm';
import { BaseRepository } from '~/repositories/base.repository';

export class BaseService<TEntity, TRepository extends BaseRepository<TEntity>> {
    public repo: TRepository;

    save(entity: TEntity) {
        return this.repo.save(entity);
    }
    saves(entity: TEntity[]) {
        return this.repo.save(entity);
    }
    find(options?: FindManyOptions<TEntity>) {
        return this.repo.find(options)
    }
}