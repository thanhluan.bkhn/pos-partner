import { IsNotEmpty } from "class-validator";
import { BasePageRequest } from "~/common/models/base-page-request";



export class TransactionDto extends BasePageRequest {

    // @IsNotEmpty()
    merchantId: number;

    storeId: number;

    transactionCode: string;

    paymentMethod: string;

    // @IsDate()
    fromDate: Date
    // @IsDate()
    toDate: Date

}