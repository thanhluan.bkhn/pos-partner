import { EntityManager } from "typeorm";
import { isNullOrUndefined } from "util";
import { BindEntityManager, InjectCustomRepository, Service } from "~/@core/decorator";
import { PosBillRepository } from "~/repositories/primary/pos-bill.repository";
import { TransactionDto } from "./dto/transaction.dto";


@Service()
export class PosBillService {




    @InjectCustomRepository(PosBillRepository)
    private repo: PosBillRepository

    @BindEntityManager()
    private em: EntityManager;

    async transactionByMerchant(transactionDto: TransactionDto) {
        const fromSql = `
        FROM
            pos_bill pb
                LEFT JOIN
            (SELECT 
                display_name, id
            FROM
                users) us ON pb.created_uid = us.id
                LEFT JOIN
            shop st ON (st.id = pb.shop_id)
                LEFT JOIN
            shop mc ON (pb.merchant_id = mc.id)
                LEFT JOIN
            pos_payment pa ON (pa.bill_id = pb.id)
                LEFT JOIN
            pos_voucher_payment vo ON (vo.bill_id = pb.id)
                LEFT JOIN
            promotion pro ON (vo.promotion_id = pro.id)
                LEFT JOIN
            partner part ON (pro.partner_deploy = part.id)
                LEFT JOIN
            contract c ON (pb.merchant_id = c.merchant_id)
                LEFT JOIN
            contract_type ct ON (c.contract_type = ct.id)
        WHERE
            1 = 1 
        `;
        ;
        const selectSql = `
        SELECT 
            pb.id,
            pb.pay_with as paymentMethod,
            pb.tft_trans_code as transactionCode,
            pb.bill_number as billNumber,
            us.display_name as nameStaff,
            CONVERT(IFNULL(vo.discount_amount,0),char) as voucherValue ,
            ifnull(pb.paid_amount, 0) as billAmount,
            IFNULL(CONCAT(' Vourcher',': ',FORMAT(vo.discount_amount,0)),'') detailBill ,    part.name nccVoucher,
            pb.status as statusBill,
            pb.merchant_id as merchantId,
            mc.name as merchantName,
            pb.shop_id as storeId,
            st.name as storeName,
            pro.name as promotionName,
            pb.device_serial as serial,
            vo.code as voucherCode,
            pb.created_date AS dateCreatedBill 
        `;

        const from = ` from ( ${selectSql} ${fromSql}) tb where 1 = 1 `;



        let whereSql = ` `;
        const { fromDate = undefined, toDate = undefined,pageIndex,pageSize,paymentMethod, ...trans } = transactionDto

        Object.keys(trans).forEach(key => {
            if (trans[key]) {
            
                whereSql += ` and ${key} = '${trans[key]}'`;

            }
        })
        if(paymentMethod) {
            const strs = paymentMethod.split(/_/);
            if(strs.length > 1 && strs[0] != "EDC") {
                whereSql += ` and paymentMethod = '${strs[1]}'`
            } else {
                whereSql += ` and paymentMethod = '${paymentMethod}' `;
            }
            
        }

        if (fromDate) {
            whereSql += ` and dateCreatedBill >= '${fromDate}' `;
        }
        if (toDate) {
            whereSql += ` and dateCreatedBill <= '${toDate}' `;
        }

        let dataSql = ` select * ${from} ${whereSql} `;

        const offset = pageSize && pageIndex && pageIndex > 0 ? (pageIndex - 1) * pageSize : undefined;

        if(!isNullOrUndefined(pageSize) && !isNaN(pageSize)){
            dataSql += ` limit ${pageSize} `;
        }
        if(!isNullOrUndefined(offset) && !isNaN(offset)){
            dataSql += ` offset ${offset} `;
        }
        const countSql = ` select count(*) as total ${from} ${whereSql} `;

        const res = await Promise.all([
            this.repo.query(dataSql),
            this.repo.query(countSql)
        ])
        return {
            data: res[0] || [],
            total: res[1][0].total || 0
        }
    }

}