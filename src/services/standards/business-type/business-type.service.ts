import { Service, InjectCustomRepository } from "~/@core/decorator";
import { BusinessTypeRepository } from "~/repositories/primary";
import { EBusinessTypeStatus } from "~/entities/primary";
import { isUndefined } from "util";

@Service()
export class BusinessTypeService {

    @InjectCustomRepository(BusinessTypeRepository)
    private repo: BusinessTypeRepository

    findByStatus(status?: EBusinessTypeStatus) {
        const where = {};

        if (status && !isUndefined(EBusinessTypeStatus[status]))  {
            Object.assign(where, {
                status: EBusinessTypeStatus[status]
            })
        }
        return this.repo.find({
            where: {
                ...where
            }
        });
    }

}