import { EBusinessTypeStatus } from "~/entities/primary";
import { IsOptional, IsEnum } from "class-validator";
import { Utils } from "~/@utils";

export class BusinessTypeStatusDto {
    @IsOptional()
    @IsEnum(EBusinessTypeStatus, {
        message: `Key status not in list  ${Utils.enumKeys(EBusinessTypeStatus)} `
    })
    status: EBusinessTypeStatus
}