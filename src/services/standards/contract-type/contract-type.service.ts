import { Service, InjectCustomRepository } from "~/@core/decorator";
import { ContractTypeRepository } from "~/repositories/primary/contract-type.repository";


@Service()
export class ContractTypeService {

    @InjectCustomRepository(ContractTypeRepository)
    private repo: ContractTypeRepository


    all(){
       return  this.repo.find()
    }
}