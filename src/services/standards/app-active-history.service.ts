import { InjectCustomRepository, Service } from "~/@core/decorator";
import { BaseService } from "../base/base.service";
import { AppActiveHistory } from "~/entities/primary";
import { AppActiveHistoryRepository } from "~/repositories/primary/app-active-history.repository";

@Service()
export class AppActiveHistoryService extends BaseService<AppActiveHistory, AppActiveHistoryRepository> {

    @InjectCustomRepository(AppActiveHistoryRepository)
    repo: AppActiveHistoryRepository;
    all () {
        const entity = new AppActiveHistory();
        this.repo.save(entity);
        return this.repo.find();
    }
}
