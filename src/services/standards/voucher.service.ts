import { BadRequestException, HttpStatus, UnauthorizedException } from '@nestjs/common';
import moment = require('moment');
import uuid = require('uuid');
import { HttpMessage } from '~/common/models/http-message';
import { VoucherRepository } from '~/repositories/primary/voucher.repository';
import { Service, InjectCustomRepository } from '~/@core/decorator';
import { VoucherStatus } from '~/common/enums';

@Service()
export class VoucherService {
    @InjectCustomRepository(VoucherRepository)
    public voucherRepository: VoucherRepository;

    async distributeVoucher( promotionId: number ) {
        let vc = await this.voucherRepository.findOne({
            relations: ["promotion"],
            where: {
                promotion: {id: promotionId},
                status: VoucherStatus.NEW
            }
        });
        if (!vc) { return null; }
        await this.voucherRepository.update(vc.id, { status: VoucherStatus.DISTRIBUTED });
        vc.status = VoucherStatus.DISTRIBUTED;
        return vc;
    }

}
