import {  } from 'typeorm';
import { InjectCustomRepository, Service } from '~/@core/decorator';
import { IPageRequest, IPageResponse } from '~/common/interfaces/paginations';
import { GeoCountry } from '~/entities/primary/geo-country';
import { GeoCountryRepository } from '~/repositories/primary';
import moment = require('moment');

@Service()
export class GeoCountryService {
    @InjectCustomRepository(GeoCountryRepository)
    public repo: GeoCountryRepository;

    async findByPage(pageRequest: IPageRequest<GeoCountry>) {
        moment.utc().add()
        return this.repo.findByPage(pageRequest);
    }

}
