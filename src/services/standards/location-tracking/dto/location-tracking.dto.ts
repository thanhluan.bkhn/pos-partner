import { Expose } from "class-transformer";
import { IsNotEmpty } from "class-validator";

export class LocationTrackingDto {
    @Expose()
    @IsNotEmpty()
    serial: string;
    @Expose()
    @IsNotEmpty()
    latitude: Number;
    @Expose()
    @IsNotEmpty()
    longitude: Number;
}