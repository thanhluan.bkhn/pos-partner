export class LogPermissionDto {
    
    serial: string;
    accessFineLocation: boolean;
    accessCoarseLocation: boolean;
    
}