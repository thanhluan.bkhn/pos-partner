import { plainToClass } from 'class-transformer';
import { isUndefined } from 'util';
import { InjectCustomRepository, Service } from '~/@core/decorator';
import { DeviceLocationRepository, DeviceLocationHistoryRepository } from '~/repositories/primary';
import { LocationTrackingDto } from './dto/location-tracking.dto';
import { DeviceLocationHistory } from '~/entities/primary';


@Service()
export class LocationTrackingService {
    @InjectCustomRepository(DeviceLocationRepository)
    private deviceLocationRepository: DeviceLocationRepository;

    @InjectCustomRepository(DeviceLocationHistoryRepository)
    private deviceLocationHistoryRepository: DeviceLocationHistoryRepository;


    async writeTrackingLocation(locationTrackingDto: LocationTrackingDto){
        const {serial,latitude,longitude} = locationTrackingDto;
        const sql = `
        INSERT INTO device_location(serial,latitude,longitude) 
        VALUES 
           ('${serial}','${latitude}','${longitude}') 
        ON DUPLICATE KEY UPDATE 
            latitude = '${latitude}' ,
            longitude = '${longitude}',
            updated_date = now()
        ` ;
      await  this.deviceLocationRepository.query(sql);
      const locationDeviceHistory = { ...new DeviceLocationHistory(),...locationTrackingDto}
      locationDeviceHistory.createdDate = new Date();
      await  this.deviceLocationHistoryRepository.save(locationDeviceHistory);
      return { 
          message: "DONE"
      }
    }
}
