import { plainToClass } from 'class-transformer';
import { isUndefined } from 'util';
import { InjectCustomRepository, Service } from '~/@core/decorator';
import { GeoWardRepository } from '~/repositories/primary';

import { GeoWardDto } from './geo-ward.dto';

@Service()
export class GeoWardService {
    @InjectCustomRepository(GeoWardRepository)
    public repo: GeoWardRepository;

    async findByPage(geoWardDto: GeoWardDto) {
            const wherePlain = plainToClass(GeoWardDto, geoWardDto, {excludeExtraneousValues: true});
            Object.keys(wherePlain).forEach(key => {
                // const newKey = camelCaseToSnakeCase(key).replace(/_/g,".");
                Object.assign(wherePlain, {
                    [key]: geoWardDto[key]
                })
            })
            const {pageIndex, pageSize, districtId, ...where} = wherePlain;
            const offset = pageSize && pageIndex && pageIndex > 0 ? (pageIndex - 1) * pageSize : undefined

            let query = this.repo
            .createQueryBuilder("root")
            .leftJoin("root.district", "district")
            .andWhere("district.id = :districtId ", {districtId})
            Object.keys(where).forEach( key => {
                const value = where[key];
                if (!isUndefined(value)) {
                    query =  query.andWhere(`root.${key} = :${key}`, {[key]: `%${value}%`})
                }
            })

            const data = query.offset(offset).limit(pageSize).getMany()
            const total = query.getCount();
            const res = await Promise.all([
                data,
                total
            ])
            return {
                data: res[0],
                total: res[1]
            }
    }

}
