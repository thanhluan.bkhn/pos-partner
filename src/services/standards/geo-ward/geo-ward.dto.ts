import { Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { BasePageRequest } from '~/common/models/base-page-request';

export class GeoWardDto  extends BasePageRequest {

    @IsNotEmpty()
    @Expose()
    districtId: number

    @Expose()
    id: number;

    @Expose()
    code: string;

    @Expose()
    name: string;

    @Expose()
    status: number;

    @Expose()
    description: string

    @Expose()
    prefix: string
}