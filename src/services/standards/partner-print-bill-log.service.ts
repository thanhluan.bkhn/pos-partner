import { Service, InjectCustomRepository } from "~/@core/decorator";
import { PartnerPrintBillLogRepository } from "~/repositories/primary";
import { PartnerPrintMessage } from "~/x-modules/partner/models/partner-print-message";
import { PartnerPrintBillLog } from "~/entities/primary";

@Service()
export class PartnerPrintBillLogService {

    @InjectCustomRepository(PartnerPrintBillLogRepository)
    public partnerPrintBillLogRepository: PartnerPrintBillLogRepository

    public saveLog(printMessage: PartnerPrintMessage) {
        try {
            const printLog = new PartnerPrintBillLog();
            printLog.lastUpdate = new Date();
            printLog.templateName = PartnerPrintMessage.getTemplatePrintName(printMessage);
            printLog.data = printMessage.data;
            printLog.printDate = new Date(printMessage.printTime);
            printLog.merchantId = printMessage.merchantId;
            printLog.terminalId = printMessage.terminalId;
            this.partnerPrintBillLogRepository.save(printLog).catch(err => {
                console.log("PartnerPrintBillLogService-saveLog error", err.message);
            });
        } catch (error) {
            console.log("PartnerPrintBillLogService-saveLog error", error.message);
        }
    }

    async updatePrintStatus(result: boolean, serialNumber: string, templateName: string) {
        try {
            let printLogs = await this.partnerPrintBillLogRepository.find({
                where: {
                    terminalId: serialNumber, templateName: templateName
                }
            });

            if (printLogs.length == 0) {
                console.log("Writelog print: ", templateName, " not found ");
                return;
            }

            let printLog = printLogs[printLogs.length - 1];
            printLog.result = result ? true : false;
            this.partnerPrintBillLogRepository.save(printLog);
        } catch (error) {
            console.log("updatePrintStatus error", error);
        }

    }

}
