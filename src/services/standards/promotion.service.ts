import { BadRequestException, HttpStatus, UnauthorizedException } from '@nestjs/common';
import moment = require('moment');
import uuid = require('uuid');
import { HttpMessage } from '~/common/models/http-message';
import { PromotionRepository } from '~/repositories/primary/promotion.repository';
import { Service, InjectCustomRepository } from '~/@core/decorator';
import { PromotionStatus } from '~/common/enums';

@Service()
export class PromotionService {
    @InjectCustomRepository(PromotionRepository)
    public promotionRepository: PromotionRepository;

    getPromtionByPartnerDeloyAndSKU( partnerCode: string, sku: string ) {
        return this.promotionRepository.createQueryBuilder("promotion")
            .innerJoinAndSelect("promotion.partnerDeploy", "partner", ` partner.code = '${partnerCode}' and partner.status = 1 `)
            .where({partnerPromotionCode: sku})
            .getOne()
    }

}
