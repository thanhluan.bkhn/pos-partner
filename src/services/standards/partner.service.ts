import { BadRequestException, HttpStatus, UnauthorizedException, InternalServerErrorException } from '@nestjs/common';
import moment = require('moment');
import uuid = require('uuid');
import { HttpMessage } from '~/common/models/http-message';
import { PartnerRepository } from '~/repositories/primary/partner.repository';
import { Service, InjectCustomRepository, InjectService } from '~/@core/decorator';
import { PartnerRequestVoucherDTO } from '~/x-modules/partner/models/partner-request-voucher';
import { VoucherDTO } from '~/x-modules/partner/models/voucher';
import { VoucherService } from './voucher.service';
import { PromotionService } from './promotion.service';
import { SecurityHelper } from '~/common/helpers/security-helper';
import { PartnerVoucherStatus } from '~/common/enums';
import { HttpPartnerException } from '~/x-modules/partner/partner.exceptions';

const VOUCHER_REQUEST_EXPIRE = 30; // time in second

@Service()
export class PartnerService {
    @InjectCustomRepository(PartnerRepository)
    public partnerRepository: PartnerRepository;

    @InjectService("PromotionService")
    promotionService: PromotionService;
    @InjectService("VoucherService")
    voucherService: VoucherService;

    async findSecretToken(publicToken: string) {
        const partner = await this.partnerRepository.findOne({
            where: {
                publicToken
            }
        })
        if (!partner || !partner.secretToken) {
            throw new UnauthorizedException(new HttpMessage(HttpStatus.UNAUTHORIZED, 'Not exist partner'));
        }
        return partner.secretToken;
    }
    buildTemplatePrintFormData(billDetail: any) {
        return billDetail;
    }

    async getVoucher(requestDTO: PartnerRequestVoucherDTO) {
        let currentTimesptamp = new Date().getTime();
        if (currentTimesptamp - requestDTO.timestamp > VOUCHER_REQUEST_EXPIRE * 1000 ||
            currentTimesptamp < requestDTO.timestamp
        ) {
            throw new HttpPartnerException(
                PartnerVoucherStatus.REQUEST_EXPIRE, "Request expired",
            );
        }

        await this.verifyPartnerSignature(
            requestDTO.partner_code + requestDTO.sku + requestDTO.timestamp,
            requestDTO.partner_code,
            requestDTO.signature
        );

        return this.distributeVoucher(requestDTO.partner_code, requestDTO.sku);
    }

    async distributeVoucher(partnerCode: string, sku: string) {
        try {
            let promotion = await this.promotionService.getPromtionByPartnerDeloyAndSKU(partnerCode, sku);
            if (!promotion) {
            throw new HttpPartnerException(
                PartnerVoucherStatus.SKU_NOT_FOUND, "Sku not found",
            );
        }

            let vc = await this.voucherService.distributeVoucher(promotion.id);
            if (!vc) {
            throw new HttpPartnerException(
                PartnerVoucherStatus.PROMOTION_EMPTY, "Promotion empty code"
            );
        }
            let vcDto = new VoucherDTO;
            vcDto.parseFromVoucher(vc);
            return new HttpMessage(PartnerVoucherStatus.SUCCESS, "Success", vcDto);
        } catch (error) {
            console.info("Distribute VC eror: {}", error);
            if (error.code) { throw error; }
            throw new InternalServerErrorException("Server error");
        }

    }

    public async verifyPartnerSignature(data: string, partnerCode: string, signature: string) {
        let partner = await this.getPartnerByCode(partnerCode);
        if (!partner) {
            throw new HttpPartnerException(
                PartnerVoucherStatus.PARTNER_NOT_FOUND, "Partner not found"
            );
        }

        let systemSign = SecurityHelper.hmac256(data, partner.secretToken);
        if (signature != systemSign) {
            throw new UnauthorizedException("Signature not vaid, (debug only, signature: " + systemSign + ")");
        }
        return true;
    }

    async getPartnerByCode(partnerCode: string) {
        return this.partnerRepository.findOne({
            where: {
                code: partnerCode,
                status: 1
            }
        })
    }
}
