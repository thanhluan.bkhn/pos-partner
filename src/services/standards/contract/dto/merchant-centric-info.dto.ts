import { Expose, Type } from "class-transformer";
import { IsDate, IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber } from "class-validator";

export class MerchantCentricInfoDto {

    @Expose()
    @IsNotEmpty()
    businessTypeId: number;
    @Expose()
    @IsNotEmpty()
    categoryId: number;
    @Expose()
    @IsNotEmpty()
    countryId: number;
    @Expose()
    @IsNotEmpty()
    cityId: number;
    @Expose()
    @IsNotEmpty()
    districtId: number;
    @Expose()
    @IsNotEmpty()
    wardId: number;
    @Expose()
    @IsNotEmpty()
    companyName: string;
    @Expose()
    @IsNotEmpty()
    representativeName: string;
    @Expose()
    @IsNotEmpty()
    representativePosition: string;
    @Expose()
    @IsNotEmpty()
    @IsPhoneNumber('vn')
    representativePhone: string;
    @Expose()
    @IsNotEmpty()
    @IsEmail()
    representativeEmail: string;

    @Expose()
    @IsNotEmpty()
    //@IsPhoneNumber('vn'), MCC đã verify
    phoneAccountNo: string;
    @Expose()
    @IsNotEmpty()
    accountName: number;

    @Expose()
    // @IsNotEmpty()
    // @IsDate()
    @Type(type => Date)
    effectiveDate: Date;
    @Expose()
    // @IsNotEmpty()
    // @IsDate()
    @Type(type => Date)
    expirationDate: Date;
    @Expose()
    addressDetail: string;
    @Expose()
    bankName: string;
    @Expose()
    bankOwner: string;
    @Expose()
    bankNo: string;
    @Expose()
    idCard: string;
    @IsOptional()
    @IsDate()
    @Type(type => Date)
    @Expose()
    dateIssueCard: Date;
    @IsOptional()
    @IsDate()
    @Type(type => Date)
    @Expose()
    placeIssuaCard: Date;

    @Expose()
    taxNo: string
}



    
    
    