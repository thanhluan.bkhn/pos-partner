import { Expose } from "class-transformer";

export class ContractAppendixResponseDTO{
    @Expose() id: number;
    @Expose() contractId: string | null;
    @Expose() description: string | null;
    @Expose() type: string | null;
    @Expose() status: number | null;
    @Expose() createdDate: Date | null;
    @Expose() discountCustomer: number | null;
    @Expose() discountFixed: number | null;
    @Expose() contractAppendixNumber: string | null;
    @Expose() updatedDate: Date | null;
    @Expose() conditionApply: string | null;
    @Expose() note: string | null;
}