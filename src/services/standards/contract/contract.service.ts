import { BadRequestException, InternalServerErrorException, HttpStatus } from '@nestjs/common';
import { EntityManager, In, Not } from 'typeorm';
import { isNullOrUndefined } from 'util';
import { BindEntityManager, InjectCustomRepository, Service, InjectService } from '~/@core/decorator';
import { Contract, EContractStatus, GeoWard, ContractAppendix } from '~/entities/primary';
import { BusinessTypeRepository } from '~/repositories/primary';
import { ContractTypeRepository } from '~/repositories/primary/contract-type.repository';
import { ContractRepository } from '~/repositories/primary/contract.repository';
import { MerchantCentricInfoDto } from './dto/merchant-centric-info.dto';
import { ContractResponseDTO } from './dto/contract-response.dto';
import { plainToClass } from 'class-transformer';
import { ProductService } from '../product/product.service';
import { ContractAppendixRepository } from '~/repositories/primary/contract-appendix.repository';
import { configEnv } from '~/@config';
import bootServer from '~/boot.server';


@Service()
export class ContractService {
    @BindEntityManager()
    private em: EntityManager

    @InjectCustomRepository(ContractRepository)
    private repo: ContractRepository;

    @InjectCustomRepository(BusinessTypeRepository)
    private businessTypeRepo: BusinessTypeRepository;
    @InjectCustomRepository(ContractTypeRepository)
    private contractTypeRepo: ContractTypeRepository;
    @InjectService("ProductService")
    private productService: ProductService;
    @InjectCustomRepository(ContractAppendixRepository)
    public contractAppendixRepository: ContractAppendixRepository;


    async create(merchantInfo: MerchantCentricInfoDto) {


        const {
            categoryId: contractType,
            representativeName: name,
            representativePosition: position,
            representativePhone: phone,
            representativeEmail: email,
            effectiveDate: fromDate,
            expirationDate: toDate,
            phoneAccountNo: accountNo,
            ...objDis
        } = merchantInfo

        const contract = {
            ...objDis,
            contractType,
            name,
            position,
            phone,
            email,
            fromDate,
            toDate,
            accountNo
        }


        const geo = await this.em.createQueryBuilder(GeoWard, "ward")
            .innerJoinAndSelect("ward.district", "district")
            .innerJoinAndSelect("district.city", "city")
            .innerJoinAndSelect("city.country", "country")
            .andWhere("ward.id = :wardId", { wardId: objDis.wardId })
            .andWhere("city.id = :cityId", { cityId: objDis.cityId })
            .andWhere("country.id = :countryId", { countryId: objDis.countryId })
            .select(`
            city.name as cityName,city.prefix as prefixCity ,
            district.name as districtName,district.prefix as prefixDistrict,
            ward.name as wardName, ward.prefix as prefixWard `)
            .getRawOne();


        if (isNullOrUndefined(geo)) {
            throw new BadRequestException("GEO DATA NOTFOUND")
        }
        const { addressDetail = '' } = merchantInfo;
        const { cityName, prefixCity, districtName, prefixDistrict, wardName, prefixWard } = geo;
        const fullAddress = ` ${addressDetail} ${prefixWard} ${wardName}, ${prefixDistrict} ${districtName}, ${prefixCity} ${cityName} `;

        const businessType = await this.businessTypeRepo.findOne({
            where: {
                id: contract.businessTypeId
            }
        })

        if (isNullOrUndefined(businessType)) {
            throw new BadRequestException("BUSINESS TYPE ID NOTFOUND")
        }

        const category = await this.contractTypeRepo.findOne({
            where: {
                id: merchantInfo.categoryId
            }
        })

        if (isNullOrUndefined(category)) {
            throw new BadRequestException("CATEGORY ID NOTFOUND")
        }

        const checkContract = await this.repo.findOne({
            where: {
                accountNo: merchantInfo.phoneAccountNo,
                status: Not(In([EContractStatus.CANCEL, EContractStatus.LIQUIDATED])),
            }
        })

        if (checkContract) {
            throw new BadRequestException("PHONEACCOUNTNO EXISTED CONTRACT ACTIVE")
        }

        const entity = await this.repo.save(Object.assign(new Contract, {
            ...contract,
            status: EContractStatus.DRAF,
            fullAddress
        }));
        return {
            contractId: entity.contractId
        }
    }

    async getContractAppendix(contractId: number) {
        return this.contractAppendixRepository.find({
            where: { contractId }
        })
    }

    async getDeviceOfMerchant(merchantId: number) {
        return this.repo.query(`
        SELECT 
                device.id, device.serial, device.status, shop.name as storeName, doInfo.close_date as deliveryDate
            FROM
                device 
                    INNER JOIN
                shop ON shop.id = shop_id
               left join (
					select dood.device_id, doo.close_date
					from device_output_order_detail dood
					inner join device_output_order doo on doo.id = dood.order_id
					where dood.merchant_id = ${merchantId} and dood.status is null and doo.status <> 0
               ) as doInfo on doInfo.device_id = device.id
            WHERE
                device.merchant_id = ${merchantId};    
        `);
    }

    async findByContractId(contractId: number) {
        try {
            //let contractDTO = new ContractResponseDTO();
            let contract = await this.repo.findOne({
                where: { contractId }
            });
            if (!contract) throw new BadRequestException("Contract not exist");
            let contractDTO = plainToClass(ContractResponseDTO, contract, { excludeExtraneousValues: true });
            contractDTO.paymentMethods = await this.productService.getPaymentMethodByContract(contractId);
            contractDTO.contractAppendix = await this.getContractAppendix(contractId);
            contractDTO.devices = await this.getDeviceOfMerchant(contractDTO.merchantId);
            this.updateResourceMedia(contractDTO);
            return contractDTO;
        } catch (error) {
            console.log("[GET CONTRACT DETAIL ERROR] ", error);
            throw new InternalServerErrorException("INTERNAL_SERVER_ERROR");
        }
    }

    private updateResourceMedia(contractDTO: ContractResponseDTO) {
        contractDTO.contractFile = contractDTO.contractFile ?
            bootServer.envConfig.BASE_MEDIA_URL + contractDTO.contractFile : contractDTO.contractFile;
        contractDTO.softFile = contractDTO.softFile ?
            bootServer.envConfig.BASE_MEDIA_URL + contractDTO.softFile : contractDTO.softFile;
        contractDTO.licenceIn = contractDTO.licenceIn ?
            bootServer.envConfig.BASE_MEDIA_URL + contractDTO.licenceIn : contractDTO.licenceIn;

        if (contractDTO.contractAppendix) {
            contractDTO.contractAppendix.forEach((ca: ContractAppendix) => {
                ca.url = ca.url ? bootServer.envConfig.BASE_MEDIA_URL + ca.url : ca.url
            })
        }

        return contractDTO;

    }


}
