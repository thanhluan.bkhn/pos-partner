import { plainToClass } from 'class-transformer';
import { isUndefined, isNullOrUndefined } from 'util';
import { InjectCustomRepository, Service } from '~/@core/decorator';
import { GeoDistrictRepository } from '~/repositories/primary';
import { GeoDistrictDto } from './geo-district.dto';
import { BadRequestException } from '@nestjs/common';

@Service()
export class GeoDistrictService {
    @InjectCustomRepository(GeoDistrictRepository)
    public repo: GeoDistrictRepository;

    async findByPage(geoDistrictDto: GeoDistrictDto) {
       
        const wherePlain = plainToClass(GeoDistrictDto, geoDistrictDto, { excludeExtraneousValues: true });
        Object.keys(wherePlain).forEach(key => {
            Object.assign(wherePlain, {
                [key]: geoDistrictDto[key]
            })
        })
        const { pageIndex, pageSize, cityId ,countryId , ...where } = wherePlain;

        if(isNullOrUndefined(countryId) && isNullOrUndefined(cityId)) {
            throw new BadRequestException("One of two values [countryId, cityId] ​​must not be blank");
        }

        const offset = pageSize && pageIndex && pageIndex > 0 ? (pageIndex - 1) * pageSize : undefined

        let query = this.repo
            .createQueryBuilder("root")
            .leftJoin("root.city", "city")
            .leftJoin("city.country","country")

        if(!isNullOrUndefined(cityId)){
            query.andWhere("city.id = :cityId", { cityId })
        }
        if(!isNullOrUndefined(countryId)){
            query.andWhere("country.id = :countryId ", { countryId })
        }
        Object.keys(where).forEach(key => {
            const value = where[key];
            if (!isUndefined(value)) {
                query = query.andWhere(`root.${key} = :${key}`, { [key]: `%${value}%` })
            }
        })

        const data = query.offset(offset).limit(pageSize).getMany()
        const total = query.getCount();
        const res = await Promise.all([
            data,
            total
        ])
        return {
            data: res[0],
            total: res[1]
        }
    }

}
