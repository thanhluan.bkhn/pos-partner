import { Service, InjectCustomRepository } from "~/@core/decorator";
import { AppParamRepository } from "~/repositories/primary";
import { In } from "typeorm";
import { snakeCaseToCamelCase } from "~/common/utilities/convert";

@Service()
export class AppParamService {
    @InjectCustomRepository(AppParamRepository)
    public repo: AppParamRepository;

    async listByType(types: string[]) {
        const output = {};
        for (const type of types) {
            const res =  await this.repo.find({
                where: {
                    type: type
                }
            });

            Object.assign(output, {
                [type]: res
            })
        }
        return  output;
    }
}