import { InjectCustomRepository, Service } from '~/@core/decorator';
import { GeoCityRepository } from '~/repositories/primary/geo-city.repository';
import { plainToClass } from 'class-transformer';
import { GeoCityDto } from './geo-city.dto';
import { isUndefined } from 'util';
@Service()
export class GeoCityService {
    @InjectCustomRepository(GeoCityRepository)
    public repo: GeoCityRepository;

    async findByPage(geoCityDto: GeoCityDto) {

            const wherePlain = plainToClass(GeoCityDto, geoCityDto, {excludeExtraneousValues: true});
            Object.keys(wherePlain).forEach(key => {
                // const newKey = camelCaseToSnakeCase(key).replace(/_/g,".");
                Object.assign(wherePlain, {
                    [key]: geoCityDto[key]
                })
            })
            const {pageIndex, pageSize, countryId, ...where} = wherePlain;
            const offset = pageSize && pageIndex && pageIndex > 0 ? (pageIndex - 1) * pageSize : undefined

            let query = this.repo
            .createQueryBuilder("root")
            .leftJoin("root.country", "country")
            .andWhere("country.id = :countryId ", {countryId})
            Object.keys(where).forEach( key => {
                const value = where[key];
                if (!isUndefined(value)) {
                    query =  query.andWhere(`root.${key} = :${key}`, {[key]: `%${value}%`})
                }
            })

            const data = query.offset(offset).limit(pageSize).getMany()
            const total = query.getCount();
            const res = await Promise.all([
                data,
                total
            ])
            return {
                data: res[0],
                total: res[1]
            }
    }

}
