import { Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { BasePageRequest } from '~/common/models/base-page-request';

export class GeoCityDto  extends BasePageRequest {

    @IsNotEmpty()
    @Expose()
    countryId: number

    @Expose()
    id: number;

    @Expose()
    code: string;

    @Expose()
    name: string;

    @Expose()
    status: number;

    @Expose()
    description: string

    @Expose()
    prefix: string
}