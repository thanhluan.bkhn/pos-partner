import { IsOptional, IsEnum } from "class-validator";
import { EProductStatus } from "~/entities/primary";
import { Utils } from "~/@utils";
import { Type } from "class-transformer";
import { DefDto, DtoProp } from "~/@core/decorator/dto.decorator";

@DefDto()
export class ProductStatusDto {

    @IsOptional()
    @IsEnum(EProductStatus, {
        message: `Key status not in list  ${Utils.enumKeys(EProductStatus)} `
    })

    status: EProductStatus

    @DtoProp("luan")
    luan: string
}