import { isUndefined } from 'util';
import { InjectCustomRepository, Service } from '~/@core/decorator';
import { EProductStatus, ContractAppendix } from '~/entities/primary';
import { ProductRepository } from '~/repositories/primary';
import { ContractAppendixRepository } from '~/repositories/primary/contract-appendix.repository';

@Service()
export class ProductService {
    @InjectCustomRepository(ProductRepository)
    public repo: ProductRepository;

    async findByStatus(status?: EProductStatus) {
        const where = {};
        if (status && !isUndefined(EProductStatus[status])) {
            Object.assign(where, {
                status: EProductStatus[status]
            })
        }
        return this.repo.find({
            where: {
                ...where
            }
        });
    }

    async getPaymentMethodByContract(contractId: number) {
        try {
            let cashMethod = { code: "CASH", name: "TIỀN MẶT" }
            let methods: [any] = await this.repo.query(`
                SELECT DISTINCT
                    UPPER(CONCAT(p.name, ' ', pn.name)) AS name,
                    UPPER(CONCAT(p.code, '_', pn.code)) AS code
                FROM
                    contract_partner cp
                        INNER JOIN
                    contract_partner_detail cpd ON cp.id = cpd.contract_partner_id
                        INNER JOIN
                    product p ON cpd.product_id = p.id
                        INNER JOIN
                    partner pn ON cp.partner_id = pn.id
                WHERE
                    cp.contract_id = ${contractId} AND cp.status = 1;
            `);
            methods.push(cashMethod);
            return methods;
        } catch (error) {
            console.log("[GetPaymentMethodByContract] error: ", error);
            return [];
        }
    }

    async paymentMethods() {
        let cashMethod = { code: "CASH", name: "TIỀN MẶT" }
        let methods: [any] = await this.repo.query(`
            SELECT
                UPPER(CONCAT(product.name, ' ', partner.name)) AS name,
                UPPER(CONCAT(product.code, '_', partner.code)) AS code
            FROM
                partner_product
                    INNER JOIN
                partner ON partner_id = partner.id
                    INNER JOIN
                product ON product_id = product.id
            WHERE
                partner.status = 1
                    AND product.code = 'EWL'
                    OR product.code = 'EDC'
            ORDER BY partner.code ;
        `);
        methods.push(cashMethod);
        return methods;
    }

}
