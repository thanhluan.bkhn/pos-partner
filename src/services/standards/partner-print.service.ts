import { BadRequestException, HttpException, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { ESocketEvent, ETemplatePrintType } from '~/common/enums';
import socketService from '~/common/helpers/socket.service';
import { HttpMessage } from '~/common/models/http-message';
import { PrintTemplate, SocketMessage } from '~/common/models/socket-message';

import { PartnerPrintBillLogService } from './partner-print-bill-log.service';
import { PartnerService } from './partner.service';
import { Service, InjectCustomRepository, InjectService } from '~/@core/decorator';
import { PartnerDevicePosRepository } from '~/repositories/primary';
import { PartnerPrintMessage } from '~/x-modules/partner/models/partner-print-message';

const TIMEOUT_RETRY = 5000;

@Service()
export class PartnerPrintService {

    @InjectCustomRepository(PartnerDevicePosRepository)
    public partnerDevicePosRepository: PartnerDevicePosRepository

    @InjectService('PartnerService')
    private partnerService: PartnerService;

    @InjectService('PartnerPrintBillLogService')
    private partnerPrintBillLogService: PartnerPrintBillLogService;

    async partnerPrintBill(
        printMessage: PartnerPrintMessage
    ) {

        const secretToken = await this.partnerService.findSecretToken(printMessage.authenticationKey);
        const ownerSignature = PartnerPrintMessage.getMD5Hash(printMessage, secretToken);
        let signatureHint = "";
        if (ownerSignature !== printMessage.signature) {
            if (process.env && process.env.NODE_ENV == "development" || true) {
                signatureHint = " My signature(debug only): " + ownerSignature;
            }
            console.log("partnerPrintBill", printMessage.terminalId, " status:", HttpStatus.UNAUTHORIZED, "signature not valid." + signatureHint);
            throw new UnauthorizedException(new HttpMessage(HttpStatus.UNAUTHORIZED, "signature not valid." + signatureHint));
        }
        printMessage.templateType = PartnerPrintMessage.getTemplateType(printMessage);
        PartnerPrintMessage.checkTemplateData(printMessage);
        this.partnerPrintBillLogService.saveLog(printMessage);
        const partnerDevicePos = await this.partnerDevicePosRepository.findOne({
            where: {
                partnerDeviceId: printMessage.merchantId,
                posSerialNumber: printMessage.terminalId,
                isDeleted: false
            }
        });

        if (!partnerDevicePos) {
            console.log("partnerPrintBill", printMessage.terminalId, " status:", HttpStatus.BAD_REQUEST, "Not find device register");
            throw new UnauthorizedException(new HttpMessage(HttpStatus.BAD_REQUEST, "Not find device register"));
        }
        if (!PartnerPrintMessage.isValidRequestDate(printMessage)) {
            console.log("partnerPrintBill", printMessage.terminalId, " status:", HttpStatus.BAD_REQUEST, "Request time not valid");
            throw new BadRequestException(new HttpMessage(HttpStatus.BAD_REQUEST, "Request time not valid"));
        }
        // let s = this.partnerService.buildTemplatePrintFormData(printMessage.data);
        // if (printMessage.templateType == ETemplatePrintType.HTML) {
        await this.printBill(printMessage);
        // }
        return new HttpMessage(HttpStatus.OK, "Request received");
    }

    /**
     * printBill
     */
    public printBill(printMessage: PartnerPrintMessage) {
        if (false && !socketService.checkClientConnected(printMessage.terminalId)) {
            console.log("PRINT BILL ERROR, CANT CONNECT TO CLIENT,", printMessage.terminalId, printMessage.merchantId);
            throw new HttpException(new HttpMessage(HttpStatus.NOT_IMPLEMENTED, "Cant request to terminal"), HttpStatus.NOT_IMPLEMENTED);
        }
        const templateName = PartnerPrintMessage.getTemplatePrintName(printMessage);

        if (printMessage.templateType == ETemplatePrintType.HTML) {
            const dataPrint = printMessage.data.replace("</body>", "<div style='position: relative;width:100%;height: 75px'></body>");
            printMessage.data = new PrintTemplate(ETemplatePrintType.HTML, templateName, dataPrint);
        }
        if (printMessage.templateType == ETemplatePrintType.IMAGE_BASE64) {
            const dataPrint = printMessage.data;
            printMessage.data = new PrintTemplate(ETemplatePrintType.IMAGE_BASE64, templateName , dataPrint);
        }

        socketService.sendDataToClient(ESocketEvent.SEND_PRINT_BILL, SocketMessage.OK(ESocketEvent.SEND_PRINT_BILL, printMessage.terminalId, printMessage.data));
    }
    // setTimeout(() => { // Retry 1
    //     socketService.sendDataToClient(ESocketEvent.SEND_PRINT_BILL, printMessage.terminalId, SocketMessage.OKMessage(ESocketEvent.SEND_PRINT_BILL, printMessage.data));
    // }, TIMEOUT_RETRY);

    // setTimeout(() => { // Retry 2
    //     socketService.sendDataToClient(ESocketEvent.SEND_PRINT_BILL, printMessage.terminalId, SocketMessage.OKMessage(ESocketEvent.SEND_PRINT_BILL, printMessage.data));
    // }, TIMEOUT_RETRY * 2);

    // public async getPrintTemplate(socketClientMessage: SocketClientMessage) {
    //     if (!socketClientMessage.serialNumber || !socketClientMessage.data.templateName) {
    //         return SocketMessage.badMessage("Income message not valid.");
    //     }

    //     if (socketClientMessage.data.templateName.indexOf(socketClientMessage.serialNumber) != 0) {
    //         throw new WsException("Client cant get this template, " + socketClientMessage.serialNumber + " " + socketClientMessage.data.templateName);
    //     }

    //     let templateData = await redisService.get(socketClientMessage.data.templateName) || "";
    //     templateData = templateData.replace("</body>", "<div style='position: relative;width:100%;height: 75px'></body>");
    //     return SocketMessage.OKMessage(ESocketEvent.SEND_TEMPLATE_DATA, templateData);
    // }
}
