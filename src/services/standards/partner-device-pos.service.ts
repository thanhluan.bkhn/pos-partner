import { Service, InjectCustomRepository } from "~/@core/decorator";
import { PartnerDevicePosRepository } from "~/repositories/primary";

@Service()
export class PartnerDevicePosService {

    @InjectCustomRepository(PartnerDevicePosRepository)
    public partnerDevicePosRepository: PartnerDevicePosRepository;

}
