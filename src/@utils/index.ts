import { ObjectLiteral } from "typeorm";

export namespace Utils {

    export function mapWhereAndRelations (dataRequest: ObjectLiteral) {
        const where = {};
        const relations: string [] = [];
        Object.keys(dataRequest).forEach( key => {
            if (key.includes(".")) {
                const arrkey = key.split(".");
                if (arrkey.length == 1) {
                    Object.assign(where, {
                        key: dataRequest[key]
                    })
                } else {
                    relations.push(arrkey[0]);
                    const newAssign = where[arrkey[0]] || {};
                    Object.assign(where, {
                        [arrkey[0]]: {
                             ...newAssign,
                            [arrkey[1]]: dataRequest[key]
                        }
                    })
                }
            }
        })
        return {
            where,
            relations
        }
    }
    export function enumKeys (value: any) {
        return Object.keys(value).filter(k => typeof value[k] === "number");
    }
    export function enumValues (value: any) {
        return enumKeys(value).map(k => value[k])
    }
}