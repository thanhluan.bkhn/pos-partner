import * as fs from 'fs';
import * as path from 'path';

export const LOGGER = {
    write: (content: any) => {
         let t = new Error();

         const dir = path.join(__dirname, '../../../../logs');
         if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
         fs.appendFileSync(path.join(dir, 'log.txt'), JSON.stringify(content) + '\r\n');

    }
}