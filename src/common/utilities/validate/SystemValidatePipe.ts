import { ValidationPipe, ArgumentMetadata } from "@nestjs/common";
import { LOGGER } from "../logger";

export class SystemValidatePipe extends ValidationPipe {
    public async transform(value, metadata: ArgumentMetadata) {
        try {
            return await super.transform(value, metadata)
        } catch (e) {
            LOGGER.write('SystemValidatePipe error: ' + e.message + "value" + value);
            console.log('SystemValidatePipe error: ', e.message, "value", value);
            throw e;

            // if (e instanceof BadRequestException) {
            //   throw new UnprocessableEntityException(e.message.message)
            // }
        }
    }
}

