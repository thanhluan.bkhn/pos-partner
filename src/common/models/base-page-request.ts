import { Expose } from 'class-transformer';
export class BasePageRequest  {

    @Expose()
    pageSize: number;

    @Expose()
    pageIndex: number;
 
}