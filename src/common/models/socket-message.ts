import { ESocketEvent, ETemplatePrintType } from "../enums";
import { HttpStatus } from "@nestjs/common";

export class SocketMessage<T = any> {
    type: ESocketEvent;
    data?: T;
    serialNumber: string;
    message = "OK";
    statusCode: HttpStatus;

    static Error(message: string) {
        const socketMessage = new SocketMessage();
        socketMessage.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        socketMessage.message = message;
        return socketMessage;
    }

    static Bad(message: string) {
        const socketMessage = new SocketMessage();
        socketMessage.statusCode = HttpStatus.BAD_REQUEST;
        socketMessage.message = message;
        return socketMessage;
    }

    static OK(skEvent: ESocketEvent, serialNumber: string, data: any) {
        const socketMessage = new SocketMessage();
        socketMessage.type = skEvent;
        socketMessage.statusCode = HttpStatus.OK;
        socketMessage.message = "";
        socketMessage.data = data;
        socketMessage.serialNumber = serialNumber;
        return socketMessage;
    }

    toJSonString() {
        let { type, statusCode, message, data } = this;
        let dataValue = "";
        if (data) { dataValue = JSON.stringify(data).substr(0, 20); }
        return JSON.stringify({ type, statusCode, message, dataValue });
    }

}

export class PrintTemplate {
    type: ETemplatePrintType;
    data: string;
    templatePrintName: string;
    constructor(type: ETemplatePrintType, templatePrintName: string , data: string) {
        this.data = data;
        this.type = type;
        this.templatePrintName = templatePrintName;
    }
}

export class SocketClientMessage<T = any> {
    serialNumber: string;
    data: T;
}
