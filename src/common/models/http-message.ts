import { HttpStatus } from "@nestjs/common";
import { EStatus } from "../enums";

export class HttpMessage {
  code: number;
  message: string;
  data: any;
  error: boolean;

  constructor(code?: number, message?: string, data?: any, error = false) {
    this.code = EStatus.Success;
    this.message = "success";
    this.data = null;
    this.error = false;

    if (code) { this.code = code; }
    if (message) { this.message = message; }
    if (data) { this.data = data; }
    if (code > 300) { this.error = true }
    if (error) { this.error = error; }
  }

  static Success(message: string, data?: any) {
    return new HttpMessage(HttpStatus.OK, message, data);
  }

  static Error(message: string) {
    return new HttpMessage(HttpStatus.BAD_REQUEST, message);
  }
}

export class Message {
  code: number;
  message: string;
  data: any;
  constructor(code?: number, message?: string, data?: any) {
    this.code = EStatus.Success;
    this.message = "action.success";
    this.data = null;
    if (code) { this.code = code; }
    if (message) { this.message = message; }
    if (data) { this.data = data; }
  }

  static Success(message: string, data?: any) {
    return new Message(EStatus.Success, message, data);
  }

  static Error(message: string) {
    return new Message(EStatus.Error, message);
  }
}
