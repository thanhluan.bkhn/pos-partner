
export interface IPage {
    pageSize?: number,
    pageIndex?: number
}

export type IPageRequest<DTO = {}> = {
    [key in keyof DTO]?: DTO[key]
} & IPage

export interface IPageResponse<T = any>  extends IPage {
    data: T [],
    total: number
}
