import * as redis from 'redis';
import { RedisClient } from 'redis';
class RedisService {
    public client: RedisClient;
    init(options: { host: string, port: number, password?: string }) {
        this.client = redis.createClient({
            host: options.host,
            port: options.port,
            auth_pass: options.password
        });
        this.client.on("connect", (err, rs) => {
            console.log("redis Connected");

        })
        this.client.on("error", (err) => {
            console.log("redis connect error", err);

        })
    }
    hgetall = (key: string): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.client.hgetall(key, (err: Error | null, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    /**
     * hmset Hash Set
     * @param timeout expire time on second, default 3600
     */
    hmset = (key: string, object: any, timeout = 3600): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.client.hmset(key, object, (err: Error | null, reply: any) => {
                if (err) {
                    reject(err);
                } else {
                    this.client.expire(key, timeout);
                    resolve(reply);
                }
            });
        });
    }

    /**
     * Set
     * @param timeout expire time on second, default 3600
     */
    set = (key: string, object: any, timeout = 3600): Promise<any> => {
            return new Promise((resolve, reject) => {
                this.client.set(key, object, (err: Error | null, reply: any) => {
                    if (err) {
                        console.log("redis set error", key, err);
                        reject(err);
                    } else {
                        this.client.expire(key, timeout);
                        resolve(reply);
                    }
                });
            });

    }
    get = (key: string): Promise<any> => {
        return new Promise((resolve, reject) => {
            let d = this.client.get(key, (err, data) => {
                resolve(data);
            });
        });
    }

    getAndClearKey = (key: string): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.client.get(key, data => {
                this.client.del(key);
                resolve(data);
            });
        });
    }
    del = (key: string): Promise<any> => {
        return new Promise((resolve, reject) => {
            this.client.del(key, (err, data) => {
                resolve(data);
            });
        });
    }
    flushall = () => {
        return new Promise((resolve, reject) => {
            this.client.flushall((err, succeeded) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(succeeded);
                }
            });
        });
    }
}

export default new RedisService();
