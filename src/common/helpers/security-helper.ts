const jwt = require("jsonwebtoken");
import * as crypto from "crypto";
import * as CryptoJS from 'crypto-js';
export class SecurityHelper {
    static serectKey: string;
    static algorithm: string;
    static password: string;

    static hmac256(data, secretKey) {
        if (!secretKey) { secretKey = SecurityHelper.serectKey; }
        return CryptoJS.HmacSHA256(data, secretKey).toString(CryptoJS.enc.Base64);
    }

    static hashData(data) {
        if (data) {
            const hash = crypto.createHash("sha256").update(data).digest("hex");
            return hash;
        }
        return "";
    }
    static md5(data) {
        if (data) {
            const hash = crypto.createHash("md5").update(data).digest("hex");
            return hash;
        }
        return "";
    }
    static encrypt(data, key) {
        if (!key) {
            key = this.algorithm;
        }
        let input = data;
        if (typeof data === "object") {
            input = JSON.stringify(data);
        }
        const cipher = crypto.createCipher(key, this.password);
        let crypted = cipher.update(input, "utf8", "hex");
        crypted += cipher.final("hex");
        return crypted;
    }
    static decrypt(data, toObject, key) {
        if (!key) {
            key = this.algorithm;
        }
        const decipher = crypto.createDecipher(key, this.password);
        let dec = decipher.update(data, "hex", "utf8");
        dec += decipher.final("utf8");
        if (toObject) {
            dec = JSON.parse(dec);
        }
        return dec;
    }
    static generateToken(data, expired = "60") {
        const token = jwt.sign(data, this.serectKey, { expiresIn: expired });
        return token;
    }
    static getToken(token) {
        try {
            const decoded = jwt.verify(token, this.serectKey);
            return decoded;
        } catch (error) {
            return "";
        }
    }
}
SecurityHelper.serectKey = "c0FTREZhYSMyNTYAsBFAIzFEc2RmR1NkZnMsLi9TREZTRDMyNA==";
SecurityHelper.algorithm = "aes256";
SecurityHelper.password = "d6dfsfeq-qnd-serct-45@56@";
exports.SecurityHelper = SecurityHelper;
exports.default = SecurityHelper;