import * as socketIo from 'socket.io';
import * as http from "http";
import { Socket } from 'socket.io';
import { HttpException, InternalServerErrorException, UnprocessableEntityException, BadRequestException } from '@nestjs/common';

import { SocketMessage, SocketClientMessage } from '../models/socket-message';
import { PartnerPrintService } from '~/services/standards/partner-print.service';
import { PartnerPrintBillLogService } from '~/services';
import redisService from './redis.service';
import { HttpMessage } from '../models/http-message';
import { InjectService } from '~/@core/decorator';
import { ESocketClientEvent, ESocketEvent } from '../enums';
type TListener = (data: any) => void;

interface ClientAskInfoMessage {
    socketId: string;
    serialNumber: string;
}

class SocketService {
    private io: socketIo.Server;
    private socket: Socket;
    private listEventListener: any[];
    /*** clientSocketItemMapping Mapping SocketId <-> SerialNumber*/
    private clientSocketItemMapping = {
        serialNumberToSocketId: {},
        socketToSerialNumber: {}
    };

    @InjectService('PartnerPrintService')
    private partnerPrintService: PartnerPrintService;

    public pushToClientIDMapping(socketId: string, serialNumber: string): boolean {
        if (!socketId || !serialNumber) { return false; }
        this.clientSocketItemMapping.serialNumberToSocketId[serialNumber] = socketId;
        this.clientSocketItemMapping.socketToSerialNumber[socketId] = serialNumber;
    }
    public clearClientIDMapping(socketId) {
        if (!socketId) { return; }
        let serialNumber = this.clientSocketItemMapping.socketToSerialNumber[socketId];

        if (serialNumber) {
            delete this.clientSocketItemMapping.socketToSerialNumber[socketId];
            delete this.clientSocketItemMapping.serialNumberToSocketId[serialNumber];
        }
    }
    public getSerialNumberFromClientItemMapping(socketId) {
        return this.clientSocketItemMapping.socketToSerialNumber[socketId];
    }

    public getSocketIdFromClientItemMapping(serialNumber) {
        return this.clientSocketItemMapping.serialNumberToSocketId[serialNumber];
    }

    init(httpServer: http.Server) {
        const partnerPrintService = new PartnerPrintService();
        const partnerPrintBillLogService = new PartnerPrintBillLogService();
        const instance = this;
        this.io = socketIo(httpServer);
        this.io.on('connect', (socket: Socket) => {
            socket.on(ESocketClientEvent.ASK_CLIENT_INFO, (data: ClientAskInfoMessage) => {
                if (!data.serialNumber) {
                    console.log("ASK_CLIENT_INFO ERROR SerialNumber empty");
                    return;
                }
                instance.pushToClientIDMapping(socket.id, data.serialNumber);
                console.log("ASK_CLIENT_INFO", socket.id, data.serialNumber);
            })

            socket.on(ESocketClientEvent.ASK_PARTNER_PRINT_TEMPLATE_RESULT, async (data: SocketClientMessage<{
                templateName: string,
                result: boolean,
            }>) => {
                try {
                    const templateName = data.data.templateName;
                    console.log("ASK_PARTNER_PRINT_TEMPLATE_RESULT", data.serialNumber, data.data.templateName, data.data.result);
                    await partnerPrintBillLogService.updatePrintStatus(data.data.result, data.serialNumber, templateName);
                    redisService.del(templateName);
                } catch (error) {
                    console.log("ASK_PARTNER_PRINT_TEMPLATE_RESULT Error", error);
                }
            });

            socket.on(ESocketClientEvent.GET_PARTNER_PRINT_TEMPLATE, (data: SocketClientMessage) => {
                console.log("ESocketClientEvent.GET_PARTNER_PRINT_TEMPLATE - Unuse channel");
                return;
                // partnerPrintService.getPrintTemplate(data).then(skMessage => {
                //     console.log("GET_PARTNER_PRINT_TEMPLATE", data.serialNumber, skMessage.toJSonString());
                //     socket.emit(ESocketEvent.SEND_TEMPLATE_DATA, skMessage);
                // })
                //     .catch(error => {
                //         console.log("GET_PARTNER_PRINT_TEMPLATE Error", error);
                //     });
            });

            socket.on(ESocketClientEvent.WRITE_LOG, (data: SocketClientMessage) => {
                // Save Log.
            });

            socket.on('disconnect', () => {
                instance.clearClientIDMapping(socket.id);
                console.log(`Client ${socket.id} disconnected`);
            });
        })
    }

    public sendDataToClient(ESocketEvent: ESocketEvent, data: SocketMessage) {
        const socketId = this.getSocketIdFromClientItemMapping(data.serialNumber);
        if (!socketId) { throw new BadRequestException(HttpMessage.Error("Error, Terminal id " + data.serialNumber + " not connected.")); }
        return this.io.sockets.sockets[socketId].emit(ESocketEvent, data);

    }

    public checkClientConnected(serialNumber: string) {
        let socketClient = this.getSocketIdFromClientItemMapping(serialNumber);
        if (socketClient) { return true; }
        return false;
    }

    public sendToAll(ESocketEvent: ESocketEvent, data: SocketMessage) {
        if (!this.io) {
            console.log("Socket is not connected");
            throw new InternalServerErrorException("Server Error");
        }
        this.io.emit(ESocketEvent, data);
    }
    // public sendClientTest(ESocketEvent: string, data: any) {
    //     if (!this.io) {
    //         console.log("Socket is not connected");
    //         throw new InternalServerErrorException("Server Error");
    //     }
    //     this.io.emit(ESocketEvent, data);
    // }
}
export default new SocketService()
