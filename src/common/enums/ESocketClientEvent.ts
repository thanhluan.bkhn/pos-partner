export enum ESocketClientEvent {
    GET_PARTNER_PRINT_TEMPLATE = "GET_PARTNER_PRINT_TEMPLATE",
    ASK_PARTNER_PRINT_TEMPLATE_RESULT = "ASK_PARTNER_PRINT_TEMPLATE_RESULT",
    WRITE_LOG = "WRITE_LOG",
    ASK_CLIENT_INFO = "ASK_CLIENT_INFO",

}