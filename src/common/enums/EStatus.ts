export enum EStatus {
    "Draft" = 1,
    "New" = 2,
    "WaitingForApproved" = 3,
    "Approved" = 4,
    "Rejected" = 5,
    "WaitingForPublished" = 6,
    "Published" = 7,
    "Expired" = 8,
    "Removed" = 9,
    "ViewDtailOnly" = 10,
    "Hide" = 11,
    "Distributed" = 12,
    "Used" = 13,
    "Active" = 14,
    "Inactive" = 15,
    "Completed" = 16,
    "Success" = 17,
    "Unsuccess" = 18,
    "Error" = 19,
}

export enum PartnerVoucherStatus {
    SUCCESS = 1,
    PARTNER_NOT_FOUND = 4,
    SKU_NOT_FOUND = 5,
    PROMOTION_EMPTY = 6,
    REQUEST_EXPIRE = 7,
    SIGNATURE_NOT_VALID = 401,
    PARTNER_NOT_IMPLEMENT = 503
}

export enum PromotionStatus {
    SAVED = "saved",
    WAIT_FOR_APPROVE = "wait_for_approve",
    COMPLETE = "complete",
    REJECT = "reject",
    WAIT_FOR_PARTNER_APPROVE = "wait_for_partner_approve",
    WAIT_FOR_MERCHANT_APPROVE = "wait_for_merchant_approve",
    RUNNING = "running",
    PENDING = "pending",
    EXPIRE = "expire",
    CANCElLED = "cancelled",
}
export enum PromotionDiscountType {
    CASH = "cash",
    DISCOUNT = "discount"
}

export enum VoucherStatus {
    CANCEL = "cancel",
    NEW = "new",
    EXPIRE = "expire",
    USED = "used",
    DISTRIBUTED = "distributed",
}