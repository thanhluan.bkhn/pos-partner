export enum ETemplatePrintType {
    Empty = "",
    HTML = "html",
    IMAGE_BASE64 = "image/base64"
}